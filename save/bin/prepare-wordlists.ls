``#!/usr/bin/env node``

'''
Takes wordlists from data-dir (e.g. data/wordlists) and makes .js files in
js-dir (e.g. webroot/js/data/wordlists)
'''

fs = require 'fs'
path = require 'path'

{ curry, lines, words, keys, map, each, join, compact, last, values, } = prelude-ls = require 'prelude-ls'

fish-lib = require 'fish-lib'
    ..import-kind global, 'all'

sys-set { +die, +verbose, +sync, }

{ argv, exit, } = process

opt = getopt do
    h:  'b'

config =
    usage: ''
    data-dir: 'data/wordlists'
    js-dir: 'webroot/js/data/wordlists'
    num-words-expected: 7_776

our =
    data-dir: relative-dir config.data-dir
    js-dir:   relative-dir config.js-dir

[...xxx] = opt.argv.remain

if opt.h
    info usage()
    exit 0

start()

# --- end

function start
    done = ({ out: filenames, }) ->
        filenames |> each go
    sys-exec do
        sprintf "ls %s/*.wordlist" shell-quote our.data-dir
        out-split: true
        done

function transform filename, words
    basename = path.basename filename
    num-words = words.length
    ierror() unless m = basename == // ^ ([^-\.]+) //
    [_, prefix] = m
    new-filename = shell-quote sprintf "%s/%s.js" our.js-dir, prefix
    log 'Making new filename %s' yellow new-filename
    log '→ number of words',
        num-words |> (if num-words == config.num-words-expected then green else bright-red)
    ierror() unless words-js = make-js words
    fs.write-file-sync new-filename, words-js
    log green '✔'

function go filename
    fs.read-file-sync filename .to-string()
    |> lines
    # --- remove last entry (blank line).
    |> popit
    |> (curry transform) filename

function popit xs
    xs
        ..pop()

function make-js words
    json = JSON.stringify words
    """
    module.exports = #json;
    """

function usage
    [_, script-name, ...args] = argv
    str = join ' ' compact array script-name, config.usage
    sprintf "Usage: %s" str

function relative-dir dir
    path.relative '.' join '/' [__dirname, '..', dir]

``#!/usr/bin/env node``

{ lines, words, keys, map, each, join, compact, last, values, } = prelude-ls = require 'prelude-ls'

path = require 'path'
fs = require 'fs'

fish-lib = require 'fish-lib'
    ..import-kind global, 'all'

sys-set { +die, -verbose, +sync, }

argv = process.argv
exit = process.exit

opt = getopt do
    h:  'b'
    v:  'b'
    c:  'b'
    l:  'b'
    b:  'b'

config =
    usage: '[-v] [-c to make css] [-l to compile ls] [-b to browserify] [-m to minify js] [-a to do all, default]'
    dir-js: 'js'
    file-less-in: 'less/style.less'
    file-less-out: 'css/style.css'
    bin-browserify: 'bin/browserify'
    bin-browserify-args: <[ -o index-browserify.js ]>
    bin-less: 'node_modules/less/bin/lessc'
    bin-lsc: 'node_modules/livescript/bin/lsc'
    bin-lsc-args: <[ -bc --no-header ]>
    bin-minify: 'node_modules/.bin/minify'
    in-minify: 'js/index-browserify.js'
    out-minify: 'js/index.js'

our = let root = path.relative process.cwd(), "#__dirname/.."
    dir-js: path.join "#root" config.dir-js
    file-less-in: path.join "#root" config.file-less-in
    file-less-out: path.join "#root" config.file-less-out
    bin-browserify: path.join "#root" config.bin-browserify
    bin-lsc: path.join "#root" config.bin-lsc
    bin-less: path.join "#root" config.bin-less
    bin-minify: path.join "#root" config.bin-minify
    in-minify: path.join "#root" config.in-minify
    out-minify: path.join "#root" config.out-minify
    root: root
    verbose-cmds: opt.v
    task-args: compact array do
        'c' if opt.c
        'l' if opt.l
        'b' if opt.b
        'm' if opt.m
    tasks: void

our.tasks = new Set do
    if our.task-args.length then our.task-args
    else <[ c l b m ]>

[...xxx] = opt.argv.remain
error usage() if xxx.length

if opt.h
    info usage()
    exit 0

let tasks = our.tasks
    if tasks.has 'c' then compile-less()
    if tasks.has 'l' then compile-ls()
    if tasks.has 'b' then browserify()
    if tasks.has 'm' then minify-js()
    if tasks.size
        log ''
        info 'All done.'
    else
        info 'Nothing to do.'

exit 0

function usage
    [_, script-name, ...args] = argv
    str = join ' ' compact array script-name, config.usage
    sprintf "Usage: %s" str

function compile-ls
    info-block 'compiling js.'
    { dir-js, } = our
    info 'js dir' magenta dir-js
    compile-file = (file) ->
        process.stdout.write sprintf '→ compiling %s ', yellow file
        sys-spawn do
            our.bin-lsc
            config.bin-lsc-args ++ [file]
            out-print: true
            verbose: false
        log green '✔'
    compile = (files) ->
        files |> each (compile-file)
    sys-exec do
        sprintf 'find %s/*.ls' shell-quote dir-js
        out-split: true
        verbose: false
        ({ out, }) ->
            compile out

function browserify
    info-block 'browserifying.'
    { bin-browserify, } = our
    info 'browserify command' magenta bin-browserify if our.verbose-cmds
    process.stdout.write '→ browserify '
    log '' if our.verbose-cmds
    sys-spawn do
        our.bin-browserify
        config.bin-browserify-args
        verbose: our.verbose-cmds
        out-print: false
    log green '✔'

function minify-js
    info-block 'minifying js.'
    { bin-minify, } = our
    info 'minify command' magenta our.bin-minify if our.verbose-cmds
    info 'minify file in' yellow our.in-minify
    info 'minify file out' green our.out-minify
    cmd = sprintf do
        '%s %s > %s'
        shell-quote bin-minify
        shell-quote our.in-minify
        shell-quote our.out-minify
    process.stdout.write '→ minify js '
    log '' if our.verbose-cmds
    sys-exec do
        cmd
        verbose: our.verbose-cmds
        out-print: false
    log green '✔'

function compile-less
    info-block 'compiling css.'
    { bin-less, file-less-in, file-less-out, } = our
    info 'less command' magenta our.bin-less if our.verbose-cmds
    info 'less file in' yellow file-less-in
    info 'less file out' green file-less-out
    cmd = sprintf do
        '%s %s > %s'
        shell-quote bin-less
        shell-quote file-less-in
        shell-quote file-less-out
    process.stdout.write '→ compiling '
    log '' if our.verbose-cmds
    sys-exec do
        cmd
        verbose: our.verbose-cmds
    log green '✔'

function info-block
    log ''
    info.apply null,
        ['---'] ++ to-array &

function to-array
    [.. for &.0]

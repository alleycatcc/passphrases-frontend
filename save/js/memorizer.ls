'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    create

$ = require 'jquery'

{ log, sprintf, } = require './util'

proto =
    # --- instance methods.
    update-stats: update-stats
    next-try-maybe: next-try-maybe
    next-try: next-try
    countdown-complete: countdown-complete
    test-for-success: test-for-success
    cleanup: cleanup

function create { passphrase, elems, ui, }
    (Object.create proto) <<<
        # --- instance variables.
        tries: 0
        delay: 0
        typed-without-looking: false
        countdown-values: [60 120 300 600 1800 3600]
        current-countdown: 0
        keypress-enabled: false
        passphrase: passphrase
        job-countdown: void
        e: elems
        ui: ui

function cleanup
    if @job-countdown then clear-interval that

function update-stats
    switch
    | @tries == 0 => note @, 'Type the passphrase'
    | @tries == 1 => note @, 'Try to type the passphrase before the hint appears'
    | @tries < 5 and @typed-without-looking => note @, 'Good job! Try making it to 5 tries'
    | @tries < 10 and @typed-without-looking => note @, 'Try making it to 10 tries without seeing what you type'
    | @typed-without-looking => note @, 'Keep practicing.'
    @e.$div-memorize-tries.html @tries

function next-try-maybe
    return unless @keypress-enabled
    @keypress-enabled = false
    @next-try ...
    true

function next-try
    @ui.ui-hint-reset()
    @ui.ui-mode-memorize 'active'
    @e.$input-passphrase-test
        ..val ''
        ..focus()
    @ui.ui-hint-show @delay

function countdown-complete
    @ui.ui-mode-memorize 'ready'
    @e.$input-passphrase-test.blur()
    @keypress-enabled = true

function test-for-success
    $input = @e.$input-passphrase-test
    passphrase-test = $input.val()
    return success @ if passphrase-test == @passphrase
    failure @, passphrase-test, $input

# ------ private instance methods.
#
# remember to call with 'this' as the first param (think python).

function success ctxt
    let @ = ctxt
        ++@tries
        if @typed-without-looking
            # play-sound XXX
            void
        if @e.$div-hint-progress.is ':animated'
            @typed-without-looking = true
        if @tries == 1
            @delay = 5
        else if @tries <= 6
            ++@delay
        if @delay <= 10
            ++@delay
        @update-stats()
        if @tries == 5
            @ui.ui-passphrase-type 'password'
        if @tries < 10
            @next-try()
        else
            if @typed-without-looking
                countdown @
            else
                @next-try()

function failure ctxt, passphrase-test, $input
    let @ = ctxt
        test-length = passphrase-test.length

        # --- add the typo class if the input string is shorter than the
        # required passphrase and is not a substring of it.

        typo = @passphrase.index-of(passphrase-test) != 0

        if typo then $input.add-class 'typo'
        else $input.remove-class 'typo'

function note ctxt, str
    let @ = ctxt
        @e.$div-memorize-note.html str

function update-countdown ctxt, total-seconds
    let @ = ctxt
        seconds = total-seconds % 60
        seconds = sprintf '%02d' seconds
        minutes = Math.floor total-seconds / 60
        @e.$div-countdown.html "#minutes:#seconds"

function countdown ctxt
    let @ = ctxt
        @typed-without-looking = false
        @ui.ui-mode-memorize 'waiting'
        note @, 'Give your mind a rest'
        total-seconds = @countdown-values[@current-countdown]
        update-countdown @, total-seconds

        if @job-countdown then clear-interval that

        counter = ~>
            --total-seconds
            if total-seconds >= 0
                return update-countdown @, total-seconds

            # --- countdown complete, move on to the next countdown.
            if @current-countdown < @countdown-values.length - 1
                ++@current-countdown
            # notification XXX
            log 'Time to keep practicing your passphrase!'
            clear-interval @job-countdown
            @countdown-complete()

        @job-countdown = set-interval counter, 1000


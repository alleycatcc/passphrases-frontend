'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init
    get-entropy
    calculate

mustache = require 'mustache'
{ get-config, } = require './config'
{ log, math-round, math-log2, } = require './util'
template = require './template'

our = let c = get-config()
    strengths: c.strengths

function init
    true

function get-entropy word-count, num-words
    entropy-per-word = math-log2 word-count
    entropy = num-words * entropy-per-word
    [ entropy-per-word, entropy, ]

function calculate { entropy-per-word, word-count, num-words, }

    keyspace = Math.pow word-count, num-words
    guesses-per-second = 1e12
    seconds = (keyspace / 2) / guesses-per-second

    [ time-value, time-units, strength, ] = get-time-units seconds

# --- returns [ time-value, units, strength, ]
#
# where strength is 2 for times in the order of a thousand years and
# greater, 1 for the (single) value on the order of years, and 0 for
# everything less.

function get-time-units seconds
    switch
    | seconds < 60 =>
        [seconds, 'seconds', 0]
    | seconds < 60 * 60 =>
        [seconds / 60, 'minutes', 0]
    | seconds < 60 * 60 * 24 =>
        [seconds / 60 / 60, 'hours', 0]
    | seconds < 60 * 60 * 24 * 30 =>
        [seconds / 60 / 60 / 24, 'days', 0]
    | seconds < 60 * 60 * 24 * 365 =>
        [seconds / 60 / 60 / 24 / 29.6, 'months', 0]
    | seconds < 60 * 60 * 24 * 365 * 1e3 =>
        [seconds / 60 / 60 / 24 / 365, 'years', 1]
    | seconds < 60 * 60 * 24 * 365 * 1e6 =>
        [seconds / 60 / 60 / 24 / 365 / 1e3, 'thousand years', 2]
    | seconds < 60 * 60 * 24 * 365 * 1e9 =>
        [seconds / 60 / 60 / 24 / 365 / 1e6, 'million years', 2]
    | seconds < 60 * 60 * 24 * 365 * 1e12 =>
        [seconds / 60 / 60 / 24 / 365 / 1e9, 'billion years', 2]
    | seconds < 60 * 60 * 24 * 365 * 1e15 =>
        [seconds / 60 / 60 / 24 / 365 / 1e12, 'trillion years', 2]
    | seconds < 60 * 60 * 24 * 365 * 1e18 =>
        [seconds / 60 / 60 / 24 / 365 / 1e15, 'quadrillion years', 2]
    | _ =>
        [seconds / 60 / 60 / 24 / 365 / 1e18, 'quintillion years', 2]

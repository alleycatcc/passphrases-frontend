'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, jqueryColor, animateSeries, each, ref$, log, error, array, proto, out$ = typeof exports != 'undefined' && exports || this;
out$.create = create;
$ = require('jquery');
global.jQuery = $;
jqueryColor = require('jquery-color/jquery.color');
animateSeries = require('./animate').animateSeries;
each = require('prelude-ls').each;
ref$ = require('./util'), log = ref$.log, error = ref$.error, array = ref$.array;
proto = {
  go: go,
  stop: stop
};
function create(args){
  var $button, $buttonInner, colorWeak, doFontSize, ref$, rotatorInterval, rotatorDuration, colorOrig, fontSizeOrigPx, m, fontSizeOrig;
  $button = args.$button, $buttonInner = args.$buttonInner, colorWeak = args.colorWeak, doFontSize = (ref$ = args.doFontSize) != null ? ref$ : true, rotatorInterval = (ref$ = args.rotatorInterval) != null ? ref$ : 10000, rotatorDuration = (ref$ = args.rotatorDuration) != null ? ref$ : 500;
  colorOrig = $button.css('color');
  fontSizeOrigPx = $buttonInner.css('font-size');
  m = /^(.+)px$/.exec(fontSizeOrigPx);
  if (!m) {
    return error("bad value " + fontSizeOrigPx);
  }
  fontSizeOrig = m[1];
  return ref$ = Object.create(proto), ref$.colorOrig = colorOrig, ref$.fontSizeOrig = fontSizeOrig, ref$.timeouts = [], ref$.started = false, ref$.animate = {
    fontSize: doFontSize ? animateSeries({
      $elem: $button,
      props: array({
        'font-size': fontSizeOrig * 1.3
      }, {
        'font-size': fontSizeOrig
      }, {
        'font-size': fontSizeOrig
      }),
      propsOrig: {
        'font-size': fontSizeOrig
      },
      duration: [400, 400, 5000],
      repeat: true
    }) : void 8,
    color: animateSeries({
      $elem: $button,
      props: array({
        color: colorWeak
      }),
      propsOrig: {
        color: colorOrig
      },
      duration: [3000]
    }),
    rotator: rotator({
      $elem: $buttonInner,
      interval: rotatorInterval,
      duration: rotatorDuration
    })
  }, ref$;
}
function go(){
  var a;
  if (this.started) {
    return;
  }
  this.started = true;
  a = this.animate;
  if (a.fontSize) {
    this.timeouts.push(setTimeout(function(){
      return a.fontSize.go();
    }, 4000));
  }
  a.color.go();
  return a.rotator.go();
}
function stop(){
  var a, that;
  a = this.animate;
  clearTimeout(
  this.timeouts.each);
  this.timeouts = [];
  if (that = a.fontSize) {
    that.stop();
  }
  a.color.stop();
  a.rotator.stop();
  return this.started = false;
}
function rotator(arg$){
  var $elem, interval, duration, rotate, unrotate;
  $elem = arg$.$elem, interval = arg$.interval, duration = arg$.duration;
  rotate = function(){
    return $elem.addClass('x--rotate');
  };
  unrotate = function(){
    return $elem.removeClass('x--rotate');
  };
  return {
    timeouts: [],
    go: function(){
      return this.next();
    },
    next: function(){
      var this$ = this;
      this.timeouts.push(setTimeout(function(){
        return rotate();
      }, interval));
      return this.timeouts.push(setTimeout(function(){
        unrotate();
        return this$.next();
      }, duration + interval));
    },
    stop: function(){
      each(clearTimeout)(
      this.timeouts);
      this.timeouts = [];
      return unrotate();
    }
  };
}
'$button.css \'dummy\' 0\n# --- final degree value.\nprops = dummy: 359\nprev-class = void\n$button.animate props,\n    duration: 100,\n    step: (x) ->\n        $button.remove-class that if prev-class\n        $button.add-class (prev-class = \'u-rotate--\' + x)';
'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var log, out$ = typeof exports != 'undefined' && exports || this;
out$.animateSeries = animateSeries;
log = require('./util').log;
function animateSeries(arg$){
  var $elem, props, propsOrig, duration, repeat;
  $elem = arg$.$elem, props = arg$.props, propsOrig = arg$.propsOrig, duration = arg$.duration, repeat = arg$.repeat;
  return {
    idx: void 8,
    $elem: $elem,
    repeat: repeat,
    propsOrig: propsOrig,
    props: props,
    duration: duration,
    finishCalled: void 8,
    animating: void 8,
    animate: function(){
      var this$ = this;
      this.animating = true;
      return this.$elem.animate(this.props[this.idx], {
        duration: this.duration[this.idx],
        done: function(){
          this$.animating = false;
          if (this$.finishCalled) {
            this$.finishCalled = false;
            return this$.restore();
          } else {
            return this$.next();
          }
        }
      });
    },
    restore: function(){
      return this.$elem.animate(this.propsOrig, {
        duration: 100
      });
    },
    next: function(){
      var l;
      this.idx = ++this.idx;
      l = this.props.length;
      if (this.idx === l) {
        if (this.repeat) {
          this.idx = this.idx % this.props.length;
        } else {
          return;
        }
      }
      return this.animate();
    },
    go: function(){
      this.idx = -1;
      this.finishCalled = false;
      return this.next();
    },
    stop: function(){
      if (this.animating) {
        this.finishCalled = true;
        return this.$elem.finish();
      } else {
        return this.restore();
      }
    }
  };
}
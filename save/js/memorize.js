'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, ref$, log, error, getElems, getConfig, memorizer, nav, our, out$ = typeof exports != 'undefined' && exports || this;
out$.init = init;
out$.uiSetPassphraseAndMemorize = uiSetPassphraseAndMemorize;
$ = require('jquery');
ref$ = require('./util'), log = ref$.log, error = ref$.error, getElems = ref$.getElems;
getConfig = require('./config').getConfig;
memorizer = require('./memorizer');
nav = require('./nav');
our = {
  elems: {},
  passphrase: void 8,
  mobile: function(){
    return $('body').width() < 601;
  }
};
window.XXX = our.mobile;
function init(){
  (function(u){
    if (!u) {
      return error("Can't init memorize");
    }
    import$(our.elems, u);
  }.call(this, getElems(getConfig().selectors.memorize)));
  initUi();
  return initEvents();
}
function initUi(){
  uiModeInput();
  updateButtonMemorize();
  if (our.mobile()) {
    return $('body').height(2000);
  }
}
function uiPassphraseType(type){
  var e;
  e = our.elems;
  e.$inputPassphraseTest.attr('type', type);
  return nav.switchTo(1);
}
function uiModeInput(){
  var e;
  e = our.elems;
  e.$divMemorizeInput.show();
  return e.$divMemorizeGo.hide();
}
function uiModeGo(passphrase){
  var e;
  e = our.elems;
  e.$divMemorizeInput.hide();
  e.$divMemorizeGo.show();
  uiModeMemorize('active');
  uiPassphraseType('text');
  return e.$divHintShow.text(passphrase);
}
function uiHintReset(){
  var e, x$, y$, z$;
  e = our.elems;
  x$ = e.$divHintProgress;
  x$.stop();
  x$.css('width', '0%');
  y$ = e.$divHintProgressWrapper;
  y$.show();
  z$ = e.$divHintShow;
  z$.hide();
  return z$;
}
function uiHintShow(delay){
  var e, animate;
  if (our.mobile()) {
    delay *= 4;
  }
  console.log('mobile', our.mobile());
  e = our.elems;
  animate = {
    width: '100%'
  };
  return e.$divHintProgress.animate(animate, delay * 1000, 'linear', function(){
    e.$divHintProgressWrapper.hide();
    return e.$divHintShow.show();
  });
}
function uiModeMemorize(mode){
  var e, divs;
  e = our.elems;
  divs = {
    active: e.$divMemorizeActive,
    waiting: e.$divMemorizeWaiting,
    ready: e.$divMemorizeReady
  };
  divs.active.hide();
  divs.waiting.hide();
  divs.ready.hide();
  return divs[mode].show();
}
function initEvents(){
  var e, submit;
  e = our.elems;
  submit = function(){
    var that;
    if (that = getPassphrase()) {
      startMemorizing(that);
    } else {
      typo();
      focusInputPassphrase();
    }
    return false;
  };
  e.$formMemorize.submit(submit);
  e.$buttonStartMemorizing.click(submit);
  e.$buttonCancel.click(function(){
    var that;
    uiModeInput();
    e.$inputPassphrase.focus();
    if (that = our.mem) {
      return that.cleanup();
    }
  });
  e.$buttonSkipCountdown.on('click', function(){
    var mem;
    if (!(mem = our.mem)) {
      return;
    }
    return mem.countdownComplete();
  });
  e.$inputPassphraseTest.on('input', function(){
    var mem;
    if (!(mem = our.mem)) {
      return;
    }
    return mem.testForSuccess();
  });
  e.$inputPassphrase.on('input', function(){
    our.passphrase = $(this).val();
    return updateButtonMemorize();
  });
  return $(window).on('keypress', function(e){
    var mem, kc;
    if (!(mem = our.mem)) {
      return;
    }
    kc = e.keyCode;
    if (!(kc === 0 || kc === 32)) {
      return;
    }
    if (mem.nextTryMaybe()) {
      return false;
    }
  });
}
function uiSetPassphraseAndMemorize(passphrase){
  var e;
  e = our.elems;
  our.passphrase = passphrase;
  e.$inputPassphrase.text(passphrase);
  return startMemorizing(passphrase);
}
function startMemorizing(passphrase){
  var e, that, memorizerParams, x$;
  e = our.elems;
  uiModeGo(passphrase);
  if (that = our.mem) {
    that.cleanup();
  }
  memorizerParams = {
    passphrase: passphrase,
    elems: our.elems,
    ui: {
      uiHintReset: uiHintReset,
      uiModeMemorize: uiModeMemorize,
      uiHintShow: uiHintShow,
      uiPassphraseType: uiPassphraseType
    }
  };
  x$ = our.mem = memorizer.create(memorizerParams);
  x$.updateStats();
  x$.nextTry();
  return x$;
}
function getPassphrase(){
  return our.elems.$inputPassphrase.val();
}
function typo(){
  var e, pp;
  e = our.elems;
  pp = e.$inputPassphrase;
  pp.addClass('typo');
  return setTimeout(function(){
    return pp.removeClass('typo');
  }, 100);
}
function focusInputPassphrase(){
  var e;
  e = our.elems;
  return e.$inputPassphrase.focus();
}
function updateButtonMemorize(){
  var e, pp, $button;
  e = our.elems;
  pp = our.passphrase;
  $button = e.$buttonStartMemorizing;
  if (pp && pp.length) {
    return $button.removeClass('x--disabled');
  } else {
    return $button.addClass('x--disabled');
  }
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init
    log
    error
    warn
    ajax-get
    math-log2
    math-round
    get-elems
    sprintf
    array
    to-array
    is-function

$ = require 'jquery'
sprintf-mod = require 'sprintf'

{ join, } = require 'prelude-ls'

our =
    dev: void

function init { dev, }
    our.dev = dev

function sprintf
    sprintf-mod ...

function log
    if our.dev
        console~log ...
    else
        alert join ' ' to-array &

# XXX
function error
    log ...

# XXX
function warn
    log ...

function ajax-get url
    results = void
    $.ajax do
        success: (data, status, xhr) ->
            results := data
        error: (xhr, status, err) ->
            error "Can't get url", url, err
        method: 'GET'
        url: url
        async: false
    results

function to-array
    [.. for &.0]

function array
    [.. for &]

function math-log2 x
    (Math.log x) / Math.log 2

# XXX
function math-round places, n
    mult = Math.pow 10 places
    (Math.round n * mult) / mult

# --- returns empty object unless all selectors were found.
function get-elems selectors
    ret = {}
    for key, selector of selectors
        $elem = $ selector
        if $elem.length
            ret[key] = $elem
        else
            warn "Can't find selector" selector
            return {}
    ret

function is-function x
    typeof! x is 'Function'

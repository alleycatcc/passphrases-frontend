'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, sprintfMod, join, our, out$ = typeof exports != 'undefined' && exports || this, toString$ = {}.toString;
out$.init = init;
out$.log = log;
out$.error = error;
out$.warn = warn;
out$.ajaxGet = ajaxGet;
out$.mathLog2 = mathLog2;
out$.mathRound = mathRound;
out$.getElems = getElems;
out$.sprintf = sprintf;
out$.array = array;
out$.toArray = toArray;
out$.isFunction = isFunction;
$ = require('jquery');
sprintfMod = require('sprintf');
join = require('prelude-ls').join;
our = {
  dev: void 8
};
function init(arg$){
  var dev;
  dev = arg$.dev;
  return our.dev = dev;
}
function sprintf(){
  return sprintfMod.apply(this, arguments);
}
function log(){
  if (our.dev) {
    return bind$(console, 'log').apply(this, arguments);
  } else {
    return alert(join(' ', toArray(arguments)));
  }
}
function error(){
  return log.apply(this, arguments);
}
function warn(){
  return log.apply(this, arguments);
}
function ajaxGet(url){
  var results;
  results = void 8;
  $.ajax({
    success: function(data, status, xhr){
      return results = data;
    },
    error: function(xhr, status, err){
      return error("Can't get url", url, err);
    },
    method: 'GET',
    url: url,
    async: false
  });
  return results;
}
function toArray(){
  var i$, x$, ref$, len$, results$ = [];
  for (i$ = 0, len$ = (ref$ = arguments[0]).length; i$ < len$; ++i$) {
    x$ = ref$[i$];
    results$.push(x$);
  }
  return results$;
}
function array(){
  var i$, x$, len$, results$ = [];
  for (i$ = 0, len$ = (arguments).length; i$ < len$; ++i$) {
    x$ = (arguments)[i$];
    results$.push(x$);
  }
  return results$;
}
function mathLog2(x){
  return Math.log(x) / Math.log(2);
}
function mathRound(places, n){
  var mult;
  mult = Math.pow(10, places);
  return Math.round(n * mult) / mult;
}
function getElems(selectors){
  var ret, key, selector, $elem;
  ret = {};
  for (key in selectors) {
    selector = selectors[key];
    $elem = $(selector);
    if ($elem.length) {
      ret[key] = $elem;
    } else {
      warn("Can't find selector", selector);
      return {};
    }
  }
  return ret;
}
function isFunction(x){
  return toString$.call(x).slice(8, -1) === 'Function';
}
function bind$(obj, key, target){
  return function(){ return (target || obj)[key].apply(obj, arguments) };
}
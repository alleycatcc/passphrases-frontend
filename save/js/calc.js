'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var mustache, getConfig, ref$, log, mathRound, mathLog2, template, our, out$ = typeof exports != 'undefined' && exports || this;
out$.init = init;
out$.getEntropy = getEntropy;
out$.calculate = calculate;
mustache = require('mustache');
getConfig = require('./config').getConfig;
ref$ = require('./util'), log = ref$.log, mathRound = ref$.mathRound, mathLog2 = ref$.mathLog2;
template = require('./template');
our = (function(c){
  return {
    strengths: c.strengths
  };
}.call(this, getConfig()));
function init(){
  return true;
}
function getEntropy(wordCount, numWords){
  var entropyPerWord, entropy;
  entropyPerWord = mathLog2(wordCount);
  entropy = numWords * entropyPerWord;
  return [entropyPerWord, entropy];
}
function calculate(arg$){
  var entropyPerWord, wordCount, numWords, keyspace, guessesPerSecond, seconds, ref$, timeValue, timeUnits, strength;
  entropyPerWord = arg$.entropyPerWord, wordCount = arg$.wordCount, numWords = arg$.numWords;
  keyspace = Math.pow(wordCount, numWords);
  guessesPerSecond = 1e12;
  seconds = (keyspace / 2) / guessesPerSecond;
  return ref$ = getTimeUnits(seconds), timeValue = ref$[0], timeUnits = ref$[1], strength = ref$[2], ref$;
}
function getTimeUnits(seconds){
  switch (false) {
  case !(seconds < 60):
    return [seconds, 'seconds', 0];
  case !(seconds < 60 * 60):
    return [seconds / 60, 'minutes', 0];
  case !(seconds < 60 * 60 * 24):
    return [seconds / 60 / 60, 'hours', 0];
  case !(seconds < 60 * 60 * 24 * 30):
    return [seconds / 60 / 60 / 24, 'days', 0];
  case !(seconds < 60 * 60 * 24 * 365):
    return [seconds / 60 / 60 / 24 / 29.6, 'months', 0];
  case !(seconds < 60 * 60 * 24 * 365 * 1e3):
    return [seconds / 60 / 60 / 24 / 365, 'years', 1];
  case !(seconds < 60 * 60 * 24 * 365 * 1e6):
    return [seconds / 60 / 60 / 24 / 365 / 1e3, 'thousand years', 2];
  case !(seconds < 60 * 60 * 24 * 365 * 1e9):
    return [seconds / 60 / 60 / 24 / 365 / 1e6, 'million years', 2];
  case !(seconds < 60 * 60 * 24 * 365 * 1e12):
    return [seconds / 60 / 60 / 24 / 365 / 1e9, 'billion years', 2];
  case !(seconds < 60 * 60 * 24 * 365 * 1e15):
    return [seconds / 60 / 60 / 24 / 365 / 1e12, 'trillion years', 2];
  case !(seconds < 60 * 60 * 24 * 365 * 1e18):
    return [seconds / 60 / 60 / 24 / 365 / 1e15, 'quadrillion years', 2];
  default:
    return [seconds / 60 / 60 / 24 / 365 / 1e18, 'quintillion years', 2];
  }
}
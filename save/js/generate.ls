'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init

$ = require 'jquery'
random-js = require 'random-js'

{ lines, each, map, join, keys, } = require 'prelude-ls'
mustache = require 'mustache'

{ log, error, array, ajax-get, get-elems, math-round, } = require './util'
{ get-config, } = require './config'
calc = require './calc'
animate-warning-element = require './animate-warning-element'
memorize = require './memorize'
template = require './template'
data = require './data'

our =

    # ---
    #
    # {
    #   meta: xxx
    #   words: xxx
    #   count: xxx
    # }
    # ...
    wordlists: {}

    strengths: void
    color-weak: void
    num-words-min: void
    num-words-max: void

    animate-warnings:
        arrow: void
        generate: void

    memorize-disabled: void

    num-words: void
    word-count: void
    description: void
    entropy-per-word: void
    entropy: void

    time-value: void
    time-units: void
    strength: void
    passphrase: void

    is-warning:
        arrow: false
        generate: false

    elems: {}

function init

    # ---
    #
    # securedrop:
    #     name: 'SecureDrop',
    #     description: 'English wordlist from the SecureDrop project'
    # ...
    { wordlists-meta, default-num-words, selectors, strengths, color-weak, num-words-min, num-words-max, } = get-config()
    our <<< { strengths, num-words-min, num-words-max, color-weak, }
    our.num-words = default-num-words

    calc.init()

    update-entropy()
    init-ui selectors
    init-events()

    { wordlist-keys, } = init-data do
        { wordlists-meta, }
    return error "error data" unless wordlist-keys

    e = our.elems
    populate-wordlists-dropdown do
        wordlist-keys, wordlists-meta, e.$select-wordlists
    set-selected()
    update-wordlist()
    update-num-words()

function populate-wordlists-dropdown keys, wordlists-meta, $select
    keys
    |> map (key) ->
        { name, } = wordlists-meta[key]
        $ '<option>'
            ..val key
            ..html name
    |> each ($option) ->
        $select.append $option

function populate-words-dropdown $select
    [3 to 10]
    |> map (n) ->
        $ '<option>'
            ..val n
            ..html '' + n + ' words'
            ..attr 'selected' 'selected' if n == our.default-num-words
    |> each ($option) ->
        $select.append $option

'''
Locally serving files through ajax is too difficult on Android, so we avoid it.

function get-words-ajax url
    results = ajax-get url
    return error "Can't get url" url unless results
      #wordlists[wordlist].count = wordlists[wordlist].wordlist.length;
    lines results
        # --- remove the trailing blank line
        ..pop()
'''

function set-selected
    void
    #$('select.wordlists option[value="' + Passphrases.prefs.wordlist + '"]').prop('selected', true);
    #    $('select.words option[value="' + Passphrases.prefs.words + '"]').prop('selected', true);

function update-wordlist
    /*
    #save option changes
    #Passphrases.prefs.wordlist = $('select.wordlists').val();
    #Passphrases.prefs.words = $('select.words').val();
    #Passphrases.savePrefs();
    */

    wordlist = get-selected-wordlist()
    { words, count: our.word-count, meta: { our.description, }, } = our.wordlists[wordlist]
    update-text()

function calculate
    e = our.elems
    { num-words, word-count, } = our
    update-text()

    [ our.time-value, our.time-units, our.strength, ] = calc.calculate do
        { our.entropy-per-word, word-count, num-words, }
    set-strength our.strength

function get-selected-wordlist()
    our.elems.$select-wordlists.val()

# --- strength: 0 (weak), 1 (so-so), 2 (strong)
function set-strength strength
    e = our.elems
    s = our.strengths
    $pw = e.$passphrase-wrapper
    window.$ = $
    s |> each (x) -> $pw.remove-class do
        'x--' + x
    $pw.add-class do
        'x--' + s[strength]

    our.animate-warnings |> each (animator) ->
        if strength == 0 then animator.go()
        else animator.stop()

function update-passphrase
    e = our.elems
    e.$passphrase.text do
        our.passphrase = generate-passphrase()
    calculate()
    update-text()
    update-memorize-button()

#xx
    # --- they chose a bad passphrase -- hint the up arrow.
    if our.num-words < 6
        warning-stop-generate()
        warning-start-arrow()
    # --- they chose a good passphrase ...
    else
        # --- ... after having chosen a bad one -- stop the hint.
        if is-warning-generate()
            warning-stop-generate()

function generate-passphrase
    #play dice roll sound effect
    #Passphrases.playSound('generate');

    wordlist = get-selected-wordlist()
    { words, count: word-count, } = our.wordlists[wordlist]
    random = new random-js
    [0 to our.num-words - 1]
    |> map (n) ->
        random-idx = random.integer 0 word-count - 1
        words[random-idx]
    |> join ' '

function init-ui selectors
    e = our.elems
    let u = get-elems selectors.generate
        return error "Can't init generate" unless u
        our.elems <<< u

    update-memorize-button()
    let aw = our.animate-warnings
        aw.generate = animate-warning-element.create do
            $button: e.$button-generate
            $button-inner: e.$button-generate-inner
            color-weak: our.color-weak
            do-font-size: false
            rotator-interval: 4_000
        aw.arrow = animate-warning-element.create do
            $button: e.$num-words-arrow-up
            $button-inner: e.$num-words-arrow-up-inner
            color-weak: our.color-weak
            do-font-size: false
            rotator-interval: 2_500

function init-events
    e = our.elems
    [ e.$select-wordlists, ] |> each (.change update-wordlist)
    e.$num-words-arrow-up-inner.on 'click' ->
        update-num-words 'up'
        update-arrows()
        update-text()
    e.$num-words-arrow-down-inner.on 'click' ->
        update-num-words 'down'
        update-arrows()
        update-text()
    e.$button-generate.click (e) ->
        update-passphrase()

        # --- trying (but failing) to prevent selection of the text on
        # double-click XX
        #e.prevent-default()
        #e.stop-propagation()
        #e.stop-immediate-propagation()
        #false

    e.$button-memorize.click ->
        return if our.memorize-disabled
        memorize.ui-set-passphrase-and-memorize our.passphrase
    e.$button-generate-inner.on 'mouseover' ->
        menu-text-secondary 0
    e.$button-generate-inner.on 'mouseout' ->
        menu-text-secondary null
    e.$button-memorize-inner.on 'mouseover' ->
        return if our.memorize-disabled
        menu-text-secondary 1
    e.$button-memorize-inner.on 'mouseout' ->
        return if our.memorize-disabled
        menu-text-secondary null

function init-data { wordlists-meta, }
    { wordlist-keys, }

    # --- securedrop, english-diceware, ...
    wordlist-keys = (keys wordlists-meta).sort()
    wordlist-count = wordlist-keys.length

    # --- fail if any of the word lists fails.
    num-ok = 0
    wordlist-keys |> each (key) ->
        these-words = data.get-words key
        return error "Can't get words for" key unless these-words
        num-ok := num-ok + 1
        our.wordlists[key] =
            words: these-words
            count: these-words.length
            meta: wordlists-meta[key]
    if num-ok != wordlist-count
        warn "Aborting"
        return {}
    { wordlist-keys, }

function update-num-words which
    e = our.elems
    is-max = our.num-words == our.num-words-max
    is-min = our.num-words == our.num-words-min
    if which == 'up' and not is-max then ++our.num-words
    if which == 'down' and not is-min then --our.num-words
    e.$num-words-num.html our.num-words
    update-entropy()
    update-text()

function update-entropy
    [ our.entropy-per-word, our.entropy, ] = calc.get-entropy our.word-count, our.num-words

function update-arrows
    e = our.elems
    $up = e.$num-words-arrow-up-inner
    $down = e.$num-words-arrow-down-inner
    is-max = our.num-words == our.num-words-max
    is-min = our.num-words == our.num-words-min
    if is-max then $up.attr 'disabled' 'disabled'
    else $up.attr 'disabled' false
    if is-min then $down.attr 'disabled' 'disabled'
    else $down.attr 'disabled' false

    #xx
    # --- clicking generate now will result in a good passphrase ...
    if our.num-words >= 6
        # --- ... though the current one is bad -- stop hinting arrow, hint
        # generate button.
        if is-warning-arrow()
            warning-stop-arrow()
            warning-start-generate()
    # --- clicking generate now will result in a bad passphrase ...
    else
        # --- ... and the current one is bad too -- stop hinting generate
        # button, hint the arrow.
        if is-warning-generate()
            warning-stop-generate()
            warning-start-arrow()

function update-memorize-button
    pp = our.passphrase
    $button = our.elems.$button-memorize
    if not pp or not pp.length
        $button.add-class 'x--disabled'
        our.memorize-disabled = true
    else
        $button.remove-class 'x--disabled'
        our.memorize-disabled = false

# --- there are several (currently 4) parts to the text.
#
# not all args need to be passed every time -- the template will decide what to render.

function update-text
    e = our.elems
    { description, word-count, num-words, strength, time-units, time-value, entropy-per-word, entropy, } = our

    # --- don't forget to quote hyphenated keys.
    template-data =
        description: description

        'word-count': word-count
        'entropy-per-word': math-round 2 that if entropy-per-word

        'num-words': num-words
        'indef-article-uppercase-num-words':
            # --- assumes english.
            if num-words `begins-with` 8 or num-words in [11 18] then 'An'
            else 'A'
        entropy: math-round 2 that if entropy

        'time-value': math-round 1 that if time-value
        'time-units': time-units
        'on-average': if strength < 2 then '' else 'on average '
        'class-strength': 'x--' + our.strengths[that] if strength?
        only: if strength < 2 then 'only ' else ''

    text = mustache.render template.stats(), template-data
    e.$div-wordlist-description.html text

function is-warning-arrow
    our.is-warning.arrow
function is-warning-generate
    our.is-warning.generate
function warning-start-arrow
    our.animate-warnings.arrow.go()
    our.is-warning.arrow = true
function warning-stop-arrow
    our.animate-warnings.arrow.stop()
    our.is-warning.arrow = false
function warning-start-generate
    our.animate-warnings.generate.go()
    our.is-warning.generate = true
function warning-stop-generate
    our.animate-warnings.generate.stop()
    our.is-warning.generate = false

# --- how is null, 0, or 1.
function menu-text-secondary how
    e = our.elems
    $mts = e.$menu-text-secondary
        ..css 'display' 'none'
    return if how == null
    $mts.eq how
        .css 'display' 'inline'

function begins-with target, pattern
    true if (target + '')[0] is (pattern + '')

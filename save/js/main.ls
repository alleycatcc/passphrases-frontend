'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

$ = require 'jquery'

{ log, warn, } = util = require './util'

cppapi = require './cppapi'
nav = require './nav'
generate = require './generate'
memorize = require './memorize'
settings = require './settings'
data = require './data'

our =
    dev: void

$ 'document' .ready ->
    # --- use local paths (not qrc:///) if we're developing.
    if document.location.href == // ^ http //
        $ 'body' .add-class 'dev'
        our.dev = true

    cppapi.init dev: our.dev

    data.init()
    util.init dev: our.dev
    nav.init()
    generate.init()
    memorize.init()
    settings.init dev: our.dev

    make-links-external()

function make-links-external
    on-click = ($a) ->
        return unless href = $a.attr 'href'
        ok = cppapi.open-link href
        warn "Couldn't open external link" href unless ok
    ext-link = ($a) ->
        $a.on 'click' -> on-click $a
    $ 'a' .each (_, a) ->
        ext-link $ a



# TODO

'''
// make shortcut keys like Cmd-Q work in OSX (#4)
if(process.platform === 'darwin') {
  var nativeMenuBar = new gui.Menu({ type: "menubar" });
  nativeMenuBar.createMacBuiltin("Passphrases", {
    hideEdit: true,
    hideWindow: true
  });
  win.menu = nativeMenuBar;
}
'''

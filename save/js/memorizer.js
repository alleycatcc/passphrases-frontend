'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, ref$, log, sprintf, proto, out$ = typeof exports != 'undefined' && exports || this;
out$.create = create;
$ = require('jquery');
ref$ = require('./util'), log = ref$.log, sprintf = ref$.sprintf;
proto = {
  updateStats: updateStats,
  nextTryMaybe: nextTryMaybe,
  nextTry: nextTry,
  countdownComplete: countdownComplete,
  testForSuccess: testForSuccess,
  cleanup: cleanup
};
function create(arg$){
  var passphrase, elems, ui, ref$;
  passphrase = arg$.passphrase, elems = arg$.elems, ui = arg$.ui;
  return ref$ = Object.create(proto), ref$.tries = 0, ref$.delay = 0, ref$.typedWithoutLooking = false, ref$.countdownValues = [60, 120, 300, 600, 1800, 3600], ref$.currentCountdown = 0, ref$.keypressEnabled = false, ref$.passphrase = passphrase, ref$.jobCountdown = void 8, ref$.e = elems, ref$.ui = ui, ref$;
}
function cleanup(){
  var that;
  if (that = this.jobCountdown) {
    return clearInterval(that);
  }
}
function updateStats(){
  switch (false) {
  case this.tries !== 0:
    note(this, 'Type the passphrase');
    break;
  case this.tries !== 1:
    note(this, 'Try to type the passphrase before the hint appears');
    break;
  case !(this.tries < 5 && this.typedWithoutLooking):
    note(this, 'Good job! Try making it to 5 tries');
    break;
  case !(this.tries < 10 && this.typedWithoutLooking):
    note(this, 'Try making it to 10 tries without seeing what you type');
    break;
  case !this.typedWithoutLooking:
    note(this, 'Keep practicing.');
  }
  return this.e.$divMemorizeTries.html(this.tries);
}
function nextTryMaybe(){
  if (!this.keypressEnabled) {
    return;
  }
  this.keypressEnabled = false;
  this.nextTry.apply(this, arguments);
  return true;
}
function nextTry(){
  var x$;
  this.ui.uiHintReset();
  this.ui.uiModeMemorize('active');
  x$ = this.e.$inputPassphraseTest;
  x$.val('');
  x$.focus();
  return this.ui.uiHintShow(this.delay);
}
function countdownComplete(){
  this.ui.uiModeMemorize('ready');
  this.e.$inputPassphraseTest.blur();
  return this.keypressEnabled = true;
}
function testForSuccess(){
  var $input, passphraseTest;
  $input = this.e.$inputPassphraseTest;
  passphraseTest = $input.val();
  if (passphraseTest === this.passphrase) {
    return success(this);
  }
  return failure(this, passphraseTest, $input);
}
function success(ctxt){
  return (function(){
    ++this.tries;
    if (this.typedWithoutLooking) {}
    if (this.e.$divHintProgress.is(':animated')) {
      this.typedWithoutLooking = true;
    }
    if (this.tries === 1) {
      this.delay = 5;
    } else if (this.tries <= 6) {
      ++this.delay;
    }
    if (this.delay <= 10) {
      ++this.delay;
    }
    this.updateStats();
    if (this.tries === 5) {
      this.ui.uiPassphraseType('password');
    }
    if (this.tries < 10) {
      return this.nextTry();
    } else {
      if (this.typedWithoutLooking) {
        return countdown(this);
      } else {
        return this.nextTry();
      }
    }
  }.call(ctxt));
}
function failure(ctxt, passphraseTest, $input){
  return (function(){
    var testLength, typo;
    testLength = passphraseTest.length;
    typo = this.passphrase.indexOf(passphraseTest) !== 0;
    if (typo) {
      return $input.addClass('typo');
    } else {
      return $input.removeClass('typo');
    }
  }.call(ctxt));
}
function note(ctxt, str){
  return (function(){
    return this.e.$divMemorizeNote.html(str);
  }.call(ctxt));
}
function updateCountdown(ctxt, totalSeconds){
  return (function(){
    var seconds, minutes;
    seconds = totalSeconds % 60;
    seconds = sprintf('%02d', seconds);
    minutes = Math.floor(totalSeconds / 60);
    return this.e.$divCountdown.html(minutes + ":" + seconds);
  }.call(ctxt));
}
function countdown(ctxt){
  return (function(){
    var totalSeconds, that, counter, this$ = this;
    this.typedWithoutLooking = false;
    this.ui.uiModeMemorize('waiting');
    note(this, 'Give your mind a rest');
    totalSeconds = this.countdownValues[this.currentCountdown];
    updateCountdown(this, totalSeconds);
    if (that = this.jobCountdown) {
      clearInterval(that);
    }
    counter = function(){
      --totalSeconds;
      if (totalSeconds >= 0) {
        return updateCountdown(this$, totalSeconds);
      }
      if (this$.currentCountdown < this$.countdownValues.length - 1) {
        ++this$.currentCountdown;
      }
      log('Time to keep practicing your passphrase!');
      clearInterval(this$.jobCountdown);
      return this$.countdownComplete();
    };
    return this.jobCountdown = setInterval(counter, 1000);
  }.call(ctxt));
}
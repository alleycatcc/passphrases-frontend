'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init
    ui-set-passphrase-and-memorize

$ = require 'jquery'

{ log, error, get-elems, } = require './util'
{ get-config, } = require './config'
memorizer = require './memorizer'
nav = require './nav'

our =
    elems: {}
    passphrase: void

    # xxx
    mobile: -> $ 'body' .width() < 601

window.XXX = our.mobile

function init
    let u = get-elems get-config().selectors.memorize
        return error "Can't init memorize" unless u
        our.elems <<< u

    init-ui()
    init-events()

function init-ui
    ui-mode-input()
    update-button-memorize()

    if our.mobile() then $ 'body' .height 2000

function ui-passphrase-type type
    e = our.elems
    e.$input-passphrase-test.attr 'type' type
    nav.switch-to 1

function ui-mode-input
    e = our.elems

    e.$div-memorize-input.show()
    e.$div-memorize-go.hide()

function ui-mode-go passphrase
    e = our.elems
    e.$div-memorize-input.hide()
    e.$div-memorize-go.show()
    ui-mode-memorize 'active'
    ui-passphrase-type 'text'
    e.$div-hint-show.text passphrase

function ui-hint-reset
    e = our.elems
    e.$div-hint-progress
        ..stop()
        ..css 'width' '0%'
    e.$div-hint-progress-wrapper
        ..show()
    e.$div-hint-show
        ..hide()

function ui-hint-show delay

    # xxx
    if our.mobile() then delay *= 4
    console.log 'mobile' our.mobile()

    e = our.elems
    animate = width: '100%'
    e.$div-hint-progress.animate do
        animate
        delay * 1000
        'linear'
        ->
            e.$div-hint-progress-wrapper.hide()
            e.$div-hint-show.show()

function ui-mode-memorize mode
    e = our.elems
    divs =
        active: e.$div-memorize-active
        waiting: e.$div-memorize-waiting
        ready: e.$div-memorize-ready
    divs.active.hide()
    divs.waiting.hide()
    divs.ready.hide()

    divs[mode].show()

function init-events
    e = our.elems

    submit = ->
        if get-passphrase()
            start-memorizing that
        else
            typo()
            focus-input-passphrase()
        false

    # --- submit on 'enter' or click.
    e.$form-memorize.submit submit
    e.$button-start-memorizing.click submit

    e.$button-cancel.click ->
        ui-mode-input()
        e.$input-passphrase.focus()
        if our.mem
            that.cleanup()

    e.$button-skip-countdown.on 'click' ->
        return unless mem = our.mem
        mem.countdown-complete()

    e.$input-passphrase-test.on 'input' ->
        return unless mem = our.mem
        mem.test-for-success()

    e.$input-passphrase.on 'input' ->
        our.passphrase = $ @ .val()
        update-button-memorize()

    $ window .on 'keypress' (e) ->
        return unless mem = our.mem
        kc = e.key-code
        # --- 32 = space; is 0 necessary??
        return unless kc == 0 or kc == 32
        false if mem.next-try-maybe()

function ui-set-passphrase-and-memorize passphrase
    e = our.elems
    our <<< { passphrase, }
    e.$input-passphrase.text passphrase
    start-memorizing passphrase

function start-memorizing passphrase
    e = our.elems
    ui-mode-go passphrase

    if our.mem
        that.cleanup()

    memorizer-params =
        passphrase: passphrase
        elems: our.elems
        ui: {
            ui-hint-reset
            ui-mode-memorize
            ui-hint-show
            ui-passphrase-type
        }
    our.mem = memorizer.create memorizer-params
        ..update-stats()
        ..next-try()

function get-passphrase
    our.elems.$input-passphrase.val()

function typo
    e = our.elems
    pp = e.$input-passphrase
    pp.add-class 'typo'
    set-timeout do
        -> pp.remove-class 'typo'
        100

function focus-input-passphrase
    e = our.elems
    e.$input-passphrase.focus()

function update-button-memorize
    e = our.elems
    pp = our.passphrase
    $button = e.$button-start-memorizing
    if pp and pp.length
        $button.remove-class 'x--disabled'
    else
        $button.add-class 'x--disabled'






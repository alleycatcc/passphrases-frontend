'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, ref$, map, each, getConfig, log, getElems, our, out$ = typeof exports != 'undefined' && exports || this;
out$.init = init;
out$.switchTo = switchTo;
$ = require('jquery');
ref$ = require('prelude-ls'), map = ref$.map, each = ref$.each;
getConfig = require('./config').getConfig;
ref$ = require('./util'), log = ref$.log, getElems = ref$.getElems;
our = {
  handlers: [],
  elems: {}
};
function init(){
  var selectors;
  selectors = getConfig().selectors;
  (function(u){
    if (!u) {
      return error("Can't init nav");
    }
    import$(our.elems, u);
  }.call(this, getElems(selectors.nav)));
  return initEvents();
}
function initEvents(){
  var e, $tabs, $contents, handlers;
  e = our.elems;
  $tabs = e.$nav;
  $contents = e.$contents;
  handlers = our.handlers;
  each(function(i){
    var $tab, $content, onclick;
    $tab = $tabs.eq(i);
    $content = $contents.eq(i);
    onclick = function(){
      $tabs.removeClass('x--active');
      $contents.removeClass('x--active');
      $tab.addClass('x--active');
      return $content.addClass('x--active');
    };
    $tab.click(onclick);
    return handlers.push(onclick);
  })(
  [0, 1, 2]);
  $tabs.hover(function(){
    return $(this).addClass('hover');
  }, function(){
    return $(this).removeClass('hover');
  });
  $tabs.on('touchstart', function(){});
  $tabs.on('touchend', function(){});
  $tabs.eq(1).click(function(){
    if ($('.memorize-input').is(':visible')) {
      return $('.passphrase-to-memorize').focus();
    } else {
      return $('.passphrase-test').focus();
    }
  });
  return switchTo(0);
}
function switchTo(n){
  return our.handlers[n]();
}
'// let the outside world switch to memorize tab\nPassphrases.tabMemorize = function(){\n  clickMemorizeTab();\n};';
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
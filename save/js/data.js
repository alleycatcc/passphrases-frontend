'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var warn, our, out$ = typeof exports != 'undefined' && exports || this;
warn = require('./util').warn;
our = {
  securedrop: require('./data/wordlists/securedrop'),
  english: require('./data/wordlists/english'),
  catalan: require('./data/wordlists/catalan'),
  german: require('./data/wordlists/german'),
  french: require('./data/wordlists/french'),
  italian: require('./data/wordlists/italian'),
  japanese: require('./data/wordlists/japanese'),
  dutch: require('./data/wordlists/dutch'),
  polish: require('./data/wordlists/polish'),
  swedish: require('./data/wordlists/swedish')
};
out$.init = init;
out$.getWords = getWords;
function init(){
  return true;
}
function getWords(wordlistName){
  var list;
  list = our[wordlistName];
  if (!list) {
    warn("Can't get list for", wordlistName);
    return [];
  }
  return list;
}
'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, randomJs, ref$, lines, each, map, join, keys, mustache, log, error, array, ajaxGet, getElems, mathRound, getConfig, calc, animateWarningElement, memorize, template, data, our, out$ = typeof exports != 'undefined' && exports || this;
out$.init = init;
$ = require('jquery');
randomJs = require('random-js');
ref$ = require('prelude-ls'), lines = ref$.lines, each = ref$.each, map = ref$.map, join = ref$.join, keys = ref$.keys;
mustache = require('mustache');
ref$ = require('./util'), log = ref$.log, error = ref$.error, array = ref$.array, ajaxGet = ref$.ajaxGet, getElems = ref$.getElems, mathRound = ref$.mathRound;
getConfig = require('./config').getConfig;
calc = require('./calc');
animateWarningElement = require('./animate-warning-element');
memorize = require('./memorize');
template = require('./template');
data = require('./data');
our = {
  wordlists: {},
  strengths: void 8,
  colorWeak: void 8,
  numWordsMin: void 8,
  numWordsMax: void 8,
  animateWarnings: {
    arrow: void 8,
    generate: void 8
  },
  memorizeDisabled: void 8,
  numWords: void 8,
  wordCount: void 8,
  description: void 8,
  entropyPerWord: void 8,
  entropy: void 8,
  timeValue: void 8,
  timeUnits: void 8,
  strength: void 8,
  passphrase: void 8,
  isWarning: {
    arrow: false,
    generate: false
  },
  elems: {}
};
function init(){
  var ref$, wordlistsMeta, defaultNumWords, selectors, strengths, colorWeak, numWordsMin, numWordsMax, wordlistKeys, e;
  ref$ = getConfig(), wordlistsMeta = ref$.wordlistsMeta, defaultNumWords = ref$.defaultNumWords, selectors = ref$.selectors, strengths = ref$.strengths, colorWeak = ref$.colorWeak, numWordsMin = ref$.numWordsMin, numWordsMax = ref$.numWordsMax;
  our.strengths = strengths;
  our.numWordsMin = numWordsMin;
  our.numWordsMax = numWordsMax;
  our.colorWeak = colorWeak;
  our.numWords = defaultNumWords;
  calc.init();
  updateEntropy();
  initUi(selectors);
  initEvents();
  wordlistKeys = initData({
    wordlistsMeta: wordlistsMeta
  }).wordlistKeys;
  if (!wordlistKeys) {
    return error("error data");
  }
  e = our.elems;
  populateWordlistsDropdown(wordlistKeys, wordlistsMeta, e.$selectWordlists);
  setSelected();
  updateWordlist();
  return updateNumWords();
}
function populateWordlistsDropdown(keys, wordlistsMeta, $select){
  return each(function($option){
    return $select.append($option);
  })(
  map(function(key){
    var name, x$;
    name = wordlistsMeta[key].name;
    x$ = $('<option>');
    x$.val(key);
    x$.html(name);
    return x$;
  })(
  keys));
}
function populateWordsDropdown($select){
  return each(function($option){
    return $select.append($option);
  })(
  map(function(n){
    var x$;
    x$ = $('<option>');
    x$.val(n);
    x$.html('' + n + ' words');
    if (n === our.defaultNumWords) {
      x$.attr('selected', 'selected');
    }
    return x$;
  })(
  [3, 4, 5, 6, 7, 8, 9, 10]));
}
'Locally serving files through ajax is too difficult on Android, so we avoid it.\n\nfunction get-words-ajax url\n    results = ajax-get url\n    return error "Can\'t get url" url unless results\n      #wordlists[wordlist].count = wordlists[wordlist].wordlist.length;\n    lines results\n        # --- remove the trailing blank line\n        ..pop()';
function setSelected(){}
function updateWordlist(){
  /*
  #save option changes
  #Passphrases.prefs.wordlist = $('select.wordlists').val();
  #Passphrases.prefs.words = $('select.words').val();
  #Passphrases.savePrefs();
  */
  var wordlist, ref$, words;
  wordlist = getSelectedWordlist();
  ref$ = our.wordlists[wordlist], words = ref$.words, our.wordCount = ref$.count, our.description = ref$.meta.description;
  return updateText();
}
function calculate(){
  var e, numWords, wordCount, ref$;
  e = our.elems;
  numWords = our.numWords, wordCount = our.wordCount;
  updateText();
  ref$ = calc.calculate({
    entropyPerWord: our.entropyPerWord,
    wordCount: wordCount,
    numWords: numWords
  }), our.timeValue = ref$[0], our.timeUnits = ref$[1], our.strength = ref$[2];
  return setStrength(our.strength);
}
function getSelectedWordlist(){
  return our.elems.$selectWordlists.val();
}
function setStrength(strength){
  var e, s, $pw;
  e = our.elems;
  s = our.strengths;
  $pw = e.$passphraseWrapper;
  window.$ = $;
  each(function(x){
    return $pw.removeClass('x--' + x);
  })(
  s);
  $pw.addClass('x--' + s[strength]);
  return each(function(animator){
    if (strength === 0) {
      return animator.go();
    } else {
      return animator.stop();
    }
  })(
  our.animateWarnings);
}
function updatePassphrase(){
  var e;
  e = our.elems;
  e.$passphrase.text(our.passphrase = generatePassphrase());
  calculate();
  updateText();
  updateMemorizeButton();
  if (our.numWords < 6) {
    warningStopGenerate();
    return warningStartArrow();
  } else {
    if (isWarningGenerate()) {
      return warningStopGenerate();
    }
  }
}
function generatePassphrase(){
  var wordlist, ref$, words, wordCount, random;
  wordlist = getSelectedWordlist();
  ref$ = our.wordlists[wordlist], words = ref$.words, wordCount = ref$.count;
  random = new randomJs;
  return join(' ')(
  map(function(n){
    var randomIdx;
    randomIdx = random.integer(0, wordCount - 1);
    return words[randomIdx];
  })(
  (function(){
    var i$, to$, results$ = [];
    for (i$ = 0, to$ = our.numWords - 1; i$ <= to$; ++i$) {
      results$.push(i$);
    }
    return results$;
  }())));
}
function initUi(selectors){
  var e;
  e = our.elems;
  (function(u){
    if (!u) {
      return error("Can't init generate");
    }
    import$(our.elems, u);
  }.call(this, getElems(selectors.generate)));
  updateMemorizeButton();
  return (function(aw){
    aw.generate = animateWarningElement.create({
      $button: e.$buttonGenerate,
      $buttonInner: e.$buttonGenerateInner,
      colorWeak: our.colorWeak,
      doFontSize: false,
      rotatorInterval: 4000
    });
    return aw.arrow = animateWarningElement.create({
      $button: e.$numWordsArrowUp,
      $buttonInner: e.$numWordsArrowUpInner,
      colorWeak: our.colorWeak,
      doFontSize: false,
      rotatorInterval: 2500
    });
  }.call(this, our.animateWarnings));
}
function initEvents(){
  var e, this$ = this;
  e = our.elems;
  each(function(it){
    return it.change(updateWordlist);
  })(
  [e.$selectWordlists]);
  e.$numWordsArrowUpInner.on('click', function(){
    updateNumWords('up');
    updateArrows();
    return updateText();
  });
  e.$numWordsArrowDownInner.on('click', function(){
    updateNumWords('down');
    updateArrows();
    return updateText();
  });
  e.$buttonGenerate.click(function(e){
    return updatePassphrase();
  });
  e.$buttonMemorize.click(function(){
    if (our.memorizeDisabled) {
      return;
    }
    return memorize.uiSetPassphraseAndMemorize(our.passphrase);
  });
  e.$buttonGenerateInner.on('mouseover', function(){
    return menuTextSecondary(0);
  });
  e.$buttonGenerateInner.on('mouseout', function(){
    return menuTextSecondary(null);
  });
  e.$buttonMemorizeInner.on('mouseover', function(){
    if (our.memorizeDisabled) {
      return;
    }
    return menuTextSecondary(1);
  });
  return e.$buttonMemorizeInner.on('mouseout', function(){
    if (our.memorizeDisabled) {
      return;
    }
    return menuTextSecondary(null);
  });
}
function initData(arg$){
  var wordlistsMeta, wordlistKeys, wordlistCount, numOk;
  wordlistsMeta = arg$.wordlistsMeta;
  ({
    wordlistKeys: wordlistKeys
  });
  wordlistKeys = keys(wordlistsMeta).sort();
  wordlistCount = wordlistKeys.length;
  numOk = 0;
  each(function(key){
    var theseWords;
    theseWords = data.getWords(key);
    if (!theseWords) {
      return error("Can't get words for", key);
    }
    numOk = numOk + 1;
    return our.wordlists[key] = {
      words: theseWords,
      count: theseWords.length,
      meta: wordlistsMeta[key]
    };
  })(
  wordlistKeys);
  if (numOk !== wordlistCount) {
    warn("Aborting");
    return {};
  }
  return {
    wordlistKeys: wordlistKeys
  };
}
function updateNumWords(which){
  var e, isMax, isMin;
  e = our.elems;
  isMax = our.numWords === our.numWordsMax;
  isMin = our.numWords === our.numWordsMin;
  if (which === 'up' && !isMax) {
    ++our.numWords;
  }
  if (which === 'down' && !isMin) {
    --our.numWords;
  }
  e.$numWordsNum.html(our.numWords);
  updateEntropy();
  return updateText();
}
function updateEntropy(){
  var ref$;
  return ref$ = calc.getEntropy(our.wordCount, our.numWords), our.entropyPerWord = ref$[0], our.entropy = ref$[1], ref$;
}
function updateArrows(){
  var e, $up, $down, isMax, isMin;
  e = our.elems;
  $up = e.$numWordsArrowUpInner;
  $down = e.$numWordsArrowDownInner;
  isMax = our.numWords === our.numWordsMax;
  isMin = our.numWords === our.numWordsMin;
  if (isMax) {
    $up.attr('disabled', 'disabled');
  } else {
    $up.attr('disabled', false);
  }
  if (isMin) {
    $down.attr('disabled', 'disabled');
  } else {
    $down.attr('disabled', false);
  }
  if (our.numWords >= 6) {
    if (isWarningArrow()) {
      warningStopArrow();
      return warningStartGenerate();
    }
  } else {
    if (isWarningGenerate()) {
      warningStopGenerate();
      return warningStartArrow();
    }
  }
}
function updateMemorizeButton(){
  var pp, $button;
  pp = our.passphrase;
  $button = our.elems.$buttonMemorize;
  if (!pp || !pp.length) {
    $button.addClass('x--disabled');
    return our.memorizeDisabled = true;
  } else {
    $button.removeClass('x--disabled');
    return our.memorizeDisabled = false;
  }
}
function updateText(){
  var e, description, wordCount, numWords, strength, timeUnits, timeValue, entropyPerWord, entropy, templateData, that, text;
  e = our.elems;
  description = our.description, wordCount = our.wordCount, numWords = our.numWords, strength = our.strength, timeUnits = our.timeUnits, timeValue = our.timeValue, entropyPerWord = our.entropyPerWord, entropy = our.entropy;
  templateData = {
    description: description,
    'word-count': wordCount,
    'entropy-per-word': (that = entropyPerWord) ? mathRound(2, that) : void 8,
    'num-words': numWords,
    'indef-article-uppercase-num-words': beginsWith(numWords, 8) || (numWords === 11 || numWords === 18) ? 'An' : 'A',
    entropy: (that = entropy) ? mathRound(2, that) : void 8,
    'time-value': (that = timeValue) ? mathRound(1, that) : void 8,
    'time-units': timeUnits,
    'on-average': strength < 2 ? '' : 'on average ',
    'class-strength': (that = strength) != null ? 'x--' + our.strengths[that] : void 8,
    only: strength < 2 ? 'only ' : ''
  };
  text = mustache.render(template.stats(), templateData);
  return e.$divWordlistDescription.html(text);
}
function isWarningArrow(){
  return our.isWarning.arrow;
}
function isWarningGenerate(){
  return our.isWarning.generate;
}
function warningStartArrow(){
  our.animateWarnings.arrow.go();
  return our.isWarning.arrow = true;
}
function warningStopArrow(){
  our.animateWarnings.arrow.stop();
  return our.isWarning.arrow = false;
}
function warningStartGenerate(){
  our.animateWarnings.generate.go();
  return our.isWarning.generate = true;
}
function warningStopGenerate(){
  our.animateWarnings.generate.stop();
  return our.isWarning.generate = false;
}
function menuTextSecondary(how){
  var e, x$, $mts;
  e = our.elems;
  x$ = $mts = e.$menuTextSecondary;
  x$.css('display', 'none');
  if (how === null) {
    return;
  }
  return $mts.eq(how).css('display', 'inline');
}
function beginsWith(target, pattern){
  if ((target + '')[0] === pattern + '') {
    return true;
  }
}
function import$(obj, src){
  var own = {}.hasOwnProperty;
  for (var key in src) if (own.call(src, key)) obj[key] = src[key];
  return obj;
}
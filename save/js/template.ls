'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    stats

function stats
    '''
        <div class="c-guess">
            {{#description}}
            <span class='c-guess__description'>{{{description}}}</span>
            {{/description}}

            {{#word-count}}
            <p><span class='c-guess__stats'>(<span class='c-guess__quantity'>{{word-count}}</span> words → <span class='c-guess__quantity'>{{entropy-per-word}}</span> bits of entropy per word)</span></p>
            {{/word-count}}

            {{#num-words}}
            <p>{{indef-article-uppercase-num-words}} <span class='c-guess__quantity'>{{num-words}}-word</span> passphrase yields <span class='c-guess__quantity'>{{entropy}}</span> bits of entropy.</p>
            {{/num-words}}

            {{#time-value}}
            <p class="x__estimate">It will take an adversary {{on-average}} <span class='c-guess__quantity {{class-strength}}'>{{only}}{{time-value}} {{time-units}}</span> to guess your passphrase, guessing at one trillion guesses per second.</p>
            {{/time-value}}
        </div>
    '''

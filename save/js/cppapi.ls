'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init
    save-preferences
    quit
    notify
    open-link
    test

{ log, error, sprintf, array, is-function, } = require './util'

# --- wormhole names must match c++.
#
# 'full' wormhole provides two-way c++ <-> js communication.
#
# not available on for example android.
#
# 'half' wormhole provides one-way c++ -> js communication.
#
# always available, and c++ will poll periodically.

config =
    wormhole-full-name: 'wormhole-full'
    wormhole-half-name: 'wormhole-half'

our =
    dev: void
    wormhole-full: void
    wormhold-half: void

function init { our.dev = false, } = {}
    return true if our.dev

    init-wormhole-full ({ err, wormhole-full, }) ->
        if err then error sprintf "Can't init cppapi: missing channel ('%s')" config.wormhole-full-name

        # --- if undefined we don't have it (e.g. android).
        our <<< { wormhole-full, }

    init-wormhole-half ({ wormhole-half, }) ->
        our <<< { wormhole-half, }

    # test
    id = 0
    secs = 3
    fun = ->
        id := id + 1
        secs := secs + 3
        our.wormhole-half.push do
            show:
                tag: 'my own tag'
                id: id
                text: "time to practize (#id)!"
        # timeout-s secs, fun
    timeout-s secs, fun

    true

function save-preferences prefs
    api-full 'savePreferences' prefs

function quit
    api-full 'quit'

function notify
    api-full 'notify'

function open-link url
    api-full 'openLink' url

function api-full funcname, ...params
    return true if our.dev

    # --- don't have it yet, or something is broken.
    return unless our.wormhole-full

    return error "Missing function" unless funcname
    return error sprintf "Invalid symbol (%s)" funcname unless api-func = our.wormhole-full[funcname]
    return error sprintf "Not a function (%s) (is a %s)" funcname, typeof! api-func unless is-function api-func

    # --- won't necessarily catch everything, though.
    #
    # return val? xx
    try
        api-func.apply null params
    catch e
        return error "Can't call api function" e
    true

function test
    wormhole-full.ping time: Date.now()

# --- old (webkit)
# function is-callback-function x
#    typeof! x is 'CallbackFunction'

function init-wormhole-full done
    # ---
    #
    # `QWebChannel`: qwebchannel.js
    # `qt`: defined if QT has actually provided a WebChannel.

    # --- ok, no channel -- not an error.
    return done {} unless qt?

    new QWebChannel qt.webChannelTransport, (channel) ->
        wormhole-full = channel.objects[config.wormhole-full-name]

        '''
        # --- connect to signal.
        foo.signal-name.connect (msg) ->

        # --- invoke method, async.
        foo.method-name "bar" (ret) ->
        '''

        err = not wormhole-full?
        done { err, wormhole-full, }

function init-wormhole-half done
    wh = window[config.wormhole-half-name] = wormhole-half()
    done wormhole-half: wh

function timeout-s secs, f
    set-timeout f, 1000 * secs

function wormhole-half
    queue = []
    clear: ->
        queue := []
    get: ->
        queue
    push: (x) -> queue.push x


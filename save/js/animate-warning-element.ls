'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    create

$ = require 'jquery'
# --- so the color plugin works.
global.jQuery = $
jquery-color = require 'jquery-color/jquery.color'
{ animate-series, } = require './animate'
{ each, } = require 'prelude-ls'
{ log, error, array, } = require './util'

proto =
    # --- instance methods.
    go: go
    stop: stop

function create args
    {
        $button, $button-inner, color-weak,
        do-font-size = true,
        rotator-interval = 10_000,
        rotator-duration = 500,
    } = args
    color-orig = $button.css 'color'
    font-size-orig-px = $button-inner.css 'font-size'
    m = font-size-orig-px == // ^ (.+) px $ //
    return error "bad value #font-size-orig-px" unless m

    font-size-orig = m.1

    (Object.create proto) <<<
        # --- instance variables.
        color-orig: color-orig
        font-size-orig: font-size-orig
        timeouts: []
        started: false
        animate:
            font-size: if do-font-size then animate-series do
                #$elem: $button-inner
                $elem: $button
                props: array do
                    *   'font-size': font-size-orig * 1.3
                    *   'font-size': font-size-orig
                    *   'font-size': font-size-orig
                props-orig: 'font-size': font-size-orig
                duration: [400 400 5000]
                repeat: true
            color: animate-series do
                $elem: $button
                props: array do
                    *   color: color-weak
                props-orig: color: color-orig
                duration: [3000]
            rotator: rotator do
                $elem: $button-inner
                interval: rotator-interval
                duration: rotator-duration

function go
    return if @started
    @started = true

    a = @animate
    if a.font-size then @timeouts.push set-timeout do
        -> a.font-size.go()
        4000
    a.color.go()
    a.rotator.go()

function stop
    a = @animate
    @timeouts.each |> clear-timeout
    @timeouts = []
    that.stop() if a.font-size
    a.color.stop()
    a.rotator.stop()
    @started = false

function rotator { $elem, interval, duration, }
    rotate = ->
        $elem.add-class 'x--rotate'
    unrotate = ->
        $elem.remove-class 'x--rotate'

    timeouts: []
    go: -> @next()
    next: ->
        @timeouts.push set-timeout do
            ~>
                rotate()
            interval
        @timeouts.push set-timeout do
            ~>
                unrotate()
                @next()
            duration + interval
    stop: ->
        @timeouts |> each clear-timeout
        @timeouts = []
        unrotate()



'''
$button.css 'dummy' 0
# --- final degree value.
props = dummy: 359
prev-class = void
$button.animate props,
    duration: 100,
    step: (x) ->
        $button.remove-class that if prev-class
        $button.add-class (prev-class = 'u-rotate--' + x)
'''


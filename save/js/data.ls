'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

{ warn, } = require './util'

our =
    securedrop: require './data/wordlists/securedrop'
    english:    require './data/wordlists/english'
    catalan:    require './data/wordlists/catalan'
    german:     require './data/wordlists/german'
    french:     require './data/wordlists/french'
    italian:    require './data/wordlists/italian'
    japanese:   require './data/wordlists/japanese'
    dutch:      require './data/wordlists/dutch'
    polish:     require './data/wordlists/polish'
    swedish:    require './data/wordlists/swedish'

export
    init
    get-words

function init
    true

function get-words wordlist-name
    list = our[wordlist-name]
    if not list
        warn "Can't get list for" wordlist-name
        return []
    list

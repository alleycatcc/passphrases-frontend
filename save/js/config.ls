'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    get-config

config =
    wordlists-meta:
        # --- don't forget to quote the key if it has a hyphen.
        securedrop:
            name: 'SecureDrop'
            description: 'English wordlist from the SecureDrop project'
        english:
            name: 'English Diceware'
            description: 'Original English Diceware wordlist'
        catalan:
            name: 'Catalan Diceware'
            description: 'Diccionari catal&agrave; per Marcel Hernandez - CC-BY 4.0'
        german:
            name: 'Deutsch Diceware'
            description: 'Deutsch W&ouml;rterbuch, von Benjamin Tenne - GPL'
        french:
            name: 'Fran&ccedil;ais Diceware'
            description: 'Dictionnaire fran&ccedil;ais par Matthieu Weber'
        italian:
            name: 'Italiano Diceware'
            description: 'Lista di parole Diceware in Italiano by Tarin Gamberini - GPL'
        japanese:
            name: 'Japanese Diceware'
            description: 'Japanese Diceware by Hiroshi Yuki &amp; J Greely - CC-BY-SA'
        dutch:
            name: 'Dutch Diceware'
            description: 'Nederlands woordenboek door Bart Van den Eynde - GPL'
        polish:
            name: 'Polski Diceware'
            description: 'Polski S&lstrok;ownik przez Piotr (DrFugazi) Tarnowski'
        swedish:
            name: 'Svensk Diceware'
            description: 'Svensk ordbok av Magnus Bodin'
    default-num-words: 7
    selectors:
        nav:
            $nav: '.c-nav__generate, .c-nav__memorize, .c-nav__settings'
            $contents: '.b-generate, .b-memorize, .b-settings'
        generate:
            $select-wordlists: '.c-wordlists'
            $num-words:
                '.b-generate__select .x__num-words'
            $num-words-num:
                '.b-generate__select .x__num-words .x__num'
            $num-words-arrow-up:
                '.b-generate__select .x__arrow-up'
            $num-words-arrow-up-inner:
                '.b-generate__select .x__arrow-up .x__inner'
            $num-words-arrow-down:
                '.b-generate__select .x__arrow-down'
            $num-words-arrow-down-inner:
                '.b-generate__select .x__arrow-down .x__inner'
            $button-generate:
                '.b-generate__generate'
            $button-generate-inner:
                '.b-generate__generate .x__inner'
            $button-memorize:
                '.b-generate__memorize'
            $button-memorize-inner:
                '.b-generate__memorize .x__inner'
            $menu-text-secondary:
                '.x__menu-text--secondary span'
            $passphrase:
                '.b-generate__passphrase'
            $passphrase-wrapper:
                '.b-generate__passphrase-wrapper'
            $div-wordlist-description:
                '.b-generate__wordlist-description'
        memorize:
            $div-memorize-go: '.memorize-go'
            $div-memorize-active: '.memorize-active'
            $div-memorize-waiting: '.memorize-waiting'
            $div-memorize-ready: '.memorize-ready'
            $div-memorize-input: '.memorize-input'
            $div-memorize-note: '.memorize-note'
            $div-memorize-tries: '.memorize-tries'
            $div-hint-progress: '.hint-progress'
            $div-hint-progress-wrapper: '.hint-progress-wrapper'
            $div-hint: '.hint'
            $div-hint-show: '.hint-show'
            $div-countdown: '.countdown'
            $input-passphrase: '.passphrase-to-memorize'
            $input-passphrase-test: '.passphrase-test'
            $button-start-memorizing: '.c-button-start-memorizing'
            $button-skip-countdown: '.button-skip-countdown'
            $button-cancel: '.button-cancel'
            $form-memorize: '.memorize-input-form'
        settings:
            $buttons: '.b-settings button'
            $button-save: '.b-settings__save'
            $button-quit: '.b-settings__quit'
            $button-test-notify: '.b-settings__test-notify'
    #path-wordlists: 'data/wordlists'
    strengths: <[ weak soso strong ]>
    num-words-min: 3
    num-words-max: 10
    # --- must match less.
    color-weak: '#900'

function get-config
    config

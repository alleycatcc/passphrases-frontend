'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init
    switch-to

$ = require 'jquery'
{ map, each, } = require 'prelude-ls'

{ get-config, } = require './config'
{ log, get-elems, } = require './util'

our =
    handlers: []
    elems: {}

function init
    { selectors, } = get-config()
    let u = get-elems selectors.nav
        return error "Can't init nav" unless u
        our.elems <<< u

    init-events()

function init-events
    e = our.elems

    # --- assume all 3 aligned.
    $tabs = e.$nav
    $contents = e.$contents
    handlers = our.handlers

    [0 to 2] |> each (i) ->
        $tab = $tabs.eq i
        $content = $contents.eq i

        onclick = ->
            # --- all.
            $tabs.remove-class 'x--active'
            $contents.remove-class 'x--active'
            # --- this.
            $tab.add-class 'x--active'
            $content.add-class 'x--active'
        $tab.click onclick
        handlers.push onclick

    $tabs.hover do
        -> $ @ .add-class 'hover'
        -> $ @ .remove-class 'hover'

    $tabs.on 'touchstart' ->
        #$ @ .add-class 'touch'
    $tabs.on 'touchend' ->
        #$ @ .remove-class 'touch'

    # --- memorize.
    $tabs.eq 1 .click ->
        if $ '.memorize-input' .is ':visible'
            $ '.passphrase-to-memorize' .focus()
        else
            $ '.passphrase-test' .focus()

    # --- start with generate.
    switch-to 0

function switch-to n
    our.handlers[n]()


'''
  // let the outside world switch to memorize tab
  Passphrases.tabMemorize = function(){
    clickMemorizeTab();
  };
'''


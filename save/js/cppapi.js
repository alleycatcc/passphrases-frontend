'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var ref$, log, error, sprintf, array, isFunction, config, our, out$ = typeof exports != 'undefined' && exports || this, toString$ = {}.toString;
out$.init = init;
out$.savePreferences = savePreferences;
out$.quit = quit;
out$.notify = notify;
out$.openLink = openLink;
out$.test = test;
ref$ = require('./util'), log = ref$.log, error = ref$.error, sprintf = ref$.sprintf, array = ref$.array, isFunction = ref$.isFunction;
config = {
  wormholeFullName: 'wormhole-full',
  wormholeHalfName: 'wormhole-half'
};
our = {
  dev: void 8,
  wormholeFull: void 8,
  wormholdHalf: void 8
};
function init(arg$){
  var ref$, id, secs, fun;
  our.dev = (ref$ = (arg$ != null
    ? arg$
    : {}).dev) != null ? ref$ : false;
  if (our.dev) {
    return true;
  }
  initWormholeFull(function(arg$){
    var err, wormholeFull;
    err = arg$.err, wormholeFull = arg$.wormholeFull;
    if (err) {
      error(sprintf("Can't init cppapi: missing channel ('%s')", config.wormholeFullName));
    }
    return our.wormholeFull = wormholeFull, our;
  });
  initWormholeHalf(function(arg$){
    var wormholeHalf;
    wormholeHalf = arg$.wormholeHalf;
    return our.wormholeHalf = wormholeHalf, our;
  });
  id = 0;
  secs = 3;
  fun = function(){
    id = id + 1;
    secs = secs + 3;
    return our.wormholeHalf.push({
      show: {
        tag: 'my own tag',
        id: id,
        text: "time to practize (" + id + ")!"
      }
    });
  };
  timeoutS(secs, fun);
  return true;
}
function savePreferences(prefs){
  return apiFull('savePreferences', prefs);
}
function quit(){
  return apiFull('quit');
}
function notify(){
  return apiFull('notify');
}
function openLink(url){
  return apiFull('openLink', url);
}
function apiFull(funcname){
  var params, res$, i$, to$, apiFunc, e;
  res$ = [];
  for (i$ = 1, to$ = arguments.length; i$ < to$; ++i$) {
    res$.push(arguments[i$]);
  }
  params = res$;
  if (our.dev) {
    return true;
  }
  if (!our.wormholeFull) {
    return;
  }
  if (!funcname) {
    return error("Missing function");
  }
  if (!(apiFunc = our.wormholeFull[funcname])) {
    return error(sprintf("Invalid symbol (%s)", funcname));
  }
  if (!isFunction(apiFunc)) {
    return error(sprintf("Not a function (%s) (is a %s)", funcname, toString$.call(apiFunc).slice(8, -1)));
  }
  try {
    apiFunc.apply(null, params);
  } catch (e$) {
    e = e$;
    return error("Can't call api function", e);
  }
  return true;
}
function test(){
  return wormholeFull.ping({
    time: Date.now()
  });
}
function initWormholeFull(done){
  if (typeof qt == 'undefined' || qt === null) {
    return done({});
  }
  return new QWebChannel(qt.webChannelTransport, function(channel){
    var wormholeFull, err;
    wormholeFull = channel.objects[config.wormholeFullName];
    '# --- connect to signal.\nfoo.signal-name.connect (msg) ->\n\n# --- invoke method, async.\nfoo.method-name "bar" (ret) ->';
    err = wormholeFull == null;
    return done({
      err: err,
      wormholeFull: wormholeFull
    });
  });
}
function initWormholeHalf(done){
  var wh;
  wh = window[config.wormholeHalfName] = wormholeHalf();
  return done({
    wormholeHalf: wh
  });
}
function timeoutS(secs, f){
  return setTimeout(f, 1000 * secs);
}
function wormholeHalf(){
  var queue;
  queue = [];
  return {
    clear: function(){
      return queue = [];
    },
    get: function(){
      return queue;
    },
    push: function(x){
      return queue.push(x);
    }
  };
}
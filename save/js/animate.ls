'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    animate-series

{ log, } = require './util'

function animate-series { $elem, props, props-orig, duration, repeat, }

    # ------ the instance.

    idx: void
    $elem: $elem
    repeat: repeat

    props-orig: props-orig
    # --- arrays.
    props: props
    duration: duration

    finish-called: void
    animating: void

    animate: ->
        @animating = true
        @$elem.animate @props[@idx],
            duration: @duration[@idx]
            done: ~>
                @animating = false
                if @finish-called
                    @finish-called = false
                    @restore()
                else
                    @next()
    restore: ->
        @$elem.animate @props-orig,
            duration: 100
    next: ->
        @idx = ++@idx
        l = @props.length
        if @idx == l
            if @repeat then @idx = @idx % @props.length
            else return
        @animate()
    go: ->
        @idx = -1
        @finish-called = false
        @next()
    stop: ->
        if @animating
            @finish-called = true
            # --- any running animations jump to end value.
            @$elem.finish()
        else
            @restore()

'''
Passphrases | https://github.com/micahflee/passphrases

Copyright (C) 2015-2016
    Micah Lee <micah@micahflee.com>
    Allen Haim <allen@netherrealm.net>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

export
    init

$ = require 'jquery'

{ log, error, array, get-elems, } = require './util'
{ get-config, } = require './config'
cppapi = require './cppapi'

our =
    elems: {}
    dev: void

function init { dev, }
    { selectors, } = get-config()
    our <<< { dev, }
    let u = get-elems selectors.settings
        return error "Can't init settings" unless u
        our.elems <<< u
    disable-buttons() if our.dev
    init-events()

function init-events
    e = our.elems
    e.$button-save.on 'click' ->
        ok = cppapi.save-preferences do
            {}
    e.$button-quit.on 'click' ->
        ok = cppapi.quit()
    e.$button-test-notify.on 'click' ->
        ok = cppapi.notify()

function disable-buttons
    e = our.elems
    e.$buttons.attr 'disabled' 'disabled'

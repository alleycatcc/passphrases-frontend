'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var $, util, ref$, log, warn, cppapi, nav, generate, memorize, settings, data, our;
$ = require('jquery');
ref$ = util = require('./util'), log = ref$.log, warn = ref$.warn;
cppapi = require('./cppapi');
nav = require('./nav');
generate = require('./generate');
memorize = require('./memorize');
settings = require('./settings');
data = require('./data');
our = {
  dev: void 8
};
$('document').ready(function(){
  if (/^http/.exec(document.location.href)) {
    $('body').addClass('dev');
    our.dev = true;
  }
  cppapi.init({
    dev: our.dev
  });
  data.init();
  util.init({
    dev: our.dev
  });
  nav.init();
  generate.init();
  memorize.init();
  settings.init({
    dev: our.dev
  });
  return makeLinksExternal();
});
function makeLinksExternal(){
  var onClick, extLink;
  onClick = function($a){
    var href, ok;
    if (!(href = $a.attr('href'))) {
      return;
    }
    ok = cppapi.openLink(href);
    if (!ok) {
      return warn("Couldn't open external link", href);
    }
  };
  extLink = function($a){
    return $a.on('click', function(){
      return onClick($a);
    });
  };
  return $('a').each(function(_, a){
    return extLink($(a));
  });
}
'// make shortcut keys like Cmd-Q work in OSX (#4)\nif(process.platform === \'darwin\') {\n  var nativeMenuBar = new gui.Menu({ type: "menubar" });\n  nativeMenuBar.createMacBuiltin("Passphrases", {\n    hideEdit: true,\n    hideWindow: true\n  });\n  win.menu = nativeMenuBar;\n}';
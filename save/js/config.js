'Passphrases | https://github.com/micahflee/passphrases\n\nCopyright (C) 2015-2016\n    Micah Lee <micah@micahflee.com>\n    Allen Haim <allen@netherrealm.net>\n\nPermission is hereby granted, free of charge, to any person obtaining a\ncopy of this software and associated documentation files (the\n"Software"), to deal in the Software without restriction, including\nwithout limitation the rights to use, copy, modify, merge, publish,\ndistribute, sublicense, and/or sell copies of the Software, and to\npermit persons to whom the Software is furnished to do so, subject to\nthe following conditions:\n\nThe above copyright notice and this permission notice shall be included\nin all copies or substantial portions of the Software.\n\nTHE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS\nOR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.\nIN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY\nCLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,\nTORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE\nSOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.';
var config, out$ = typeof exports != 'undefined' && exports || this;
out$.getConfig = getConfig;
config = {
  wordlistsMeta: {
    securedrop: {
      name: 'SecureDrop',
      description: 'English wordlist from the SecureDrop project'
    },
    english: {
      name: 'English Diceware',
      description: 'Original English Diceware wordlist'
    },
    catalan: {
      name: 'Catalan Diceware',
      description: 'Diccionari catal&agrave; per Marcel Hernandez - CC-BY 4.0'
    },
    german: {
      name: 'Deutsch Diceware',
      description: 'Deutsch W&ouml;rterbuch, von Benjamin Tenne - GPL'
    },
    french: {
      name: 'Fran&ccedil;ais Diceware',
      description: 'Dictionnaire fran&ccedil;ais par Matthieu Weber'
    },
    italian: {
      name: 'Italiano Diceware',
      description: 'Lista di parole Diceware in Italiano by Tarin Gamberini - GPL'
    },
    japanese: {
      name: 'Japanese Diceware',
      description: 'Japanese Diceware by Hiroshi Yuki &amp; J Greely - CC-BY-SA'
    },
    dutch: {
      name: 'Dutch Diceware',
      description: 'Nederlands woordenboek door Bart Van den Eynde - GPL'
    },
    polish: {
      name: 'Polski Diceware',
      description: 'Polski S&lstrok;ownik przez Piotr (DrFugazi) Tarnowski'
    },
    swedish: {
      name: 'Svensk Diceware',
      description: 'Svensk ordbok av Magnus Bodin'
    }
  },
  defaultNumWords: 7,
  selectors: {
    nav: {
      $nav: '.c-nav__generate, .c-nav__memorize, .c-nav__settings',
      $contents: '.b-generate, .b-memorize, .b-settings'
    },
    generate: {
      $selectWordlists: '.c-wordlists',
      $numWords: '.b-generate__select .x__num-words',
      $numWordsNum: '.b-generate__select .x__num-words .x__num',
      $numWordsArrowUp: '.b-generate__select .x__arrow-up',
      $numWordsArrowUpInner: '.b-generate__select .x__arrow-up .x__inner',
      $numWordsArrowDown: '.b-generate__select .x__arrow-down',
      $numWordsArrowDownInner: '.b-generate__select .x__arrow-down .x__inner',
      $buttonGenerate: '.b-generate__generate',
      $buttonGenerateInner: '.b-generate__generate .x__inner',
      $buttonMemorize: '.b-generate__memorize',
      $buttonMemorizeInner: '.b-generate__memorize .x__inner',
      $menuTextSecondary: '.x__menu-text--secondary span',
      $passphrase: '.b-generate__passphrase',
      $passphraseWrapper: '.b-generate__passphrase-wrapper',
      $divWordlistDescription: '.b-generate__wordlist-description'
    },
    memorize: {
      $divMemorizeGo: '.memorize-go',
      $divMemorizeActive: '.memorize-active',
      $divMemorizeWaiting: '.memorize-waiting',
      $divMemorizeReady: '.memorize-ready',
      $divMemorizeInput: '.memorize-input',
      $divMemorizeNote: '.memorize-note',
      $divMemorizeTries: '.memorize-tries',
      $divHintProgress: '.hint-progress',
      $divHintProgressWrapper: '.hint-progress-wrapper',
      $divHint: '.hint',
      $divHintShow: '.hint-show',
      $divCountdown: '.countdown',
      $inputPassphrase: '.passphrase-to-memorize',
      $inputPassphraseTest: '.passphrase-test',
      $buttonStartMemorizing: '.c-button-start-memorizing',
      $buttonSkipCountdown: '.button-skip-countdown',
      $buttonCancel: '.button-cancel',
      $formMemorize: '.memorize-input-form'
    },
    settings: {
      $buttons: '.b-settings button',
      $buttonSave: '.b-settings__save',
      $buttonQuit: '.b-settings__quit',
      $buttonTestNotify: '.b-settings__test-notify'
    }
  },
  strengths: ['weak', 'soso', 'strong'],
  numWordsMin: 3,
  numWordsMax: 10,
  colorWeak: '#900'
};
function getConfig(){
  return config;
}
const path = require('path');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const proxy = require('http-proxy-middleware');

function createWebpackMiddleware(compiler, publicPath) {
  return webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath,
    silent: true,
    stats: 'errors-only',
  });
}

module.exports = function addDevMiddlewares(app, webpackConfig) {
  const compiler = webpack(webpackConfig);
  const middleware = createWebpackMiddleware(compiler, webpackConfig.output.publicPath);

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));

//   app.use('/api/forecast', proxy ({
//     // --- e.g.: https://graphdata.buienradar.nl/forecast/json/?lat=52.37&lon=4.89&btc=201804271428"
//     target: 'https://graphdata.buienradar.nl/forecast',
//     logLevel: 'debug',
//     changeOrigin: true,
//     pathRewrite: { '^/api/forecast': '' },
//   }))
//
//   app.use ('/api/log-server', proxy ({
//     target: 'http://localhost:3003/log',
//     logLevel: 'debug',
//     changeOrigin: false,
//     pathRewrite: { '^/api/log-server': '' },
//   }))
//

  // Since webpackDevMiddleware uses memory-fs internally to store build
  // artifacts, we use it instead
  const fs = middleware.fileSystem;

  app.get('*', (req, res) => {
    fs.readFile(path.join(compiler.outputPath, 'index.html'), (err, file) => {
      if (err) {
        res.sendStatus(404);
      } else {
        res.send(file.toString());
      }
    });
  });
};

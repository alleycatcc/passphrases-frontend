defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  isObject, assocPathM,
  tap, join,
  id,
  appendToM,
  mapTuples,
  assocM,
  lets,
} from 'stick-js'

import mergeAll from 'ramda/src/mergeAll'

import {
  logWith,
  reduceObjDeep,
  transformIntl,
  mapAsList,
} from 'app-util'

// --- for <component>/messages.js
export const msgs = {
  'app.containers.GenerateTab': {
    numWords: `{num} {num, plural,
      one {word}
      other {words}}
    `,
    moreInfo: {
      words: `{num} {numNum, plural,
        one {word}
        other {words}}
      `,
      bitsPerWord: `{num} {numNum, plural, one {bit} other {bits}} of entropy per word`,
      numWordsWord: '{numWords}-word',
      bitsTotal: 'A {numWordsWord} passphrase yields {numBits} {numBitsNum, plural, one {bit} other {bits}} of entropy.',
      warningGuess: '{guess}',
      guess: 'on average {guess}',
      onlyTime: 'only {time} {units}',
      time: '{time} {units}',
      willTake: 'It will take an adversary {guess} to guess your passphrase, guessing at one trillion guesses per second.',
    },
    timeUnits: {
      seconds: 'seconds',
      minutes: 'minutes',
      hours: 'hours',
      days: 'days',
      months: 'months',
      years: 'years',
      thousandYears: 'thousand years',
      millionYears: 'million years',
      billionYears: 'billion years',
      trillionYears: 'trillion years',
      quadrillionYears: 'quadrillion years',
      quintillionYears: 'quintillion years',
    },
  },
  'app.containers.SettingsTab': {
    foss: 'Passphrases is free and open source software, released under an MIT license.',
    source: 'Source code',
    willOpenInBrowser: 'Will open in a separate browser window',
    internet: 'Passphrases does not collect any information or do anything over the Internet.',
  },
}

// --- for i18n.js
export default msgs
  | mapAsList ((id, msgs) => transformIntl (id) (msgs))
  | mergeAll

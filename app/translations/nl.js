defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  isObject, assocPathM,
  tap, join,
  id,
  appendToM,
  mapTuples,
  assocM,
  lets,
} from 'stick-js'

import mergeAll from 'ramda/src/mergeAll'

import {
  logWith,
  reduceObjDeep,
  transformIntl,
  mapAsList,
} from 'app-util'

const messages = {
  'app.containers.GenerateTab': {
    numWords: "{num} {num, plural, one {woord} other {woorden}}",
    moreInfo: {
      words: "{num} {numNum, plural, one {woord} other {woorden}}",
      bitsPerWord: "entropie per woord bedraagt {num} {numNum, plural, one {bit} other {bits}}",
      numWordsWord: "{numWords}-woord",
      bitsTotal: "De entropie van een {numWordsWord} passphrase bedraagt {numBits} {numBits, plural, one {bit} other {bits}}",
      warningGuess: "{guess}",
      guess: "gemiddeld {guess}",
      onlyTime: "slechts {time} {units}",
      time: "{time} {units}",
      willTake: "Een tegenstander die één biljoen keer per seconde kan gokken, zal er {guess} over doen om jouw wachtwoord te raden.",
    },

    // --- assume always floating point, hence plural ok.
    timeUnits: {
      seconds: "seconden",
      minutes: "minuten",
      hours: "uur",
      days: "dagen",
      months: "maanden",
      years: "jaar",
      thousandYears: "duizend jaar",
      millionYears: "miljoen jaar",
      billionYears: "miljard jaar",
      trillionYears: "biljoen jaar",
      quadrillionYears: "duizend biljoen jaar",
      quintillionYears: "miljoen biljoen jaar"
    }
  },
  'app.containers.SettingsTab': {
    foss: 'Passphrases is vrije, open-source software, onder een MIT licentie uitgebracht.',
    source: 'Broncode',
    willOpenInBrowser: 'Wordt in een apart browservenster getoond',
    internet: 'Passphrases verzamelt geen gegevens en verstuurt geen data over het internet.',
  },
}

export default messages
  | mapAsList ((id, msgs) => transformIntl (id) (msgs))
  | mergeAll

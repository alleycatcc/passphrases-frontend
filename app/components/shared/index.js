defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  prop, whenTrue, sprintf1,
  ifOk,
  tap, id,
} from 'stick-js'

import React, { PureComponent, } from 'react'
import styled from 'styled-components'

import {
  mlt, mgt, mediaComp, media,
  logWith,
} from 'app-util'

import configMod from 'alleycat-js/config'
import config from 'config'

const { configGet, configGetOr, configGets, } = config | configMod.init

const colors = 'colors' | configGet

const PasswordInnerS = styled.div`
`

const PasswordInner = ({ password, }) => <PasswordInnerS>
  { password }
</PasswordInnerS>

export const Password = ({ children, variant = 'normal', password, warning, }) => <PasswordS warning={warning} className={'x--variant-' + variant}>
  { children | ifOk (
    id,
    _ => <PasswordInner password={password} />
  )}
</PasswordS>

const PasswordS = styled.div`
  ${ media (
    768 | mlt | mediaComp ('font-size: 1.8em;'),
    768 | mgt | mediaComp ('font-size: 2.0em;'),
  )}

  background: white;
  margin-top: 3vh;
  text-align: center;
  vertical-align: middle;
  display: flex;
  align-items: center;
  justify-content: center;
  ${ prop ('warning') >> whenTrue (_ => colors.password.warning | sprintf1 ('color: %s;'))}

  &.x--variant-normal {
    height: 16vh;
    padding: 15px;
    width: calc(100% - 32px);
    border: 1px solid #999999;
  }

  &.x--variant-input {
    padding: 5px;
    width: calc(100% - 12px);
  }
`

export const TabWrapper = styled.div`
  ${ media (
    768 | mlt | mediaComp ('width: 100%;'),
    768 | mgt | mediaComp ('width: 500px;'),
  )}

  margin: auto;
`


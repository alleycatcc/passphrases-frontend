defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  tap,
  mergeM,
  bindProp, whenOk,
  invoke,
  condS, guard, otherwise, guardV,
  eq,
  lets, letS,
  sprintf1, sprintfN,
  multiply,
  map,
  prop,
  ifTrue,
  arg1, arg2,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import messages from './messages'

import {
  fold, foldJust,
  Just, Nothing,
  logWith,
  whyYouRerender,
  propsChanged,
  ierrorErrorDie,
  alertError,
  toJust,
  divMod,
} from 'app-util'

import config from '../../config'

import dieProgress from './die'

const { floor, } = Math

const CountdownInner = styled.div`
`

const dieSize = 30

const BarS = styled.div`
  width: 100%;
  height: ${dieSize}px;
  position: relative;
  .x__wrapper {
    width: calc(100% - 40px);
    height: ${ 'barHeight' | prop};
    display: inline-block;
    position: relative;
  }
  canvas {
    position: relative;
    vertical-align: top;
  }
`

// const BarInnerS = styled.div.attrs ({
//   style: ({ height, color, }) => ({
//     height,
//   })
// })`

const BarInnerS = styled.div`
  display: inline-block;
  width: 100%;
  height: 100%;
  position: absolute; left: 0; top: 0;
  > div {
    display: inline-block;
    height: 100%;
    background: ${prop ('color')};
  }
`

const BarInnerL = styled.div`
  border-top-left-radius: 10000px;
  border-bottom-left-radius: 10000px;
  width: 3%;
`

const BarInnerM = styled.div.attrs ({
  style: ({ pos, }) => ({
    width: pos | sprintf1 ('calc((100%% - 6%% - 2px) * %s'),
  })
})`
`

const BarInnerR = styled.div`
  border-top-right-radius: 10000px;
  border-bottom-right-radius: 10000px;
  width: 3%;
`

const BarInner = ({ color, pos, }) => <BarInnerS
  color={color}
>
  <BarInnerL />
  <BarInnerM pos={pos} />
  <BarInnerR />
</BarInnerS>

class Bar extends PureComponent {
  componentDidUpdate (prevProps) {
    const { props, } = this
    if (!propsChanged (prevProps, props)) return
    const { cur, targetVal, } = props
    if (this.draw) (cur / targetVal * 600) | this.draw
  }

  componentDidMount () {
    this.draw = dieProgress.go (this.ref, {
      width: dieSize,
      height: dieSize,
    })
  }

  render () {
    const { props, } = this
    const { cur, targetVal, } = props
    return <BarS barHeight='4px'>
      <div className='x__wrapper'>
        <BarInner color='#bbb' pos='1' />
        <BarInner color='#766' pos={cur / targetVal} />
      </div>
      { true && <canvas
        ref={ref => this.ref = ref}
      /> }
    </BarS>
  }
}

const DigitS = styled.div`
  font-size: 2.0em;
  text-align: center;
  margin-top: 6vh;
`

const Digits = ({ cur, targetVal, }) => <DigitS>
  { lets (
    _ => (targetVal - cur) / 1000,
    divMod (3600),
    arg1 >> prop (1) >> divMod (60),
    arg2 >> prop (1),
    (_, [hrs], [mins], secs) => hrs !== 0 ?
      [hrs, mins, secs] | sprintfN ('%d:%02d:%02d') :
      [     mins, secs] | sprintfN ('%d:%02d')
  )}
</DigitS>

const inner = style => cur => targetVal => <CountdownInner>
  { style | condS ([
    'bar'     | eq | guard (_ => <Bar cur={cur} targetVal={targetVal} />),
    'digits'  | eq | guard (_ => <Digits cur={cur} targetVal={targetVal} />),
    otherwise      | guard (x => ierrorErrorDie ('unexpected:', x))
  ])}
</CountdownInner>

const CountdownWrapper = styled.div`
  min-height: ${dieSize}px;
  margin-top: 2vh;
`

export default class Countdown extends PureComponent {
  constructor (props) {
    super (props)
    this | mergeM ({
      job: void 8,
      state: {
        cur: 0,
      },
    })
  }

  componentDidMount () {
    const { props, } = this
    const { target, onDidMount, } = props
    this.startStop (target)
    onDidMount | whenOk (invoke)
  }

  // --- this is the place for starting animations, now that will-receive-props is gone.
  componentDidUpdate (prevProps) {
    const { props, } = this
    const { target, } = props
    if (! propsChanged (prevProps, props, 'Countdown')) return
    this.startStop (target)
  }

  startStop (target) {
    const { props, } = this

    this.stop ()
    this.start (target)
  }

  start (target) {
    const { props, } = this
    const { inflate = 1, } = props

    this.setState ({ cur: 0, })
    return this.foldTarget (this | bindProp ('startReal'))
  }

  startReal (targetVal) {
    const { props, } = this
    const { onStarted, onTimeElapsed, interval = 200, } = props

    this.job = setInterval (_ => {
      const cur = this.state.cur
      const nxt = cur + interval
      if (nxt > targetVal) {
        onTimeElapsed | whenOk (invoke)
        return this.stop ()
      }
      this.setState ({ cur: nxt, })
    }, interval)

    onStarted | whenOk (invoke)
  }

  stop () {
    const { props, job, } = this
    const { onStopped, } = props

    if (!job) return

    job | clearInterval
    this.job = void 8
    onStopped | whenOk (invoke)
  }

  foldTarget = f => {
    const { props, } = this
    const { target, inflate = 1, } = props

    return target | map (inflate | multiply) | foldJust (f)
  }

  render () {
    const { props, state, } = this
    const { target, style, } = props
    const { cur, } = state

    return <CountdownWrapper>
      { this.foldTarget (cur | inner (style)) }
    </CountdownWrapper>
  }
}

Countdown.propTypes = {
}

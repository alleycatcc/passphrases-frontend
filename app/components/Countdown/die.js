defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN, rangeBy,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  dot4, side4,
  invoke,
  sideN,
  mergeToSym, mergeWith,
  ampersandN,
} from 'stick-js'

const { log, } = console
const { PI: pi, random, floor, } = Math

const logWith = (header) => (...args) => log (... [header, ...args])

const getContext2D = dot1 ('getContext') ('2d')
const fillStyle = assocM ('fillStyle')
const fillRect = side4 ('fillRect')
const strokeRect = side4 ('strokeRect')
const strokeStyle = assocM ('strokeStyle')
const lineWidth = assocM ('lineWidth')

const side6 = prop => (...vals) => sideN (prop) (vals)

// --- choose source value on conflict
const chooseSrc = (src, tgt) => src
const mergeChooseSrc = mergeToSym | mergeWith (chooseSrc)

const defaultProps = mergeChooseSrc ({
  width: 200,
  height: 200,
  margin: 0.1,
  r: 0.10,
  velocity: 50,
  eye6: {
    gutter: 0.26,
    yShift: 0.25,
  },
  totalTime: 10000,
})

export const go = (canvas, cfg) => {
  const state = cfg | defaultProps
  const { width, height, } = state
  const ctxt = canvas
    | mergeM ({
      width, height,
    })
    | getContext2D

  // --- animation way
  // draw (state, ctxt) | drawLoop (state)

  return draw (state, ctxt)
}

const max = a => b => Math.max (a, b)
const min = a => b => Math.min (a, b)

const numTicks = 600

const drawLoop = ({ totalTime, }) => drawIt => {
  let t = 0
  const withLoop = (timestamp) => {
    t = (timestamp / totalTime * numTicks) | floor | min (numTicks)
    ; t | drawIt
    requestAnimationFrame (withLoop)
  }

  t | drawIt
  withLoop | requestAnimationFrame
}

const clearRect = side4 ('clearRect')

const clear = ({ width, height, }) => clearRect (0, 0, width, height)
const drawFrame = ({ width, height, margin, }) => lets (
  () => width * margin,
  (x) => width - 2 * x,
  () => height * margin,
  (_1, _2, y) => height - 2 * y,

  (x, rWidth, y, rHeight) =>
    fillStyle ('#000000')
    >> fillRect (x, y, rWidth, rHeight),
)

const beginPath = side ('beginPath')
const arc = side6 ('arc')
const stroke = side ('stroke')
const fill = side ('fill')

const eyePos6 = ({ eye6: { gutter, yShift }, width, height, }) => lets (
  _ => gutter * width,
  _ => yShift * height,
  (dx, dy) => ({
    1: [-dx, -dy],
    2: [ dx, -dy],
    3: [-dx,   0],
    4: [ dx,   0],
    5: [-dx,  dy],
    6: [ dx,  dy],
  })
)

const eye = ({ r, velocity, width, height, eye6, }) => which => amount => lets (
  _ => eyePos6 ({ eye6, width, height, }) [which],
  _ => r * width,

  ([shiftX, shiftY]) => 0.5 * width + shiftX,
  ([shiftX, shiftY]) => 0.5 * height + shiftY,

  _ => [amount * 2 * pi, random () * 2 * pi],

  (_, rr, x, y, [theta, thetaShift]) =>
  beginPath
  >> strokeStyle ('#ffffff')
  >> fillStyle ('#ffffff')
  >> lineWidth (5)
  >> arc (x, y, rr, 0 + thetaShift, theta + thetaShift, false)
  >> fill
  // >> (amount === 1 ? )
)

const drawEyes = cfg => t => {
  const { velocity, } = cfg
  const _eye = eye (cfg)
  const eye1 = _eye (1)
  const eye2 = _eye (2)
  const eye3 = _eye (3)

  const interval = numTicks / 6

  const stage = n => {
    const tt = (n - 1) * interval
    const f = _eye (n) ((t - interval * (n - 1)) / interval)
    return t > tt ? appendM (f) : id
  }

  return []
    | stage (1)
    | stage (2)
    | stage (3)
    | stage (4)
    | stage (5)
    | stage (6)
}

const draw = (state, ctxt) => (t) => {
  const { width, height, margin, } = state
  ctxt | clear (state)
       | drawFrame (state)
       | tap (ampersandN (drawEyes (state) (t)))
}

export default {
  go: go,
}

// global | mergeM ({ go, })


import { fromJS } from 'immutable';
import countdownReducer from '../reducer';

describe('countdownReducer', () => {
  it('returns the initial state', () => {
    expect(countdownReducer(undefined, {})).toEqual(fromJS({}));
  });
});

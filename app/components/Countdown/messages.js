/*
 * Countdown Messages
 *
 * This contains all the text for the Countdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Countdown.header',
    defaultMessage: 'This is Countdown container !',
  },
});

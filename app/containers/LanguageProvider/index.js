defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
    pipe, compose, composeRight,
} from 'stick-js'

import React, { PureComponent, } from 'react'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'
import { IntlProvider, } from 'react-intl'

import {
  makeSelectLocale,
} from 'domains/ui/selectors'

export class LanguageProvider extends PureComponent {
  render () {
    console.log ('hello')
    const { props, } = this
    const { children, locale, messages, } = props
    return (
      <IntlProvider
        locale={locale}
        key={locale}
        messages={messages[locale]}
      >
        { /* the production build will fail if this is { ... children} */ }
        { React.Children.only (children) }
      </IntlProvider>
    )
  }
}

const mapStateToProps = createStructuredSelector ({
  locale: makeSelectLocale (),
})

const mapDispatchToProps = (dispatch) => ({
})

export default LanguageProvider
  | connect (mapStateToProps, mapDispatchToProps)

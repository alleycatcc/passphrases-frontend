defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  mergeM,
  take,
  minus,
  dot,
  prop,
  ifOk, bindProp,
  id,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectGenerateTab from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import {
  iwarn, ierror, tellIf,
  resolveP,
  logAndroidPerf,
  alertError,
  media, mlt, mgt, mediaComp,
  length,
  isMobile,
} from 'app-util'

import configMod from 'alleycat-js/config'
import config from '../../config'

const { configGet, configGetOr, configGets, } = config | configMod.init

import { changeLocale, } from '../App/actions'
// import { } from '../../domains/domain/selectors'
import { makeSelectLocale, } from '../../domains/ui/selectors'
// import { } from '../../components/xxx'

const hostChannelOutgoing = 'hostChannelFromJS'
const { [hostChannelOutgoing]: channelOut, } = window

const openLink = channelOut | ifOk (
  bindProp ('openLink'),
  // --- no android context
  _ => (...args) => console.log ('[MOCK] open link', ...args),
)

const Top = styled.div`
  ${ media (
    768 | mlt | mediaComp (
      'font-size: 1.3em;',
      'line-height: 1.5em;',
      'overflow-y: scroll;',
    ),
    768 | mgt | mediaComp (
      'font-size: 1.5em;',
      'line-height: 1.5em;',
      // 'overflow-y: scroll;',
    ),
  )}
  height: 100%;
  font-family: 'Lora';
`

const Description = styled.div`
  font-size: 1.0em;
  div {
    margin-top: 2vh;
  }
  .x__steps {
    color: #8a0a0a;
  }
`

const About = styled.div`
  margin-top: 2vh;
  padding-top: 1vh;
  border-top: 1px solid #999999;
  font-size: 0.9em;
  div {
    margin-top: 2vh;
  }
`

const sourceLink = 'sourceLink' | configGet

const Button = styled.button`
  border: 1px solid black;
  margin-right: 2vw;
  padding: 2px;
  font-size: 1em;
  cursor: pointer;
`

const LanguageSwitcherS = styled.div`
  width: 100%;
  .x__wrapper {
    ${ media (
      768 | mlt | mediaComp ('font-size: 1.2em;'),
      768 | mgt | mediaComp ('font-size: 0.8em;'),
    )}
    position: relative;
    margin: 0px;
    display: flex;
    justify-content: flex-end;
    height: 30px;
    vertical-align: middle;
    .x__item, .x__box {
      width: 30px;
      text-align: center;
    }
    .x__spacer {
      width: 10px;
    }
    .x__box {
      border-bottom: 1px solid #999;
      position: absolute;
      top: 0;
      right: calc(10px + (30px + 10px) * ${prop ('fromRight')});
      // --- transition not possible -- whole component is re-rendered.
      transition: right .2s;
      height: 100%;
      z-index: 1;
    }
    .x__item {
      height: 100%;
      z-index: 2;
      cursor: pointer;
    }
  }
`

const LanguageSwitcher = ({ selectedIdx, changeLocaleEn, changeLocaleNl, }) => <LanguageSwitcherS fromRight={1 - selectedIdx}>
  <div className='x__wrapper'>
    <div className='x__spacer' />
    <div className='x__item' onClick={changeLocaleEn}>en</div>
    <div className='x__spacer' />
    <div className='x__item' onClick={changeLocaleNl}>nl</div>
    <div className='x__spacer' />
    <div className='x__box'/>
  </div>
</LanguageSwitcherS>

const LinkLike = styled.span`
  cursor: pointer;
  color: rgb(0, 0, 238);
  text-decoration: underline;
`

const hostLinkSource =  (sourceLink) => (children) => (
  <LinkLike onClick={_ => sourceLink | openLink}>
    { React.Children.only (children) }
  </LinkLike>
)
const hyperlinkSource = (sourceLink) => (children) => (
  <a target='_blank' href={sourceLink}>
    { React.Children.only (children) }
  </a>
)

export class SettingsTab extends React.PureComponent {
  constructor (props) {
    super (props)

    const { changeLocaleDispatch, } = props

    this | mergeM ({
      selectedLocaleIdx: 10,

      changeLocaleEn: () => {
        'en' | changeLocaleDispatch
        console.log ('this.selectedLocaleIdx', this.selectedLocaleIdx)
        this.selectedLocaleIdx = 0
        console.log ('this.selectedLocaleIdx', this.selectedLocaleIdx)
      },
      changeLocaleNl: () => {
        'nl' | changeLocaleDispatch
        this.selectedLocaleIdx = 1
      },
    })
  }

  render () {
    const { props, } = this
    const { locale, } = props
    console.log ('locale', locale)
    return (
      <Top>
        <LanguageSwitcher
          selectedIdx={locale === 'en' ? 0 : 1 /* xxx */}
          changeLocaleEn={this.changeLocaleEn}
          changeLocaleNl={this.changeLocaleNl}
        />
        <Description>
          <div>Passphrases is a tool that helps you generate high-entropy passphrases using a diceware
            wordlist, and to memorize them using a technique called spaced repetition.</div>

          <div>The ‘generate’ step works by simulating the following process:</div>

          <ul className='x__steps'>
            <li>Start with a wordlist containing, say, 7,776 entries.</li>
            <li>Roll a six-sided die five times and multiply the results: this will yield a number between 1 and 7,776.</li>
            <li>Choose that word from the list, e.g., 'fusoku'. You now have a one-word passphrase.</li>
            <li>Repeat the last two steps until you have at least six words, and then string them together: ‘fusoku samui rakkan enka saka jouki’.</li>
          </ul>

          <div>The ‘memorize’ step works using a technique for memorizing a string of text by practicing at varying intervals.</div>
        </Description>
        <About>
          <div>
            <FormattedMessage {...messages.foss}>
              { id }
            </FormattedMessage>
          </div>
          <div>
            { (isMobile () ? hostLinkSource (sourceLink) : hyperlinkSource (sourceLink)) (
                <FormattedMessage {...messages.source}>
                  { id }
                </FormattedMessage>
            ) }
            &nbsp;(<FormattedMessage {...messages.willOpenInBrowser}>
              { dot ('toLowerCase') }
            </FormattedMessage>).
          </div>
          <div>
            <FormattedMessage {...messages.internet} />
          </div>
        </About>
      </Top>
    )
  }
}

const mapStateToProps = createStructuredSelector ({
  locale: makeSelectLocale (),
})

const mapDispatchToProps = (dispatch) => ({
  changeLocaleDispatch: changeLocale >> dispatch,
})

export default SettingsTab
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'generateTab' })

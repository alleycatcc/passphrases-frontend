defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const MOCK = true
const MOCKDELAY = 0

const localConfig = {
  verbose: true,
}

import {
  pipe, compose, composeRight,
  add, multiply, join,
  path,
  sprintf1, sprintfN, ok, dot1, timesF, dot, whenOk,
  compact,
  guard, otherwise,
  exception, raise,
  invoke,
  concatTo,
  lets,
  letS,
  die,
  defaultTo,
  neu1, notOk,
  drop,
} from 'stick-js'

import { call, put, select, takeLatest, take, } from 'redux-saga/effects'
import request from 'utils/request'

import {
  changeTabReal,
} from '../App/actions'

import {
  makeSelectDebug,
} from '../../domains/app/selectors'

import {
  iwarn, ierror, tellIf,
  resolveP,
  logAndroidPerf,
  toJS, alertError,
  toJust,
} from 'app-util'

import { CHANGE_TAB, CHANGE_TAB_REAL, } from '../../containers/App/constants'

const tell = tellIf (localConfig.verbose)

// --- xxx resP
const delayWith = (ret, ms) => new Promise ((res, _) =>
  setTimeout (_ => ret | res, ms))

const delay = (...args) => delayWith (null, ...args)

const doRequest = debug => requestURL => debug
  ? call (delayWith, mock (), MOCKDELAY)
  : call (request, requestURL)

function* generalAlert () {
  true | alertError
}

function* sagaChangeTabReal ({ data: newLoc, }) {
  // --- simplest way: just manually set the hash.
  // --- this causes the action to be fired twice, but doesn't seem to be causing unnecessary rerenders.
  document.location.hash = newLoc
}

function* sagaChangeTab ({ data: { tab: newLoc, delay: theDelay, }}) {
  const delayAmount = theDelay | toJust
  if (delayAmount | ok) yield call (delay, delayAmount)
  yield changeTabReal (newLoc) | put
}

function mock () {
  return {}
}

export default function* rootSaga () {
  yield [
    takeLatest (CHANGE_TAB, sagaChangeTab),
    takeLatest (CHANGE_TAB_REAL, sagaChangeTabReal),
  ]
}


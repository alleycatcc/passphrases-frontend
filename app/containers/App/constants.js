import { LOCATION_CHANGE } from 'react-router-redux'

export const CHANGE_LOCALE = 'passphrases/App/CHANGE_LOCALE'

// --- special (react router).
export const LOCATION_CHANGE_REACT_ROUTER = LOCATION_CHANGE

export const CHANGE_TAB = 'passphrases/App/CHANGE_TAB'
export const CHANGE_TAB_REAL = 'passphrases/App/CHANGE_TAB_REAL'

export const ACCEPT_PASSWORD = 'passphrases/App/ACCEPT_PASSWORD'

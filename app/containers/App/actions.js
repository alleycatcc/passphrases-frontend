/**

Design: consider all actions and constants to be global, and keep them in containers/App.

We have three general stores: api, domain, and ui. `api` keeps track of, for instance, whether any api call is 'loading', in order to show a general spinner for example. `domain` keeps track of the domain data: everything which comes in as a result of api calls. It also has its own `error` property, which if true, means the app should halt (store is totally corrupted). `ui` keeps track of various UI state which for whatever reason shouldn't be tracked as react state: switches, toggles, etc.

Components which are classes are assumed to use react state; otherwise they should be stateless.

A single saga (containers/App/saga) picks up and sequences fat actions.

Sagas belonging to a container can do more specific things.

Containers can technically have their own actions & reducers, but we tend not to use them (keep all domain data in the `domain` store). But it could be useful as the app gets more complex.

UI events can either

1) Call an action directly (fetchKeysDispatch prop).

2) Fire a past tense action in that component's saga (e.g. GET_ASSIGNED_KEYS_CLICKED). From there it can pass control to a saga in the main saga (e.g. FETCH_ASSIGNED_KEYS), in which case it's considered a 'fat' action; or, if it stays within its own saga, e.g. updating some local UI, it's a 'skinny' action.

Passing control to the main saga can be done by using put() on an action.

If it's necessary to wait on the main saga and then continue in the local saga, there's two ways:

1) put() an action (FETCH_ASSIGNED_KEYS) and then take() the actions (FETCH_ASSIGNED_KEYS_SUCCESS and FETCH_ASSIGNED_KEYS_ERROR). This feels pretty spaghetti, but is also simple to understand. Advantage: you know if it was success / error.
2) call the saga directly with yield call(). This feels unconventional but saner. But, then you can't call fetchAssignedKeys() from within the local saga, which again means missing a bit of control. And you have to do a new select to find out if it went well.
3) Return a promise from the action creator and use put.sync? Haven't managed this and it all starts getting more complicated.

If it's not necessary to wait, just put the action.

Some store flags:

 domain:
  error: the state has become corrupted. fatal.
    --- 'Oops, the app has shut down unexpectedly.
    Please reload the page to try again'.
    --- the main component should have a selector
    for this and shut everything down.
 api:
  loading: bool
  success: [yes/no/void]
  error: last api request failed. not fatal.
    --- 'A request to the server failed -- please try again'.
 ui:
  error: show the bubble, with 'oops' or a message.

  as part of a fat action:
    FETCH_ASSIGNED_KEYS_ERROR

Past tense actions:

 click on 'switch tab'.
 a ui event which only affects the ui.
 SWITCH_TAB_CLICKED

 click on 'get assigned keys' button.
 a ui event which affects the ui and the domain.
 GET_ASSIGNED_KEYS_CLICKED

 callback returned.

Present tense: we want something to be done. UPLOAD_FILE, SET_THIS, TOGGLE_THAT.

["isn't it overkill to give the event an action? why not just FETCH_ASSIGNED_KEYS as the action?" because, if you later decide that the button should FETCH_ASSIGNED_KEYS and also change the UI (e.g. close a modal), depending on whether or not it succeeded, you will have to switch to a saga. And you can't put the close modal action inside FETCH_ASSIGNED_KEYS, because another button might also want to FETCH_ASSIGNED_KEYS and not close the modal, not semantic anyway etc.]

  buttons saga: GET_ASSIGNED_KEYS_CLICKED: put FETCH_ASSIGNED_KEYS
  api reducer: FETCH_ASSIGNED_KEYS: api loading
  component: api selector / get ('loading'): component spinner
  main saga: FETCH_ASSIGNED_KEYS:
   [skipping FETCH_ASSIGNED_KEYS_BEGIN]
  main saga: do the request.
  main saga: put FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR
  api reducer: FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR: component loading.
  domain reducer: FETCH_ASSIGNED_KEYS_SUCCESS/FETCH_ASSIGNED_KEYS_ERROR: update state.

  ui events: CLICKED, CHOSEN, CHANGED, OPENED, CLOSED, ...


as for addkeysfromcsvmodal: everything happens in the local saga. if it seems ok, don't over-engineer ...


from ButtonsMain/saga#sagaGetAssignedKeysClicked
 can put fetchAssignedKeys (), but it's async, so what if you want to do some ui after that?
 can do yield sagaFetchAssignedKeys | call, is that weird?
 can do put.sync, how?
avoid actions.ui.blah because typos too easy.

FETCH_ASSIGNED_KEYS_ERROR


If a container has associated components, put them in the folder with the container.

generate (plop) is potentially useful, but we would have to edit the templates first.

Always use PureComponent, not Component, and if you pass changes through the redux way it should always work. (Component uses a deep compare).

Saga formulas:
  no parens:
    yield keys | fetchAssignedKeysSuccess | put
  yes parens:
  yield fetchAssignedKeys () | put

In the reducer, on actions: fromJS
In the component: toJS. Handy idiom:
  codes={assignedKeysCodesOrdered | whenOk (toJS)}

It's ok to pull things from selectors inside the saga.

'Do you want headers?': y means use <Helmet>, for managed <head>. Usually n.

*/

import { CHANGE_TAB, CHANGE_TAB_REAL, LOCATION_CHANGE_REACT_ROUTER, ACCEPT_PASSWORD,
  UPDATE_COUNTDOWN_TARGET, UPDATE_COUNTDOWN_CUR,
  CHANGE_LOCALE,
} from './constants'

import { Just, Nothing, } from 'app-util'

export const changeTab = (tab, delay = Nothing) => ({
  type: CHANGE_TAB,
  data: {
    tab,
    delay,
  },
})

export const changeTabReal = (tab) => ({
  type: CHANGE_TAB_REAL,
  data: tab,
})

export const acceptPassword = (passphrase, numWords, selectedList) => ({
  type: ACCEPT_PASSWORD,
  data: {
    passphrase,
    numWords,
    selectedList,
  }
})

export const changeLocale = (locale) => ({
  type: CHANGE_LOCALE,
  data: locale,
})

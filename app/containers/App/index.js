defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1,
} from 'stick-js'

import React from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import {
  Alert,
} from 'alleycat-components'

import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'

import domainReducer from '../../domains/domain/reducer'
import uiReducer from '../../domains/ui/reducer'

import appReducer from '../../domains/app/reducer'

import {
  makeSelectError,
} from '../../domains/domain/selectors'

import HomePage from 'containers/HomePage/Loadable'
import NotFoundPage from 'containers/NotFoundPage/Loadable'

import saga from './saga'

const fontLora = require ('../../fonts/lora.woff2')

const AppWrapper = styled.div`
  @font-face {
    src: url("${_ => fontLora}") format("woff2");
    font-family: "Lora";
  }
  font-family: 'Lora', sans-serif;
  font-size: 12px;
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0 auto;
  display: flex;
  height: 100%;
  // padding: 0 16px;
  flex-direction: column;
  z-index: 1;
`

class App extends React.PureComponent {
  render () {
    const { error, } = this.props
    const title = 'FIXME'

    return error | ifTrue (_ =>
      <div>
        <div>Sorry, but we've encountered a fatal error.</div>
        <div>Please reload the page and start again.</div>
      </div>
    ) (_ =>
      <AppWrapper>
        <Helmet
          titleTemplate={"%s - " + title}
          defaultTitle={title}
        >
          <meta name="description" content="" />

        </Helmet>
        <Switch>
          <Route exact path="/" render={props => <HomePage
              route={props}
              tab='generate'
            />}
          />
          <Route path="/generate" render={props => <HomePage
              route={props}
              tab='generate'
            />}
          />
          <Route path="/memorize" render={props => <HomePage
              route={props}
              tab='memorize'
            />}
          />
          <Route path="/settings" render={props => <HomePage
              route={props}
              tab='settings'
            />}
          />
          <Route path="" component={NotFoundPage} />
        </Switch>
        {Alert ()}
      </AppWrapper>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  error: makeSelectError (),
})

export default App
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'app' })
  | injectReducer ({ reducer: domainReducer, key: 'domain' })
  | injectReducer ({ reducer: uiReducer,     key: 'ui' })
  | injectReducer ({ reducer: appReducer,    key: 'app' })

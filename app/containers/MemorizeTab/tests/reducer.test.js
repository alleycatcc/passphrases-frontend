
import { fromJS } from 'immutable';
import generateTabReducer from '../reducer';

describe('generateTabReducer', () => {
  it('returns the initial state', () => {
    expect(generateTabReducer(undefined, {})).toEqual(fromJS({}));
  });
});

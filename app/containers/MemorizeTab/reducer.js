defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
} from 'stick-js'

// import { } from '../../containers/App/constants'

const initialState = {
}

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}



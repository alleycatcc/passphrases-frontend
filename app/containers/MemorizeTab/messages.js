/*
 * GenerateTab Messages
 *
 * This contains all the text for the GenerateTab component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.GenerateTab.header',
    defaultMessage: 'This is GenerateTab container !',
  },
});

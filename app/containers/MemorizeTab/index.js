defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

// xxx
let notify_id = 0

import {
  pipe, compose, composeRight,
  path, bindProp,
  side, side1, side2,
  whenOk,
  prop, whenTrue,
  sprintf1,
  mergeM, ifOk, id, always,
  join,
  multiply,
  map,
  dot1,
  dot,
  sprintfN,
  tap,
  condS, guard, otherwise, guardV,
  eq,
  whenNotOk,
  noop,
  ok,
  not,
  whenFalse,
  ifFalse,
  split, defaultToV,
  whenPredicate,
  reduce, ifPredicate, assocM,
  ampersandN,
  notOk,
  list,
  lets,
} from 'stick-js'

import fromPairs from 'ramda/src/fromPairs'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectMemorizeTab from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import {
  notifyIfInBackground, notify,
  storePassphrase, storeNumWords,
  storeSelectedList, storeRoundIdx,
  quit,
} from 'hostChannel'
import memorizerMod from '../../memorizer'

import {
  Password,
  TabWrapper,
} from 'components/shared'

import Countdown from 'components/Countdown'

import {
  Go as GoIcon,
} from 'alleycat-components/icons'

import {
  iwarn, ierror, tellIf,
  resolveP,
  logAndroidPerf,
  alertError,
  Just, Nothing, foldJust,
  whyYouRerender,
  cata,
  toJust,
  logWith,
  isNothing,
  ierrorErrorDie,
  media, mlt, mgt, mediaComp,
  getQueryParam,
  isMobile,
} from 'app-util'

const ifFalseV = always >> ifFalse
const whenFalseV = always >> whenFalse

import configMod from 'alleycat-js/config'
import config from '../../config'

const { configGet, configGetOr, configGets, } = config | configMod.init

const SHOW_DEBUG_BUTTONS = false
const showDebugButtons = SHOW_DEBUG_BUTTONS || getQueryParam ('debug') === '1'

const create = dot1 ('create')

const memInit = side ('init')
const memStartActive = side ('startActive')

// import { } from '../App/actions'
// import { } from '../../domains/domain/selectors'
// import { } from '../../domains/ui/selectors'
// import { } from '../../components/xxx'

const memTestForSuccess = side2 ('testForSuccess')

const mobileInflate = isMobile () ? 3 : 1

const Top = styled.div`
  height: 100%;
  font-family: serif;
  ${ media (
    768 | mlt | mediaComp ('font-size: 0.9em;'),
    768 | mgt | mediaComp ('font-size: 1.1em;'),
  )}
`

const InstructionsS = styled.div`
  ${ prop ('hide') >> whenTrue (_ => 'display: none;')}
  font-size: 1.5em;
  .x__tries {
    ${ prop ('tries') >> whenNotOk (_ => 'display: none;')}
    width: 100%;
    text-align: center;
    margin-top: 3vh;
    span {
      color: green;
    }
  }
  .x__instruct {
    margin-top: 0.5vh;
    margin-bottom: 0.5vh;
    line-height: 1.1em;
    min-height: 7vh;
    border-bottom: 1px solid #999999;
  }
`

const SkipCountdownS = styled.div`
  text-align: center;
  margin-top: 10vh;
  svg {
    cursor: pointer;
  }
  div {
    display: inline-block;
  }
  .x__skip {
    vertical-align: middle;
    font-size: 1.5em;
    margin-right: 3vw;
  }
`

const buttonIconWidth = 'buttonIconWidth' | configGet

const SkipCountdown = ({ skipCountdown, }) => <SkipCountdownS>
  <div className='x__skip'>
    Skip countdown
  </div>
  <div className='x__button'
    onClick={skipCountdown}
  >
    <GoIcon
      width={buttonIconWidth} height={buttonIconWidth}
    />
  </div>
</SkipCountdownS>

const Instructions = ({ hide, instructions, tries, }) => <InstructionsS
  tries={tries}
  hide={hide}
>
  <div className='x__instruct'>
    { instructions }
  </div>
  <div className='x__tries'>
    Tries: <span>{ tries }</span>
  </div>
</InstructionsS>

const PasswordDisplayS = styled.div`
  ${ media (
    768 | mlt | mediaComp (
      'margin-top: 2vh;',
      'font-size: 2.0em;',
    ),
    768 | mgt | mediaComp (
      'margin-top: 5vh;',
      'font-size: 2.0em;',
    ),
  )}
  font-family: 'Lora';
  min-height: 3vh;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`

const PasswordWrapper = styled.div`
  ${ media (
    768 | mlt | mediaComp ('margin-top: 2vh;'),
    768 | mgt | mediaComp ('margin-top: 6vh;'),
  )}
`

const PasswordDisplay = ({ password, showPassphrase, }) => <PasswordDisplayS>
  { showPassphrase && password }
</PasswordDisplayS>

const colorWrong = ['colors', 'guess', 'wrong'] | configGet

const InputS = styled.input`
  width: calc(100% - 14px);
  height: 5vh;
  padding: 5px;
  border: 2px ridge #781200;
  &.x--has-typo {
    outline: 0px;

    color: ${colorWrong};
    border: 5px solid ${colorWrong};

    margin: -5px 0px -3px -5px;
    width: calc(100% - 20px);
    padding-left: 7px;
    padding-right: 10px;
    padding-bottom: 10px;
    padding-top: 12px;
    height: calc(5vh - 10px);
  }
`

// --- note that textarea can not (easily?) conceal the password, hence text input.

class Input extends PureComponent {
  constructor (props) {
    super (props)
    this.ref = void 8
  }

  componentDidUpdate (prevProps) {
    const { props, ref, } = this
    const { clearKey, } = props
    if (prevProps.clearKey === clearKey) return
    if (!ref) ierrorErrorDie ('no ref')
    ref.querySelector ('input').value = ''
  }

  onFocus () {
    const { props, } = this
    const { scrollToInstructions, } = props
    if (!isMobile ()) return
    document.body.classList.add ('x--soft-keyboard-visible')
    scrollToInstructions ()
  }

  onBlur () {
    if (!isMobile ()) return
    document.body.classList.remove ('x--soft-keyboard-visible')
  }

  render () {
    const { props, } = this
    const { clearKey, onInput, hasTypo, concealInput, } = props

    return <div
      ref={ref => this.ref = ref}
      style={{ width: '100%', }}
    >
      <InputS
        className={hasTypo && 'x--has-typo'}
        onInput={path (['target', 'value']) >> onInput}
        onFocus={this.onFocus.bind (this)}
        onBlur={this.onBlur.bind (this)}
        hasTypo={hasTypo}

        autoComplete='off'
        autoCorrect='off'
        autoCapitalize='off'
        spellCheck='false'

        type={concealInput ? 'password' : 'text'}
      />
    </div>
  }
}

const CountdownS = styled.div`
`

const CountdownInner = styled.div`
`

const stateCata = reactState => cata ({
  MemActive: (
    countdownTarget, instructions, tries,
    hasTypo, concealInput,
  ) => ({
    instructions,
    tries,
    hasTypo,
    concealInput,
    mode: 'active',
    clearKey: tries === reactState.tries
      ? reactState.clearKey
      : reactState.clearKey + 1,
    showPassphrase: tries === 0 ? true : reactState.showPassphrase,
    countdownTarget: tries === reactState.tries
      ? reactState.countdownTarget
      : countdownTarget,
  }),
  MemPendingNotify: (timeLeft, instructions) => ({
    instructions,
    countdownTarget: timeLeft | Just,
    mode: 'pending',
  })
})

const setState = bindProp ('setState')

const showHidePassphrase = val => ctxt => _ =>
  ctxt.setState ({ showPassphrase: val, })

const hidePassphrase = false | showHidePassphrase
const showPassphrase = true  | showHidePassphrase

const startNewRound = (ctxt) => _ => {
  const { memorizer, } = ctxt
  memorizer | memStartActive
  notifyIfInBackground (...ctxt.getNotifyText ())
}

const callNotifyIfInBackground = (short, long) => notifyIfInBackground (++notify_id, short, long)
const callNotify = (short, long) => notify (++notify_id, short, long)

const countdownInterval = 'countdownInterval' | configGet

const Button = styled.button`
  border: 1px solid black;
  margin-right: 2vw;
  padding: 2px;
  font-size: 1em;
  cursor: pointer;
`

const ByTheWay = styled.div`
  ${ media (
    768 | mlt | mediaComp (
      'margin-top: 5vh;',
      'width: calc(100% - 2px);',
      'font-size: 1.3em;',
    ),
    768 | mgt | mediaComp (
      'margin-top: 7vh;',
      'width: 60%',
      'font-size: 1.5em;',
    ),
  )}
  display: ${ prop ('show') >> whenFalseV ('none') };
  border-top: 1px solid #999999;
  padding-top: 2vh;
  width: 60%;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  .x__close {
    font-weight: bold;
  }
`

export class MemorizeTab extends React.PureComponent {
  constructor (props) {
    super (props)
    this | mergeM ({
      newProps: true,
      initRoundIdx: getQueryParam ('roundIdx') | whenOk (Number),
      lastRoundStored: -1,

      hidePassphrase: this | hidePassphrase,
      showPassphrase: this | showPassphrase,
      startNewRound: this | startNewRound,

      state: {
        hasTypo: void 8,
        instructions: void 8,
        tries: void 8,
        countdownTarget: void 8,
        countdownRun: true,
        // countdownKey: 0,
        showPassphrase: false,

        concealInput: void 8,
        clearKey: 0,

        mode: void 8,
        state: void 8,
      },
    })
  }

  // xxx locale
  getNotifyText () {
    const { props, } = this
    const { numWords, selectedList, } = props
    // --- not working yet (name of list)
    // return [numWords, selectedList] | sprintfN ('Time to practice your %d-word %s passphrase!')
    return lets (
      _ => []         | sprintfN ('Time to practice your passphrase!'),
      _ => [numWords] | sprintfN ('%d words | unknown wordlist'),
      list,
    )
  }

  // --- xxx did update.
  updateStuff (props) {
    const { password: passphrase, numWords, selectedList, } = props
    if (passphrase | notOk) return
    console.log ('update: passphrase, numWords, selectedList', passphrase, numWords, selectedList)
    passphrase | ampersandN ([
      this | bindProp ('initMemorizer'),
      storePassphrase,
      _ => numWords | storeNumWords,
      _ => selectedList | storeSelectedList,
    ])
    // --- we have a memorizer now.
    const { memorizer, } = this
    const roundIdx = memorizer.getRoundIdx ()
    // --- so that choosing a new password resets the counter.
    this.setState ({ roundIdx, })
  }

  // --- xxx did update.
  componentWillReceiveProps (nextProps) {
    this.updateStuff (nextProps)
  }

  componentWillMount () {
    const { props, } = this
    this.newProps = true
    this.updateStuff (props)
  }

  componentDidUpdate (prevProps) {
    const { memorizer, lastRoundStored, } = this
    const roundIdx = memorizer.getRoundIdx ()
    if (roundIdx === lastRoundStored) return
    roundIdx | storeRoundIdx
    this.lastRoundStored = roundIdx
  }

  resolveMemorizerState (memState) {
    const { memorizer, } = this
    const round = memorizer | ifOk (m => m.getRoundIdx (), _ => -1)
    memState | stateCata (this.state)
             | assocM ('roundIdx', round)
             | setState (this)
  }

  initMemorizer (passphrase) {
    const params = {
      passphrase,
      listener: {
        onUpdateState: this | bindProp ('resolveMemorizerState'),
        onNextTry: _ => {
        },
      },
    }

    const { newProps, initRoundIdx=0, } = this

    if (this.newProps) {
      params | assocM ('roundIdx') (initRoundIdx)
      this.newProps = false
    }

    this.memorizer = memorizerMod
      | create (params)
      | memInit
      | memStartActive
  }

  onInput (password) {
    const countdownElapsed = false
    this.memorizer | whenOk (memTestForSuccess (password, countdownElapsed))
  }

  // --- We use 'sleutel' to force a re-render of countdown when the target is set, but has the same
  // value as the previous one.
  //
  // --- Note that our target is a Maybe, which is technically an object, and therefore would
  // trigger a re-render anyway. And `key` would not be right, because it would force a new instance
  // to be constructed.

  tabActive () {
    const { props, state, } = this
    // countdownKey,
    const {
      instructions, tries,
      countdownTarget, mode,
      hasTypo, concealInput, showPassphrase,
      clearKey,
      roundIdx,
    } = state

    const { password = undefined, scrollToInstructions, } = props

    const hasPassphrase = password | ok

    // sleutel={countdownKey}

    return <div>
      <Instructions instructions={instructions} tries={tries} hide={hasPassphrase | not}/>
      <Countdown
        onStarted={this.hidePassphrase}
        onDidMount={this.showPassphrase}
        onTimeElapsed={this.showPassphrase}
        sleutel={roundIdx}
        target={countdownTarget}
        inflate={mobileInflate}
        interval={countdownInterval}
        style='bar'
      />
      <PasswordWrapper>
        <Password variant='input'>
          <Input
            onInput={this | bindProp ('onInput')}
            hasTypo={hasTypo}
            type={'password'}
            concealInput={concealInput}
            clearKey={clearKey}
            scrollToInstructions={scrollToInstructions}
          />
        </Password>
      </PasswordWrapper>
      <PasswordDisplay
        password={password}
        showPassphrase={showPassphrase}
      />
      <ByTheWay show={password | ok}>
        If you haven't memorized your passphrase, <span className='x__close'>don't close this window</span> without writing it down first. This program won't save it for you.
      </ByTheWay>
    </div>
  }

  tabPending () {
    const { state, } = this
    const { instructions, countdownTarget, } = state

    return <div>
      <Instructions instructions={instructions} />
      <Countdown
        onTimeElapsed={this.startNewRound}
        target={countdownTarget}
        style='digits'
        interval={950}
      />
      <SkipCountdown skipCountdown={this.startNewRound}/>
    </div>
  }

  tabNoPassword () {
    return <PasswordDisplay
      password='<not yet generated>'
      showPassphrase={true}
    />
  }

  render () {
    const { props, state, } = this
    const { mode, roundIdx, } = state
    const { password, } = props

    return (
      <Top>
        { showDebugButtons && <div>
          <Button onClick={_ => notify (this.getNotifyText (), ++notify_id)}>
            notify now
          </Button>
          <Button onClick={_ => setTimeout (
            _ => callNotifyIfInBackground (...this.getNotifyText ()),
            10000,
          )}>
            notifybg in 10 secs
          </Button>
          <Button onClick={_ => setTimeout (
            _ => callNotify (this.getNotifyText ()),
            15 * 6e4,
          )}>
          notify in fifteen minutes
          </Button>
          <Button onClick={_ => quit ()}>
            quit
          </Button>
          <Button onClick={_ => alertError (document.location.toString ())}>
            show location
          </Button>
          <span>
            countdown: {roundIdx}
          </span>
        </div> }
        <TabWrapper> {
          mode | condS ([
            'active'  | eq | guard  (_ => this.tabActive ()),
            'pending' | eq | guard  (_ => this.tabPending ()),
            otherwise      | guard  (_ => this.tabNoPassword ()),
          ])
        } </TabWrapper>
      </Top>
    )
  }
}

MemorizeTab.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
})

const mapDispatchToProps = (dispatch) => ({
})

export default MemorizeTab
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'generateTab' })

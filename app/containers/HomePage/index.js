defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  split, prop, map, join, assoc,
  whenOk, sprintf1,
  eq, not, ifPredicate, whenPredicate,
  ifOk,
  ifTrue,
  prependTo,
  sprintfN,
  bindProp,
  whenTrue,
  concat, divideBy,
  whenFalse,
  always,
  mergeM,
  id,
  tap,
} from 'stick-js'

import { Nothing, Just, fold, } from 'alleycat-js/bilby'

import React, { PureComponent, Component, } from 'react'

import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import saga from './saga'

import configMod from 'alleycat-js/config'
import config from '../../config'

const { configGet, configGetOr, configGets, } = config | configMod.init

import { fetchForecast, changeLocation, } from '../App/actions'

import { makeSelectShowRain, } from '../../domains/app/selectors'

import {
  makeSelectAddingKey, makeSelectAddingKeysFromCSV,
  makeSelectAnalysingEvents,
} from 'domains/ui/selectors'

import Main from 'containers/Main'

import {
  spinner, alleyCatFooter,
} from 'alleycat-components/index'

import {
  toJS, whyYouRerender, alertError,
  iwarn, ierror,
  logAndroidPerf,
  mapX, tellIf,
  mlt, mgt, mediaComp, media,
} from 'app-util'

// --- simple check for mobile -- not really good though (monitor could have touch events)
const isMobile = _ => 'ontouchstart' in document.documentElement

const localConfig = {
  verbose: true,
  debug: {
    render: false,
  },
}

const tell = tellIf (localConfig.verbose)

const Spinner = 'simple' | spinner
const AlleyCatFooter = 'simple' | alleyCatFooter

const footerWidth = '300px'
const footerHeight = '21px'

const TopS = styled.div`
  height: 100%;
  .x--soft-keyboard-visible & {
    overflow-y: scroll;
  }
`

const Top = ({ children, innerref, }) => <TopS innerRef={innerref}>
  { React.Children.only (children) }
</TopS>

const bgImage = ['images', 'bg'] | configGet
const Wrapper = styled.div`
  ${ media (
    768 | mlt | mediaComp ('width: 100%;'),
    768 | mgt | mediaComp ('width: 800px;'),
  )}

  margin: auto;

  background-image: url(${bgImage});
  background-repeat: repeat;
  height: 100%;
`

const MainWrapper = styled.div`
  margin: auto;
  height: calc(100% - ${footerHeight});

`

export class HomePage extends PureComponent {
  constructor (props) {
    super (props)
    this | mergeM ({
      theRefs: {
        top: void 8,
        mainTabsWrapper: void 8,
      },
      state: {
        selectedIdx: -1,
      },
    })
  }

  componentWillReceiveProps (nextProps) {
    if (localConfig.debug.render) whyYouRerender ('main', this.props, nextProps)
  }

  // --- this is horrible.
  scrollToInstructions () {
    const { top, mainTabsWrapper, } = this.theRefs
    if (!top) return ierror ("Can't scroll to instructions: no top ref")
    if (!mainTabsWrapper) return ierror ("Can't scroll to instructions: no wrapper ref")

    const margin = 9
    const t = mainTabsWrapper.getBoundingClientRect ().top
    top.scrollTop = t - margin
  }

  render () {
    const { props, theRefs, } = this
    const { tab, } = props

    // <AlleyCatFooter width={footerWidth} height={footerHeight}/>

    return (
      <Top innerref={ref => this.theRefs.top = ref}>
        <Wrapper>
          <MainWrapper>
            <Main
              tab={tab}
              scrollToInstructions={this.scrollToInstructions.bind (this)}
              homeRefs={theRefs}
            />
          </MainWrapper>
        </Wrapper>
      </Top>
    )
  }
}

HomePage.propTypes = {
}

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
})

export default HomePage
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'HomePage', })

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const config = {
  verbose: true,
}

import {
  pipe, compose, composeRight,
} from 'stick-js'

import { call, put, select, takeLatest, take, } from 'redux-saga/effects'
import request from 'utils/request'

import {
  toJS, iwarn, tellIf,
  resolveP,
} from 'app-util'

export default function* rootSaga () {
}

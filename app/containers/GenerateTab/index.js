defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  sprintfN, condS, eq, otherwise, guard, guardV, gt,
  prop,
  tap,
  keys,
  path,
  bindPropTo,
  ifOk, ifTrue,
  id, noop,
  sprintf1,
  dot,
  split,
  ok,
  mergeM,
  join,
  take,
  whenTrue,
  whenOk,
  plus, minus,
  filter,
} from 'stick-js'

import React, { PureComponent, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectGenerateTab from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import {
  iwarn, ierror, tellIf,
  resolveP,
  logAndroidPerf,
  alertError,
  mapX, logWith, length,
  mlt, mgt, mediaComp, media,
  Left, Right, Just, Nothing, isNothing, fold,
  isMobile,
  addCommas,
} from 'app-util'

const compact = Boolean | filter

import animateWarningElement from '../../animateWarningElement'

import configMod from 'alleycat-js/config'
import config from '../../config'

const { configGet, configGetOr, configGets, } = config | configMod.init

import { acceptPassword, } from '../App/actions'
// import { } from '../../domains/domain/selectors'
// import { } from '../../domains/ui/selectors'
// import { } from '../../components/xxx'

import {
  ArrowFat,
  Repeat as RepeatIcon,
  Go as GoIcon,
} from 'alleycat-components/icons'

import {
  Password,
  TabWrapper,
} from 'components/shared'

import data from 'data'
import { getEntropy, calculate, generatePassphrase, } from 'calc'

const buttonIconWidth = 'buttonIconWidth' | configGet
const wordlistsMeta = 'wordlistsMeta' | configGet

const dictionaries = wordlistsMeta | keys

const ListSelectS = styled.div`
  text-align: center;
  position: relative;
  select {
    border-style: ridge;
    padding: 7px;
    ${ media (
      768 | mlt | mediaComp ('font-size: 15px;'),
      768 | mgt | mediaComp ('font-size: 1.8em;'),
    )}
  }
`

const ListSelect = ({
  onSelectDictionary, initialDict,
}) => <ListSelectS>
  <select
    onChange={path (['target', 'value']) >> onSelectDictionary}
    defaultValue={initialDict}
  > { dictionaries | mapX ((name, idx) => {
    const text = ['wordlistsMeta', name, 'name'] | configGet
    return <option
      key={idx}
      value={name}
    >{ text }</option>
  }) }
  </select>
</ListSelectS>

const Top = styled.div`
`

const NumWordsS = styled.div`
  ${ media (
    768 | mlt | mediaComp ('font-size: 1.4em;'),
    768 | mgt | mediaComp ('font-size: 2.0em;'),
  )}
  margin: auto;
  margin-top: 2vh;
  width: 100%;
  text-align: center;
  ${ prop ('warning') >> whenTrue (_ => colors.password.warning | sprintf1 ('color: %s;'))}
`

const ButtonWrapper = styled.div`
  ${ prop ('disabled') >> ifTrue (
    _ => 'opacity: 0.5;',
    _ => 'opacity: 1.0; cursor: pointer;',
  )}

  ${ prop ('warning') >> whenTrue (
    _ => colors | path (['password', 'warning']) | sprintf1 ('color: %s;'),
  )}


  &.x--rotate {
    transition: transform .2s;
    transform: rotate(359deg);
  }

  // display: flex-item;
  display: flex;
`

const Arrow = ({ disabled, rotate, stroke, onClick, }) => <ArrowFat
  rotate={rotate}
  width={buttonIconWidth}
  stroke={stroke}
  height={buttonIconWidth}
/>

const NumWords = ({ numWords, warning, }) => <NumWordsS warning={warning}>
  <FormattedMessage {...messages.numWords}
    values={{ num: numWords, }}
  />
</NumWordsS>

const ControlsS = styled.div`
`

const ButtonsS = styled.div`
  margin-top: 2vh;
  width: 100%;
  justify-content: space-evenly;
  align-items: center;
  flex-grow: 1;
  display: flex;
  font-size: 30px;
`

const Buttons = ({ increaseNumWords, decreaseNumWords, generate, memorize, warning, }) => <ButtonsS>
  <ButtonWrapper disabled={increaseNumWords | isNothing}
                 onClick={increaseNumWords | fold (id, noop)}
                 className='up-arrow'
                 warning={warning}
  >
    <Arrow rotate='-90' stroke={warning | whenTrue (_ => colors.password.warning)}/>
  </ButtonWrapper>
  <ButtonWrapper disabled={decreaseNumWords | isNothing}
                 onClick={decreaseNumWords | fold (id, noop)}
  >
    <Arrow rotate='90' />
  </ButtonWrapper>
  <ButtonWrapper
    onClick={generate | fold (id, noop)}
  >
    <RepeatIcon width={buttonIconWidth} height={buttonIconWidth}
                // disabled={generate | isNothing}
    />
  </ButtonWrapper>
  <ButtonWrapper disabled={memorize | isNothing}
                 onClick={memorize | fold (id, noop)}
  >
    <GoIcon width={buttonIconWidth} height={buttonIconWidth} />
  </ButtonWrapper>
</ButtonsS>

const Controls = ({
  numWords, onSelectDictionary, initialDict,
  increaseNumWords, decreaseNumWords, generate, memorize,
  warning,
}) => <ControlsS>
  <ListSelect onSelectDictionary={onSelectDictionary} initialDict={initialDict}
  />
  <NumWords numWords={numWords} warning={warning}/>
  <Buttons increaseNumWords={increaseNumWords} decreaseNumWords={decreaseNumWords}
           generate={generate} memorize={memorize}
           warning={warning}
  />
</ControlsS>

const InfoS = styled.div`
  ${ media (
    768 | mlt | mediaComp ('font-size: 0.95em; margin-top: 2vh;'),
    768 | mgt | mediaComp ('font-size: 1.5em; margin-top: 5vh;'),
  )}
  text-align: center;
  width: calc(100% - 2px);
  .x__num {
    text-decoration: underline;
  }
  .x__info {
    font-weight: bold;
  }
  .x__moreinfo {
  }
  .x__yields {
    margin-top: 2vh;
  }
  .x__willtake {
    margin-top: 2vh;
  }
`

const colors = 'colors' | configGet

const InfoGuess = styled.span`
  color: ${ prop ('strength') >> condS ([
    1 | gt    | guardV (colors.password.ok),
    otherwise | guardV (colors.password.warning),
  ])};
`

const Info = ({ numWords, description, wordCount, entropy, entropyPerWord, timeToGuess, strength, warning, }) => <InfoS>
  <div className='x__info'>
    { description }
  </div>
  <div className='x__moreinfo'>

  (
  <FormattedMessage {...messages.moreInfo.words}
    values={{
      num: <span className='x__num'>{wordCount | addCommas}</span>,
      numNum: wordCount,
    }}
  />
  →
  <FormattedMessage {...messages.moreInfo.bitsPerWord}
    values={{
      num: <span className='x__num'>{entropyPerWord | sprintf1 ('%.2f')}</span>,
      numNum: entropyPerWord,
    }}
  />
  )
  </div>

  <div className='x__yields'>
    <FormattedMessage {...messages.moreInfo.bitsTotal}
      values={{
        numWordsWord:
          <span className='x__num'>
            <FormattedMessage {...messages.moreInfo.numWordsWord}
              values={{
                numWords,
              }}
            />
          </span>,
        numBits: <span className='x__num'>{
          entropy | sprintf1 ('%.2f')
        }</span>,
        numBitsNum: entropy,
      }}
    />
  </div>

  <div className='x__willtake'>
    <FormattedMessage {...messages.moreInfo.willTake}
      values={{
        guess: <FormattedMessage {...messages.moreInfo[warning ? 'warningGuess' : 'guess']}
          values={{
            guess: <InfoGuess strength={strength}>
              <FormattedMessage {...messages.moreInfo[warning ? 'onlyTime' : 'time']}
                values={{
                  time: timeToGuess [0],
                  units: <FormattedMessage {...messages.timeUnits[timeToGuess [1]]}
                  />
                }}
              />
            </InfoGuess>,
          }}
        />,
      }}
    />
  </div>
</InfoS>

const initialDict = 'dutch'
const initialNumWords = 7
const maxNumWords = 10
const minNumWords = 3
const guessesPerSecond = 1e12

export class GenerateTab extends React.PureComponent {
  constructor (props) {
    super (props)

    this | mergeM ({
      currentName: initialDict,
      upArrowAnimator: void 8,

      // --- these are copied into the state as well.
      // --- we use the instance vars as the source of truth because setState is not synchronous.
      numWords: initialNumWords,
      password: void 8,
    })

    this.state = {
      // --- must be kept in sync with the instance vars.
      numWords: this.numWords,
      password: this.password,

      entropy: void 8,
      entropyPerWord: void 8,
      strength: void 8,
      timeToGuess: void 8,
      selectedList: {
        wordCount: void 8,
        words: void 8,
        description: void 8,
      },
    }
  }

  componentWillMount () {
    this.updateState ()
  }

  componentDidMount () {
    // ------ this is ported from old jquery code, hence this approach.
    const arrow = document.querySelector ('.up-arrow')
    const animator = animateWarningElement.create ({
      elem: arrow,
      rotatorInterval: 1500,
    }).init ()
    this.upArrowAnimator = animator
  }

  onSelectDictionary (which) {
    this.currentName = which
    this.clearPassword (false)
    this.updateState ()
  }

  updateState () {
    const { state, numWords, password, currentName: name, upArrowAnimator: animator, } = this
    const words = data [name]
    const wordCount = words | length
    const description = ['wordlistsMeta', name, 'description'] | configGet
    const [entropyPerWord, entropy] = getEntropy (wordCount, numWords)

    const [timeValue, timeUnits, strength] = calculate (guessesPerSecond, entropyPerWord, wordCount, numWords)
    const timeToGuess = [timeValue | sprintf1 ('%.1f'), timeUnits]

    animator | whenOk ((animator) => {
      animator.stop ()
      if (strength < 2) animator.go ()
    })

    this.setState ({
      entropy,
      entropyPerWord,
      numWords,
      password,

      timeToGuess,
      strength,

      selectedList: {
        wordCount,
        words,
        description,
      }
    })
  }

  _alterNumWords (f) {
    this.numWords = this.numWords | f
    this.clearPassword (false)
    this.updateState ()
  }

  increaseNumWords () {
    this._alterNumWords (1 | plus)
  }

  decreaseNumWords () {
    this._alterNumWords (1 | minus)
  }

  clearPassword (updateState = true) {
    this.password = void 8
    if (updateState) this.updateState ()
  }

  generate () {
    const { state, numWords, } = this
    const { selectedList, } = state
    const { words, } = selectedList
    this.password = generatePassphrase (words, numWords)
    this.updateState ()
  }

  memorize () {
    const { props, } = this
    const { state, } = this
    const { password, numWords, selectedList, } = state
    const { acceptPasswordDispatch, } = props
    acceptPasswordDispatch (password, numWords, selectedList)
    document.location.hash = '/memorize'
  }

  render () {
    const { state, } = this
    const {
      numWords, password, selectedList,
      entropy, entropyPerWord, timeToGuess, strength,
    } = state

    const { name, words, description, wordCount, } = selectedList
    const onSelectDictionary = 'onSelectDictionary' | bindPropTo (this)

    const canIncreaseNumWords = this.state.numWords < maxNumWords
    const canDecreaseNumWords = this.state.numWords > minNumWords
    const canGenerate = true
    const canMemorize = password | ok

    const warning = strength < 2

    const increaseNumWords = canIncreaseNumWords ? 'increaseNumWords' | bindPropTo (this) | Just
                                                 : Nothing
    const decreaseNumWords = canDecreaseNumWords ? 'decreaseNumWords' | bindPropTo (this) | Just
                                                 : Nothing
    const generate = canGenerate ? 'generate' | bindPropTo (this) | Just
                                 : Nothing

    const memorize = canMemorize ? 'memorize' | bindPropTo (this) | Just
                                 : Nothing

    return (
      <Top>
        <TabWrapper>
          <Controls numWords={numWords} onSelectDictionary={onSelectDictionary}
                    initialDict={initialDict}
                    increaseNumWords={increaseNumWords}
                    decreaseNumWords={decreaseNumWords}
                    generate={generate} memorize={memorize}
                    warning={warning}
          />
          <Password password={password} warning={warning} />
          <Info
            numWords={numWords}
            description={description}
            wordCount={wordCount}
            entropy={entropy}
            entropyPerWord={entropyPerWord}
            timeToGuess={timeToGuess}
            strength={strength}
            warning={warning}
          />
        </TabWrapper>
      </Top>
    )
  }
}

const mapStateToProps = createStructuredSelector ({
})

const mapDispatchToProps = (dispatch) => ({
  acceptPasswordDispatch: acceptPassword >> dispatch,
})

export default GenerateTab
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'generateTab' })

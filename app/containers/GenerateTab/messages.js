defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
} from 'stick-js'

import {
  transformIntlComponent,
} from 'app-util'

import { defineMessages, } from 'react-intl'
import { msgs, } from 'translations/en'

const idPref = 'app.containers.GenerateTab'

export default msgs [idPref]
  | transformIntlComponent (idPref)
  | defineMessages

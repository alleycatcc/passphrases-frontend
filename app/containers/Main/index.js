defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  join, tap, not, sprintfN, ifOk, whenOk, assocM, noop,
  list, defaultTo,
  lets,
  prop,
  reduce, mergeM,
  map, addIndex,
  whenTrue,
  condS, guard, otherwise,
  eq,
  mapTuples, values,
  die,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectMain from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import GenerateTab from 'containers/GenerateTab'
import MemorizeTab from 'containers/MemorizeTab'
import SettingsTab from 'containers/SettingsTab'

import {
  mapX, tellIf,
  iwarn, ierror,
  ierrorErrorDie,
  logAndroidPerf,
  logWith,
  whyYouRerender,
  error as alertError,
  Just, Nothing,
  isMobile,
} from 'app-util'

import configMod from 'alleycat-js/config'
import config from '../../config'

const { configGet, configGetOr, configGets, } = config | configMod.init

import { tabs, } from 'alleycat-components'

import { changeTab, } from '../App/actions'

// import { } from '../../domains/domain/selectors'
// import { } from '../../domains/app/selectors'

import { makeSelectTab, makeSelectPassword, makeSelectNumWords, makeSelectSelectedList, } from 'domains/ui/selectors'

const fromPairs = x => x
  | reduce ((acc, [k, v]) => acc | assocM (k, v)) ({})

const Tabs = tabs ('nav-id-swiper')

const TabHeaderS = styled.div``
const TabHeader = ({ text, idx, }) => <TabHeaderS>
  Header: {text} {idx}
</TabHeaderS>
const tabHeader = (props) => <TabHeader { ... props } />

const Tab = ({ idx, tag, numWords, selectedList, password, scrollToInstructions, }) => tag | condS ([
  'generate' | eq | guard (_ => <GenerateTab />),
  'memorize' | eq | guard (_ => <MemorizeTab
    password={password}
    numWords={numWords}
    selectedList={selectedList}
    scrollToInstructions={scrollToInstructions}
  />),
  'settings' | eq | guard (_ => <SettingsTab />),
  otherwise | guard (tab => ierrorErrorDie ('bad tab:', tab) >> die ('bad')),
])

// --- works ok, but the props are sent to all the tabs.
const tabContents = ({ numWords, selectedList, password, scrollToInstructions, }) => ({ idx, tag, swiperInstance, isCurTab, }) => <Tab
  idx={idx}
  tag={tag}
  isCurTab={isCurTab}
  password={password}
  numWords={numWords}
  selectedList={selectedList}
  scrollToInstructions={scrollToInstructions}
/>

const navBarHeight = '15vh'

const Top = styled.div`
  height: 100%;
  margin: auto;
  border: 1px solid black;

// display: flex;
// flex-direction: column;
// justify-content: flex-start;

  .x--soft-keyboard-visible & {
    height: 10000px;
  }
`

const img1 = ['images', 'dice'] | configGet
const img2 = ['images', 'head'] | configGet
const img3 = ['images', 'options'] | configGet

const NavBarS = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  padding-top: 5%;
  padding-bottom: 10%;
  img {
    height: 100%;
    width: 100%;
  }
  height: ${navBarHeight};
`

const NavBarItemS = styled.div`
  display: flex-item;
  cursor: pointer;
  position: relative;
  width: 4vw;
  height: 4vw;
  min-width: 40px;
  min-height: 40px;

  &:nth-child(3) {
    img {
      width: 80%;
      height: 80%;
      margin: auto;
    }
  }

  &.x--active {
    .x__border {
      opacity: 1 !important;
    }
  }
  &:hover {
    .x__label {
      opacity: 1;
    }
  }

.x__border, .x__label {
  width: 80%;
  position: absolute;
  left: 0px;
  opacity: 0;
  transition: opacity 200ms;
}

.x__border {
  bottom: -28px;
  z-index: 100;
  height: 1px;
  background: black;
  transform: translateX(10%);
}
.x__label {
  color: #002200;
  bottom: -60px;
  font-size: 15px;
  font-family: sans;
  left: -10px;
}
`

const NavBarItem = ({ isActive, img, text, onClick, }) => {
  return <NavBarItemS
    className={isActive ? 'x--active' : ''}
    onClick={onClick}
  >
    <img src={img}/>
    <div className='x__border'/>
    <div className='x__label'>
      { isMobile () ? null : text }
    </div>
  </NavBarItemS>
}

const data = [
  [img1, 'generate', 'generate'],
  [img2, 'memorize', 'memorize'],
  [img3, 'settings', 'settings'],
]

const NavBar = ({ changeTabDispatch, curTag, }) => <NavBarS>
  { data | mapX (([img, tag, text], idx) =>
    <NavBarItem
      key={idx}
      img={img}
      text={text}
      isActive={curTag === tag}
      onClick={_ => tag | changeTabDispatch}
    />
  )}
</NavBarS>

const TabsWrapperS = styled.div`
  padding: 5%;
  padding-top: 2vh;
  width: 100%;

  height: calc(100% - ${navBarHeight});
  // height: 100%;

// display: flex-item;
// flex-grow: 1;
// border: 1px solid black;
`

const TabsWrapper = ({ children, innerref, }) => <TabsWrapperS innerRef={innerref}>
  { React.Children.only (children) }
</TabsWrapperS>

export class Main extends PureComponent {
  constructor (props) {
    super (props)
    this.swiperInstance = void 8
  }

  componentDidMount () {
    const { props, } = this
    const { tab = 'generate', tabFromStore, changeTabDispatch, } = props
    if (!tabFromStore) tab | changeTabDispatch
  }

  render () {
    const { props, } = this
    const {
      tabFromStore, password, numWords, selectedList,
      changeTabDispatch, changeTabWithDelayDispatch,
      scrollToInstructions,
      homeRefs,
    } = props

    if (!tabFromStore) return null

    return <Top>
      <NavBar
        changeTabDispatch={changeTabDispatch}
        curTag={tabFromStore}
      />
      <TabsWrapper
        innerref={ref => homeRefs.mainTabsWrapper = ref}
      >
        <Tabs
          tags={['generate', 'memorize', 'settings']}
          curTab={tabFromStore}
          onSwipeDispatch={changeTabWithDelayDispatch}
          tabContents={tabContents ({ numWords, selectedList, password, scrollToInstructions, })}
          opts={{ onlyMobile: true, }}
        />
      </TabsWrapper>
    </Top>
  }
}

Main.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
  tabFromStore: makeSelectTab (),
  password: makeSelectPassword (),
  numWords: makeSelectNumWords (),
  selectedList: makeSelectSelectedList (),
})

const swipeDelay = 'swipeDelay' | configGet

const mapDispatchToProps = (dispatch) => ({
  changeTabDispatch: changeTab >> dispatch,
  changeTabWithDelayDispatch: tab => changeTab (tab, swipeDelay) | dispatch,
})

export default Main
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'main' })

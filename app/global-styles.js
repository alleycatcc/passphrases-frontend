import { injectGlobal } from 'styled-components';

const config = {
}

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html {
    height: 100vh;
    width: 100%;
  }

  body {
    background-size: 100%;
    @media only screen and (max-width: 768px) {
      // --- soft navigation buttons.
      height: calc(100vh - 60px);
    }
    @media only screen and (min-width: 768px) {
      height: calc(100vh - 0px);
    }
    width: 100vw;
    color: black;
    background-color: white;
  }

  body.ReactModal__Body--open {
    background-image: none;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
`;

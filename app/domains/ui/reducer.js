defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, whenOk, dot,
  drop, assoc,
  tap, eq,
  condS, guard, otherwise, guardV,
  id,
  always,
  prop, path,
  merge,
} from 'stick-js'

import {
  reducer,
  Just, Nothing, foldJust,
  deconstruct,
  logWith,
  getQueryParam,
} from 'app-util'

import {
  CHANGE_LOCALE,
  CHANGE_TAB_REAL, ACCEPT_PASSWORD, LOCATION_CHANGE_REACT_ROUTER,
} from 'containers/App/constants'

const initialState = {
  locale: 'en',

  tab: void 8,
  password: getQueryParam ('passphrase'),
  numWords: getQueryParam ('numWords') | whenOk (Number),
  selectedList: getQueryParam ('selectedList'),

  countdownCur: void 8,
  countdownTarget: 10 | Just,
}

const reducerTable = {
  // --- react router.
  [LOCATION_CHANGE_REACT_ROUTER]: path (['payload', 'pathname']) >> drop (1) >> assoc ('tab'),

  [CHANGE_LOCALE]:                prop ('data') >> assoc ('locale'),

  [CHANGE_TAB_REAL]:              prop ('data') >> assoc ('tab'),
  [ACCEPT_PASSWORD]:              prop ('data') >> (({ passphrase, numWords, selectedList, }) =>
    merge ({
      numWords,
      selectedList,
      password: passphrase,
    })
  ),
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)

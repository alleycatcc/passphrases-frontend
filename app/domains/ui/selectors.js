// ------ in the passphrases app we're using the UI domain a lot because no data comes in from
// outside.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
  add, whenOk,
  tap,
  prop,
  ifOk, always,
} from 'stick-js'

import {
  length,
} from 'alleycat-js/general'

import {
  logWith,
} from 'app-util'

const selectDomain = prop ('ui')

export const makeSelectTab = _ => createSelector (
  selectDomain,
  prop ('tab'),
)

export const makeSelectPassword = _ => createSelector (
  selectDomain,
  prop ('password'),
)

export const makeSelectNumWords = _ => createSelector (
  selectDomain,
  prop ('numWords'),
)

export const makeSelectSelectedList = _ => createSelector (
  selectDomain,
  prop ('selectedList'),
)

// --- this one happens extremely early, before the reducers have been connected.
export const makeSelectLocale = _ => createSelector (
  selectDomain,
  ifOk (prop ('locale'), 'en' | always),
)

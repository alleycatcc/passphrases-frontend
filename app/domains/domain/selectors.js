// ------ sending a 'slice' parameter to a makeSelector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  map, dot, dot1, whenOk, ifOk,
  gt, ok, ne, eq, nieuw1,
  prop,
} from 'stick-js'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

const selectDomain = prop ('domain')

export const makeSelectError = _ => createSelector (
  selectDomain,
  prop ('error'),
)

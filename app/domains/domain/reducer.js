defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  assoc,
  id, always,
} from 'stick-js'

import {
  reducer,
  logAndroidPerf, logWith,
} from 'app-util'

// import { } from '../../containers/App/constants'

const initialState = ({
  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,
})

const reducerTable = {
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  assoc, eq, id,
  condS, guard, otherwise, guardV,
  prop,
} from 'stick-js'

import {
  reducer,
} from 'app-util'

// import { } from '../../containers/App/constants'

const UPDATE_DEBUG = 'stub'

const initialState = {
  debug: false,

  // --- `true` means the reducer is totally corrupted and the app should halt.
  error: false,
}

const reducerTable = {
  [UPDATE_DEBUG]: prop ('data') >> assoc ('debug'),
}

export default (state = initialState, action) => state | reducer (reducerTable) (action)

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk,
  prop,
} from 'stick-js'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import {
  length, logWith,
} from 'app-util'

const selectApp = prop ('app')

export const makeSelectDebug = _ => createSelector (
  selectApp,
  prop ('debug'),
)

export const makeSelectShowRain = _ => createSelector (
  selectApp,
  prop ('showRain'),
)


/*

// ---some old examples:
export const makeSelectUploadedEventsResultsLastSuccess = _ => createSelector (
  makeSelectUploadedEventsResults (),
  whenOk (get ('lastSuccess') >> toOrderedMap >> iSortBy (
    (_, name) => name,
  ))
)

*/

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
} from 'stick-js'

import {
  Just, Nothing,
  isMobile,
} from 'app-util'

export default {
  title: 'Passphrases',
  sourceLink: 'https://gitlab.com/alleycatcc/passphrases-frontend.git',
  images: {
    bg: require ('./images/background-texture.png'),
    dice: require ('./images/dice.png'),
    head: require ('./images/head.svg'),
    options: require ('./images/option-horizontal.svg'),
  },
  colors: {
    password: {
      ok: '#184518',
      warning: '#ab0a0a',
    },
    guess: {
      wrong: '#ab0a0a',
    }
  },
  buttonIconWidth: isMobile () ? 30 : 40,
  // --- use Nothing for none.
  swipeDelay: 100 | Just,
  countdownInterval: 50,
  memorizer: {
    test: {
      goodJob: Nothing,
      numTries: 2,
      concealAfter: 1,
    },
    normal: {
      numTries: 10,
      concealAfter: 5,
      goodJob: 5,
    },
  },
  wordlistsMeta: {
    securedrop: {
      name: 'SecureDrop',
      description: 'English wordlist from the SecureDrop project',
    },
    english: {
      name: 'English Diceware',
      description: 'Original English Diceware wordlist',
    },
    catalan: {
      name: 'Catalan Diceware',
      description: 'Diccionari català per Marcel Hernandez - CC-BY 4.0',
    },
    german: {
      name: 'Deutsch Diceware',
      description: 'Deutsch Wörterbuch von Benjamin Tenne - GPL',
    },
    french: {
      name: 'Français Diceware',
      description: 'Dictionnaire français par Matthieu Weber',
    },
    italian: {
      name: 'Italiano Diceware',
      description: 'Lista di parole Diceware in Italiano by Tarin Gamberini - GPL',
    },
    japanese: {
      name: 'Japanese Diceware',
      description: 'Japanese Diceware by Hiroshi Yuki & J Greely - CC-BY-SA',
    },
    dutch: {
      name: 'Dutch Diceware',
      description: 'Nederlandse Diceware door Remko Tronçon',
    },
    polish: {
      name: 'Polski Diceware',
      description: 'Polski Słownik przez Piotr (DrFugazi) Tarnowski',
    },
    swedish: {
      name: 'Svensk Diceware',
      description: 'Svensk ordbok av Magnus Bodin',
    },
  },
}

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  assoc,
  id,
  condS, guard, otherwise, guardV,
  eq,
} from 'stick-js'

import {
  deconstruct,
} from 'app-util'

import { combineReducers, } from 'redux'
import { LOCATION_CHANGE, } from 'react-router-redux'

const routeInitialState = {
  location: null,
}

const routeReducer = (state = routeInitialState, action) => action | deconstruct (
  ({ payload, data, type, }) => type | condS ([
    LOCATION_CHANGE | eq | guard (assoc ('location') (payload)),
    otherwise            | guard (id),
  ])
)

export default function createReducer (injectedReducers) {
  return combineReducers ({
    // --- react router.
    route: routeReducer,
    // --- all our reducers are added here -- none has a 'special' status in the hierarchy.
    ...injectedReducers,
  })
}

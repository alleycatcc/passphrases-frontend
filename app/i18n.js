import { addLocaleData } from 'react-intl'
import enLocaleData from 'react-intl/locale-data/en'
import nlLocaleData from 'react-intl/locale-data/nl'

const DEFAULT_LOCALE = 'en'

import enTranslationMessages from './translations/en.js'
import nlTranslationMessages from './translations/nl.js'

addLocaleData (enLocaleData)
addLocaleData (nlLocaleData)

export const appLocales = [
  'en',
  'nl',
]

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages = locale !== DEFAULT_LOCALE
    ? formatTranslationMessages (DEFAULT_LOCALE, enTranslationMessages)
    : {}
  return Object.keys (messages).reduce ((formattedMessages, key) => {
    const formattedMessage = !messages[key] && locale !== DEFAULT_LOCALE
      ? defaultFormattedMessages[key]
      : messages[key]
    return Object.assign (formattedMessages, { [key]: formattedMessage })
  }, {})
}

export const translationMessages = {
  en: formatTranslationMessages ('en', enTranslationMessages),
  nl: formatTranslationMessages ('nl', nlTranslationMessages),
}

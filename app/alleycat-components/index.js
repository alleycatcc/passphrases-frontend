defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ifTrue, blush, ifOk, id,
  guard, otherwise,
  ok, eq, invoke,
  condS,
  concat, prop,
  join,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import styled from 'styled-components'

import ReactSAlert from 'react-s-alert'

import 'react-s-alert/dist/s-alert-default.css'
// --- optional.
import 'react-s-alert/dist/s-alert-css-effects/slide.css'

import TooltipComponent from 'rc-tooltip'
import SpinnerRB_ from './LoadingIndicatorRB'
import NavIdSwiper from './NavIdSwiper'

import {
  ElemP,
  whyYouRerender,
} from 'app-util'

export const H4 = ({ children }) =>
  <H4S>
    <H4Inner>
      { children }
    </H4Inner>
  </H4S>

export const H3 = ({ children }) =>
  <H3S>
    <H3Inner>
      { children }
    </H3Inner>
  </H3S>

export const H2 = ({ children }) =>
  <H2S>
    <H2Inner>
      { children }
    </H2Inner>
  </H2S>

const H4S = styled.div`
  font-size: 1em;
  margin-bottom: 5px;
`

const H2S = styled.div`
  font-size: 1.5em;
  margin-bottom: 5px;
  display: flex;
  text-decoration: underline;
  justify-content: center;
`

const H4Inner = styled.span`
`

const H2Inner = styled.span`
`

const H3Inner = styled.span`
  border-bottom: 1px dotted black;
`

const H3S = styled.div`
  font-size: 1.2em;
  margin-bottom: 5px;
  &:before {
    content: ''
  }
`

const InputS = styled.input`
  border-style: inset;
  padding: 5px;
  width: ${ prop ('width') >> ifOk (
    id, '100%' | blush,
  )};
`

export const Input = ElemP (InputS)

export const TextArea = ({ onChange, defaultValue, }) => <TextAreaS
    onChange={onChange}
    defaultValue={defaultValue}
/>

const TextAreaS = styled.textarea`
  border: 1px solid blue;
  padding: 30px;
  height: 200px;
  width: 100%;
`

const ButtonS = styled.button`
  border: 1px solid black;
  position: relative;
  color: antiquewhite;
  padding: 10px;
  background: grey;
  &:not(:disabled) {
    background: darkred;
    cursor: pointer;
    .overlay {
      display: none;
    }
  }
  .overlay {
    background: black;
    opacity: 0.5;
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
  }
`

export const Button = ({ onClick, disabled, children, }) => <ButtonS
  onClick={onClick}
  disabled={disabled}
>
  <div className='overlay'/>
  {children}
</ButtonS>

const IconFAS = styled.div`
  display: inline-block;
`

export const IconFA = ({ icon, style = {}, }) => <IconFAS>
  <i
    className={'fa ' + icon}
	style={style}
  />
</IconFAS>

const TooltipComponentS = styled (TooltipComponent)`
`

export const Tooltip = ({ placement, overlay, children, }) => <TooltipComponentS
  placement={placement}
  overlay={overlay}
>
  <div style={{display: 'inline-block'}}>
    {children}
  </div>
</TooltipComponentS>

const AlertS = styled.div`
  .s-alert-box {
    background: darkred;
    border: 1px solid black;
    font-size: 20px;
    padding: 10px 30px;

    max-width: 500px;
  }
`

export const Alert = _ => <AlertS>
  <ReactSAlert
    stack={{limit: 3}}
    effect='slide'
    timeout={60000}
  />
</AlertS>

export const Footer = styled.div`
  font-size: 11px;
  color: black;
  margin: auto;
  width: ${prop ('width') >> ifOk (id) (_ => 'inherit')};
  height: ${prop ('height') >> ifOk (id) (_ => 'inherit')};
  text-align: center;
  height: 21px;
`

export const alleyCatFooter = condS ([
  'simple' | eq | guard (() =>
    (props) => <Footer {...props}>
      Site by AlleyCat Amsterdam | <a target='_blank' href="http://alleycat.cc">alleycat.cc</a>
    </Footer>
  ),
  otherwise | guard (_ => 'simple' | alleyCatFooter),
])

// --- String (type) -> <Spinner>

export const spinner = condS ([
  'simple' | eq | guard (() => (props) => <SpinnerRB {...props}/>),
  otherwise | guard (_ => 'simple' | spinner),
])

export const SpinnerRB = ({ height, width, factor, }) => <SpinnerRB_
  height={height}
  width={width}
  factor={factor}
/>

// --- String (type) -> <Tabs>
export const tabs = condS ([
  'nav-id-swiper' | eq | guard (() => (props) => <NavIdSwiper {... props} />),
  otherwise | guard (_ => 'nav-id-swiper' | tabs),
])

const FontAwesomeS = styled.span`
  font-family: 'FontAwesome';
`

export const FontAwesome = ({ i, }) => {
  const className = ['fas', 'fa-' + i] | join (' ')
  return <FontAwesomeS className={className} />
}


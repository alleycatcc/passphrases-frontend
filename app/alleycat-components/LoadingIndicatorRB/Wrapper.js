defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  sprintf1, ok, dot1, times, dot, whenOk,
  prop,
  compact,
  ifTrue, guard, otherwise,
  defaultTo,
} from 'stick-js'

import styled from 'styled-components'

export default styled.div`
  margin: 2em auto;
  width:  ${prop ('width')  >> defaultTo (_ => '40px') };
  height: ${prop ('height') >> defaultTo (_ => '40px') };
  position: relative;
`

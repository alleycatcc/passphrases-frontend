import React from 'react';

import Circle from './Circle';
import Wrapper from './Wrapper';

// --- higher factor means slower spinner.
// strange artifact at top of spinner though on non-1.

const LoadingIndicator = ({ width, height, factor = 1, }) => (
  <Wrapper height={height} width={width}>
    <Circle />
    <Circle rotate={30} delay={-1.1 * factor} factor={factor}/>
    <Circle rotate={60} delay={-1 * factor} factor={factor}/>
    <Circle rotate={90} delay={-0.9 * factor} factor={factor}/>
    <Circle rotate={120} delay={-0.8 * factor} factor={factor}/>
    <Circle rotate={150} delay={-0.7 * factor} factor={factor}/>
    <Circle rotate={180} delay={-0.6 * factor} factor={factor}/>
    <Circle rotate={210} delay={-0.5 * factor} factor={factor}/>
    <Circle rotate={240} delay={-0.4 * factor} factor={factor}/>
    <Circle rotate={270} delay={-0.3 * factor} factor={factor}/>
    <Circle rotate={300} delay={-0.2 * factor} factor={factor}/>
    <Circle rotate={330} delay={-0.1 * factor} factor={factor}/>
  </Wrapper>
);

export default LoadingIndicator;

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  prop, sprintfN, timesV, whenOk,
  sprintf1,
  T,
} from 'stick-js'

import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

const circleFadeDelay = keyframes`
  0%,
  39%,
  100% {
    opacity: 0;
  }

  40% {
    opacity: 1;
  }
`;

//     ${props.rotate && `
//       -webkit-transform: rotate(${props.rotate}deg);
//       -ms-transform: rotate(${props.rotate}deg);
//       transform: rotate(${props.rotate}deg);
//     `}
//       ${props.delay && `
//         -webkit-animation-delay: ${props.delay}s;
//         animation-delay: ${props.delay}s;
//       `}

const animationTime = (factor = 1) => (1.2 * factor) | sprintf1 ('%ss')

const Circle = (props) => {
  const CirclePrimitive = styled.div`
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;

    ${ prop ('rotate') >> whenOk (timesV (3) >> sprintfN (`
      -webkit-transform: rotate(%sdeg);
      -ms-transform: rotate(%sdeg);
      transform: rotate(%sdeg);
    `)) }

    &:before {
      content: '';
      display: block;
      margin: 0 auto;
      width: 15%;
      height: 15%;
      background-color: #999;
      border-radius: 100%;
      animation: ${circleFadeDelay} ${ prop ('factor') >> animationTime } infinite ease-in-out both;
      ${ prop ('delay') >> whenOk (timesV (2) >> sprintfN (`
        -webkit-animation-delay: %ss;
        animation-delay: %ss;
      `)) }
    }
  `;
  return <CirclePrimitive { ... props } />;
};

Circle.propTypes = {
  delay: PropTypes.number,
  rotate: PropTypes.number,
};

export default Circle;

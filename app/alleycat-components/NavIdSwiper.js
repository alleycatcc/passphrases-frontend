defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  join, tap, not, sprintfN, ifOk, whenOk, assocM, noop,
  list, defaultTo,
  lets,
  prop,
  reduce, mergeM,
  map, addIndex,
  whenTrue,
} from 'stick-js'

import React, { PureComponent, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import Swiper from 'react-id-swiper'
import 'react-id-swiper/src/styles/css/swiper.css'

import {
  mapX, tellIf,
  iwarn, ierror,
  logAndroidPerf,
  logWith,
  whyYouRerender,
  error as alertError,
  ierrorErrorDie,
  Just, Nothing,
  isMobile,
} from 'app-util'

const fromPairs = x => x
  | reduce ((acc, [k, v]) => acc | assocM (k, v)) ({})

const touchRatio = 1

const tab = (swiperInstance, tabContents, curIdx) => (tag, idx) => <Tab key={idx}> {
  tabContents ({ idx, tag, swiperInstance, isCurTab: idx == curIdx, })
} </Tab>

const SwiperContainer = styled.div`
  height: 100%;
  .swiper-container {
    height: 100%;
  }
`

const Tab = styled.div`
  height: 100%;
`

export default class NavIdSwiper extends PureComponent {
  constructor (props) {
    super (props)
    this.swiperInstance = void 8
  }

  swiperParams () {
    const { props, } = this
    const { tags, onSwipeDispatch = noop, } = props

    return {
      touchRatio,

      on: {
        init: noop,
        slideChangeTransitionStart: _ => lets (
          _ => this.swiperInstance | whenOk (
            si => tags [si.activeIndex],
          ),
          whenOk (onSwipeDispatch),
        ),
      },
    }
  }

  swiper (idxForTag, tags, curTab, { onlyMobile=false, } = {}) {
    const { props, } = this
    const { tabContents = noop, } = props

    const curIdx = idxForTag [curTab] | defaultTo (_ => {
      ierrorErrorDie ('no tab for tag:', curTab)
    })

    if (this.swiperInstance) this.swiperInstance.slideTo (curIdx)

    return <Swiper {... this.swiperParams ()}
      initialSlide={idxForTag [curTab] | Number}
      ref={whenOk (({ swiper, }) => {
        this.swiperInstance = swiper
        if (onlyMobile && ! isMobile ()) swiper.allowTouchMove = false
      }
      )}
      style={{
        height: '100%',
      }}
    >
      { tags | mapX (tab (this.swiperInstance, tabContents, curIdx)) }
    </Swiper>
  }

  render () {
    const { props, } = this
    const { tags = [], curTab = tags [0], opts, } = props

    const idxForTag = tags
      | mapX ((tag, idx) => [tag, idx])
      | fromPairs

    return <SwiperContainer>
      { this.swiper (idxForTag, tags, curTab, opts) }
    </SwiperContainer>
  }
}

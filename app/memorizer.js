defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const TEST = process.env.NODE_ENV !== 'production'

import {
  pipe, compose, composeRight,
  mergeM,
  factory, factoryProps,
  tap,
  each,
  always,
  condS, eq, againstAll, guardV, lt, otherwise,
  ifTrue, whenOk,
  prop, sprintf1, join,
  side, side1, side2,
  updateM, add,
  not,
  noop,
  passToN,
  arg3,
  id,
  gte,
  arg2,
  multiply,
  map,
  appendTo,
  minus,
  lets,
} from 'stick-js'

import daggy from 'daggy'

import {
  Just, Nothing, cata,
  logWith,
  fold,
} from 'app-util'

import configMod from 'alleycat-js/config'
import config from 'config'

const { configGet, configGetOr, configGets, } = config | configMod.init

// xxx stick
const V = void 8 | always
const N = null   | always

const { floor, min: mathMin, } = Math

const min = a => b => mathMin (a, b)

const countdownValues = TEST ? [10, 11, 12, 13, 14, 15] : [60, 120, 300, 600, 1800, 3600]

export const Memorizer = daggy.taggedSum ('Memorizer', {
  MemActive: [
    'countdownTarget', 'instructions', 'tries',
    'hasTypo', 'concealInput,'
  ],
  MemPendingNotify: ['timeLeft', 'instructions', 'roundIdx'],
})

const { MemActive, MemPendingNotify, } = Memorizer

// --- this is for updating one of the fields inside a catamorphism.
// --- argN is the argument number in the constructor's arguments.
//
// --- e.g., to map the fourth field:
//
// cata ({
//   MemActive: f | cataMapper (MemActive) (3)
//
//   // 'identity'
//   MemPendingNotify,
//   ...
//
// })

const cataMapper = constructor => argN => f =>
  (...args) => args | updateM (argN) (f) | passToN (constructor)

// xxx stick
const arg = n => (...args) => args [n]

const memPendingNotify = idx => _ => lets (
  _ => idx | min (countdownValues | length) | minus (1),
  valKey => MemPendingNotify (
    countdownValues [valKey] * 1000,
    'Give your mind a rest.',
    idx,
  )
)

const memUpdateMemActive = argN => f => cata ({
  MemActive: f | cataMapper (MemActive) (argN),

  MemPendingNotify,
})

const memGetMemActive = argN => cata ({
  MemActive: argN | arg,
  MemPendingNotify: V,
})

const memUpdateCountdownTarget = memUpdateMemActive (0)
const memUpdateInstructions    = memUpdateMemActive (1)
const memUpdateTries           = memUpdateMemActive (2)
const memUpdateHasTypo         = memUpdateMemActive (3)
const memUpdateConcealInput    = memUpdateMemActive (4)

const memGetTries = memGetMemActive (2)

// Num (tries) -> Maybe (countdown) -> (Num (ms) -> Num (ms))
const countdownUpdaterMap = condS ([
  1  | eq   | guardV (5000 | always),
  6  | lt   | guardV (2000 | add),
  10 | lt   | guardV (1000 | add),
  otherwise | guardV (id),
])

const countdownUpdater = tries => fold (
  tries | countdownUpdaterMap,
  5000,
) >> Just

const length = prop ('length')

const onUpdateState = side1 ('onUpdateState')
const onNextTry = side ('onNextTry')
const onModeChange = side1 ('onModeChange')
const onUpdateCountdown = side2 ('onUpdateCountdown')

const cfgPath = TEST ? 'test' : 'normal'
const makeCfgPath = appendTo (['memorizer', cfgPath])

const {
  cfgNumTries,
  cfgConcealAfter,
  cfgGoodJob,
} =
configGets ({
  cfgNumTries: 'numTries' | makeCfgPath,
  cfgConcealAfter: 'concealAfter' | makeCfgPath,
  cfgGoodJob: 'goodJob' | makeCfgPath,
})

const text = {
  type: 'Type the passphrase.',
  tryBefore: 'Try to type the passphrase before the hint appears.',
  goodJob: sprintf1 ('Good job! Try making it to %d tries.'),
  tryWithHidden: sprintf1 ('Try making it to %d tries without seeing what you type.'),
  keepPracticing: 'Keep practicing.',
}

const getInstructions = (countdownElapsed, tries) => {
  const notElapsed = countdownElapsed | not | always

  if (TEST) return tries | condS ([
    againstAll ([2 | lt, notElapsed]) | guardV (cfgNumTries | text.tryWithHidden),
    otherwise | guardV (''),
  ])
  else return tries | condS ([
    1 | eq | guardV (text.tryBefore),
    againstAll ([cfgGoodJob | lt, notElapsed])  | guardV (cfgGoodJob | text.goodJob),
    againstAll ([cfgNumTries | lt, notElapsed]) | guardV (cfgNumTries | text.tryWithHidden),
    countdownElapsed | not | always    | guardV (text.keepPracticing),
    otherwise | guardV (''),
  ])
}

// --- xxx
const isMobile = _ => 'ontouchstart' in document.documentElement
const mobileAdjust = isMobile () ? multiply (3) : id

const proto = {
  init () {
    return this
  },

  _updateState (fs) {
    fs | each (f => this | updateM ('state', f))
    this.listener | onUpdateState (this.state)
  },

  // --- we always start one time with no countdown and just 'type the phrase to begin'.
  startActive () {
    const { listener, } = this
    this.state = MemActive (Nothing, text.type, 0, false, false),
    listener | onUpdateState (this.state)
  },

  testForSuccess (passphrase, countdownElapsed) {
    (passphrase == this.passphrase) | ifTrue (
      _ => this._success (countdownElapsed),
      _ => this._checkForTypo (passphrase),
    )
  },

  _success (countdownElapsed) {
    this | updateM ('state') (1 | add | memUpdateTries)

    const tries = this.state | memGetTries
    const txt = getInstructions (countdownElapsed, tries)

    return (tries >= cfgNumTries && !countdownElapsed) ?
      this._updateState ([
        memPendingNotify (
          this.roundIdx += 1,
        ),
      ]) :
      this._updateState ([
        memUpdateInstructions (txt | always),
        memUpdateCountdownTarget (countdownUpdater (tries)),
        memUpdateConcealInput (tries | gte (cfgConcealAfter) | always),
      ])
  },

  getRoundIdx () {
    return this.roundIdx
  },

  _checkForTypo (passphrase) {
    // --- it's a typo if the input string is shorter than the required passphrase and is not a
    // substring of it.

    const isTypo = this.passphrase.indexOf (passphrase) !== 0

    this._updateState ([
      memUpdateHasTypo (isTypo | always),
    ])
  },
}

const props = {
  passphrase: void 8,
  listener: void 8,

  state: void 8,

  // --- pending.
  //
  // convenient to keep track of this in the object and not carry it around in all states of the ADT.

  roundIdx: 0,
}

export default proto | factory | factoryProps (props)

// --- this app is entirely offline, hence the data/ folder.

import securedrop from './securedrop'
import english from './english'
import catalan from './catalan'
import german from './german'
import french from './french'
import italian from './italian'
import japanese from './japanese'
import dutch from './dutch'
import polish from './polish'
import swedish from './swedish'

export default {
  securedrop,
  english,
  catalan,
  german,
  french,
  italian,
  japanese,
  dutch,
  polish,
  swedish,
}

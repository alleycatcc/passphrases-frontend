// import {
//   find as iFind,
//   filter as iFilter,
//   sortBy as iSortBy,
//   get as iGet,
//   getIn as iGetIn,
//   size as iSize,
//   toJS as iToJS,
//   fromJS as iFromJS,
//   valueSeq as iValueSeq,
// } from 'alleycat-js/immutable'

import {
  isJust, isNothing, fold, toJust, foldJust,
  Left, Right, Just, Nothing,
  cata,
} from 'alleycat-js/bilby'

// import { } from 'alleycat-js/daggy'

import {
  whyYouRerender, ElemP,
  propsChanged,
} from 'alleycat-js/react'

import {
  reducer,
} from 'alleycat-js/redux'

import {
  error as alertError,
  ierrorError, ierrorErrorDie,
} from 'alleycat-js/react-s-alert.js'

import {
  iwarn, ierror, tellIf, logAndroidPerf, logWith,
  mapX,
  length, divMod, addCommas,
  resolveP,
  prettyBytes,
  deconstruct,
  reduceObjDeep, transformIntl, transformIntlComponent,
  mapAsList, mapAsListIn,
  getQueryParam, getQueryParams,
} from 'alleycat-js/general'

import {
  createLengthEqualSelector,
} from 'alleycat-js/reselect'

import {
  mlt, mgt, mediaComp, media,
} from 'alleycat-js/styled'

export {
  // iFind, iFilter, iSortBy, iGet, iGetIn, iSize, iToJS,
  // iFromJS, iValueSeq,

  whyYouRerender, ElemP,
  propsChanged,

  reducer,

  alertError, ierrorError, ierrorErrorDie,
  iwarn, ierror, tellIf, logAndroidPerf, logWith,
  mapX, length, divMod, resolveP, addCommas,
  prettyBytes, deconstruct,
  reduceObjDeep, transformIntl, transformIntlComponent,
  mapAsList, mapAsListIn,
  getQueryParam, getQueryParams,

  createLengthEqualSelector,

  mlt, mgt, mediaComp, media,

  Left, Right, Just, Nothing, isJust, isNothing, fold,
  cata,
  toJust, foldJust,
}

// xxx
export const isMobile = _ => 'ontouchstart' in document.documentElement


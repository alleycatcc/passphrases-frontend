defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, ifOk, ifPredicate, whenOk, whenPredicate,
  id, tap, recurry, roll,
  map, filter, reject, reduce, flip, flip3,
  join, split, last, head, tail,
  dot, dot1, dot2, side, side1, side2,
  cond, condS, guard, guardV, otherwise,
  sprintf1, sprintfN, rangeBy,
  noop, blush, always, T, F,
  prop, path, has, hasIn,
  bindPropTo, bindProp, bindTo, bind,
  assoc, assocPath, assocM, assocPathM,
  concatTo, concat, appendTo, append,
  concatToM, concatM, appendToM, appendM,
  merge, mergeTo, mergeM, mergeToM,
  mergeIn, mergeInTo, mergeInM, mergeInToM,
  updateM, update, updatePathM, updatePath,
  lets, letS, compactOk, compact,
  die, raise, decorateException, exception,
  lt, gt, eq, ne, lte, gte,
  factory, factoryProps,
  each,
} from 'stick-js'

import {
  logWith,
} from 'app-util'

import fromPairs from 'ramda/src/fromPairs'

// xxx
let notifyIdx = 0

const hostChannelOutgoing = 'hostChannelFromJS'

const { [hostChannelOutgoing]: channelOut, } = window

// --- [localName, foreignName, mockString]
const spec = [
  ['notifyIfInBackground', 'notifyIfInBackground', '[MOCK] notify if in background'],
  ['notify', 'notify', '[MOCK] notify'],
  ['storePassphrase', 'storePassphrase', '[MOCK] storing passphrase'],
  ['storeRoundIdx', 'storeRoundIdx', '[MOCK] storing round idx'],
  ['storeNumWords', 'storeNumWords', '[MOCK] storing num words'],
  ['storeSelectedList', 'storeSelectedList', '[MOCK] storing selected list'],
  ['quit', 'finish', '[MOCK] quit'],
]

const specMapper = ([localName, foreignName, mockString]) => [localName, channelOut
  | ifOk (
    bindProp (foreignName),
    // --- no android context
    _ => (...args) => console.log (mockString, ...args),
  )
]

const hostTable = spec
  | map (specMapper)
  | fromPairs

export const storePassphrase = hostTable.storePassphrase
export const storeRoundIdx = hostTable.storeRoundIdx
export const storeNumWords = hostTable.storeNumWords
export const storeSelectedList = hostTable.storeSelectedList
export const quit = hostTable.quit

export const notify = (short, long) =>
  hostTable.notify (++notifyIdx, short, long)
export const notifyIfInBackground = (short, long) =>
  hostTable.notifyIfInBackground (++notifyIdx, short, long)

export default {
  storePassphrase,
  storeRoundIdx,
  storeNumWords,
  storeSelectedList,
  quit,
  notify,
  notifyIfInBackground,
}

// ------ ported from legacy jquery code.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  mergeM,
  factory, factoryProps,
  tap,
  each,
} from 'stick-js'

const rotateClass = 'x--rotate'

const proto = {
  init () {
    return this | mergeM ({
      animate: {
        rotator: rotator ({
          elem: this.elem,
          interval: this.rotatorInterval,
          duration: this.rotatorDuration,
        })
      },
    })
  },
  go () {
    if (this.started) return
    this.started = true
    const { animate, } = this
    animate.rotator.go ()
  },
  stop () {
    const { animate, } = this
    this.timeouts | each (clearTimeout)
    this.timeouts = []
    animate.rotator.stop ()
    this.started = false
  },
}

const props = {
  elem: void 8,
  rotatorInterval: 1e4,
  rotatorDuration: 500,

  timeouts: [],
  started: false,
  animate: void 8,
}

const rotator = ({ elem, interval, duration, }) => {
  const rotate   = _ => elem.classList.add (rotateClass)
  const unrotate = _ => elem.classList.remove (rotateClass)
  return {
    timeouts: [],
    go () {
      this.next ()
    },
    stop () {
      this.timeouts | each (clearTimeout)
      this.timeouts = []
      unrotate ()
    },
    next () {
      this.timeouts.push (setTimeout (_ => rotate (), interval))
      this.timeouts.push (setTimeout (_ => {
        unrotate ()
        this.next ()
      }, duration + interval))
    }
  }
}

export default proto | factory | factoryProps (props)

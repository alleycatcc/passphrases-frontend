defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, composeRight, compose,
  prop, againstAll,
  ifPredicate, eq, gt,
  getType,
  isType,
  isObject, isArray,
  ifTrue, always, ifOk,
  noop,
} from 'stick-js'

const length = prop ('length')

  /*
import {
  isJust, toJust, isLeft,
} from './bilby'
*/

// xxx
const isLeft = noop

export const ifLongerThan = n => ifPredicate (length >> gt (n))

export const isException = isType ('Error')
export const ifException = ifPredicate (isException)
export const ifNegativeOne = ifPredicate (-1 | eq)

export const ifSingletonLeft = ifPredicate (
  againstAll ([
    isArray,
    length >> eq (1),
    prop (0) >> isLeft
  ])
)

// export const ifAllOk = (...args) => ifPredicate (allOk) (...args)

export const ifArray = isArray | ifPredicate
export const ifObject = isObject | ifPredicate
export const isNull = null | eq
export const ifNull = ifPredicate (isNull)

// export const ifOkV = curry ((a, b, c) => c | ifOk (
//   a | always, b | always,
// ))
// export const ifTrueV = curry ((yes, no, x) => x | ifTrue (yes | always, no | always))

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  id, noop,
  divideBy,
  always, map, addIndex,
  ok, whenOk, dot, dot1,
  ifPredicate, whenPredicate,
  eq, lt, gt, ifFalse, ifTrue,
  concat,
  condS, cond, otherwise, guard,
  sprintf1,
  lets, blush,
  prop,
  dot2,
  exception, raise,
  invoke,
  defaultTo,
  ne,
  ifOk,
  whenTrue,
  join, T, F,
  not,
  tap,
  list,
  prepend,
  whenNotOk,
  assoc,
  notOk,
  toThe,
  isObject, isArray,
  mergeToM,
  reduce,
  letS,
  assocM,
  concatTo,
  assocPathM,
  split,
} from 'stick-js'

import splitAt from 'ramda/src/splitAt'
import reverse from 'ramda/src/reverse'

import memoize from 'memoize-immutable'
export { memoize, }

import {
  ifArray,
} from './predicate'

export const iwarn = (...args) => console.warn ('Internal warning:', ...args)
export const ierror = (...args) => console.error ('Internal error:', ...args)

// xxx
// const fatalMsg = curry ((msg, state) => state
//   | tap (_ => console.error (msg))
//   | set ('error') (true)
// )

export const mapX = map | addIndex
export const trim = dot ('trim')
export const length = prop ('length')

// export const isEmpty = x => x | getType | condS ([
//   'Array' | eq | guard (_ => x | isEmptyList),
//   'Object' | eq | guard (_ => x | isEmptyObject),
//   'String' | eq | guard (_ => x | isEmptyString),
//   otherwise | guard (_ => exception ('bad arg ' + x) | raise)
// ])
// export const isNotEmpty = isEmpty >> not

// export const whenNotEmpty = whenPredicate (isNotEmpty)
// export const ifEmpty = ifPredicate (isEmpty)
// export const ifNotEmpty = ifPredicate (isNotEmpty)

export const isEmptyList = prop ('length') >> eq (0)
export const isNotEmptyList = isEmptyList >> not
export const isEmptyString = isEmptyList

export const tellIf = ifFalse (
  _ => noop,
  _ => (...args) => console.log (...args),
)

export const resolveP = (...args) => Promise.resolve (...args)
export const rejectP  = (...args) => Promise.reject (...args)
export const allP     = (...args) => Promise.all (...args)
export const startP   = _ => null | resolveP

// --- Number -> String -> String
export const prettyBytes = invoke (() => {
  const row = fmt => (pred, n, suffix) =>
    1024 | toThe (n + 1) | pred | guard (
      divideBy (1024 | toThe (n)) >> sprintf1 (fmt) >> concat (' ' + suffix)
    )

  return numDecimals => lets (
    ()          => numDecimals | sprintf1 ('%%.%sf'),
    (fmt)       => fmt | row,
    (_, rowFmt) => condS ([
        rowFmt (lt, 0, 'b'),
        rowFmt (lt, 1, 'k'),
        rowFmt (lt, 2, 'M'),
        rowFmt (lt, 3, 'G'),
        rowFmt (_ => T, 3, 'G'),
    ]),
  )
})

export const isTrue = eq (true)
export const isFalse = eq (false)

// @test
export const allUniqueAndOkAnd = (pred) => (xs) => {
  const seen = new Map
  const notPred = pred >> not
  for (const x of xs) {
    if (x | notPred) return false
    if (x | notOk) return false
    if (seen.has (x)) return false
    seen.set (x, true)
  }
  return true
}

export const allUniqueAndOk = allUniqueAndOkAnd (T)

export const compareDates = (a, b) => lets (
  _ => a | Number,
  _ => b | Number,
  (an, bn) => an === bn ? 0 : an < bn ? -1 : 1
)

/*

test allUniqueAndOk

; [
    [void 8, void 8],
    [void 8, null],
    [void 8, 3],
    [1, 1],
    [1, 2],
    ['1', 1],
    [null, 1],
    [null, null],
    [null, void 8],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 2],
    [1, 2, 3, 4, 5],
]
*/

export const checkUploadFilesLength = (files, alertFunc = noop) => files.length | condS ([
  0 | eq | guard (_ => ierror ('empty file list')),
  1 | gt | guard (_ => 'Too many files'
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

export const checkUploadFilesSize = ({
  file,
  maxFileSize: max = 1024 * 1024,
  prettyBytesDecimalPlaces: places = 0,
  alertFunc = noop,
}) => file.size | condS ([
  max | gt | guard (_ => max
    | prettyBytes (places)
    | sprintf1 ('File too large! (max = %s)')
    | tap (alertFunc)
    | F
  ),
  otherwise | guard (T),
])

// export const nullToUndef = ifNull (noop) (id)

export const color = x => ['#', x] | join ('')

// --- android webview only shows the first arg by default; note that logging objects won't work
// without an inspect () call.
export const logAndroid = list >> join (',') >> console.log

export const logAndroidPerf = (...args) => logAndroid (...(args | prepend (performance.now ())))

export const { log } = console
export const logWith = header => (...args) => log (... [header, ...args])

export const divMod = m => n => lets (
  _ => n % m,
  (mod) => (n - mod) / m,
  (mod, div) => [div, mod],
)

export const andN = (...args) => {
  for (const i of args)
    if (!i) return false
  return true
}

export const defaultToV = v => defaultTo (v | always)

export const mergeAll = xs => xs | reduce ((tgt, src) => mergeToM (tgt, src), {})

export const singletonArray = ifArray (id, x => [x])

export const deconstruct = id

export const addCommas = invoke (() => {
  const ifLengthUnder4 = ifPredicate (length >> lt (4))
  const comma = ifLengthUnder4 (
    id,
    letS ([
      splitAt (3),
      (_, [b, c]) => [b, c | comma] | join (','),
    ]),
  )
  return String >> reverse >> comma >> reverse
})

const { hasOwnProperty: hasOwn, } = {}

const _reduceObjDeep = (f) => (acc) => (path) => (o) => {
  let curAcc = acc
  for (const k in o) if (hasOwn.call (o, k)) {
    path.push (k)
    const v = o [k]
    // --- also arrays
    if (typeof v === 'object') curAcc = _reduceObjDeep (f) (acc) (path) (v)
    else curAcc = f (curAcc, [k, v, path])
    path.pop ()
  }
  return curAcc
}

// xxx stick
export const reduceObjDeep = (f) => (acc) => (o) => _reduceObjDeep (f) (acc) ([]) (o)

const _transformIntl = idPref => (acc, [k, v, path]) => acc | lets (
  _ => path | join ('.'),
  concatTo (idPref + '.'),
  (_, key) => assocM (key) (v)
)

// --- for i18n.js
export const transformIntl = idPref => o => reduceObjDeep (idPref | _transformIntl) ({}) (o)

const _transformIntlComponent = idPref => (acc, [k, v, path]) => acc | lets (
  _ => path | join ('.'),
  concatTo (idPref + '.'),

  (_, key) => assocPathM (path) ({
    id: key,
    defaultMessage: v,
  })
)

// --- for <component>/messages.js
export const transformIntlComponent = idPref => o => reduceObjDeep (idPref | _transformIntlComponent) ({}) (o)

export const mapAsList = (f) => (o) => {
    const ret = []
    for (const k in o) if (hasOwn.call (o, k)) ret.push (f (k, o [k]))
    return ret
}

export const mapAsListIn = (f) => (o) => {
    const ret = []
    for (const k in o) ret.push (f (k, o [k]))
    return ret
}

// xxx stick
const mapAsListWithFilter = (p) => (f) => (o) => {
    const ret = []
    for (const k in o) if (hasOwn.call (o, k)) {
        const v = o [k]
        const kk = f (k, v)
        if (p (kk)) ret.push (f (k, v))
    }
    return ret
}

// xxx stick
const mapAsListInWithFilter = (p) => (f) => (o) => {
    const ret = []
    for (const k in o) {
        const v = o [k]
        const kk = f (k, v)
        if (p (kk)) ret.push (f (k, v))
    }
    return ret
}

export const getQueryParams = _ => document.location.href
  | split ('#') | prop (0)
  | split ('?') | prop (1)
  | defaultToV ('')
  | decodeURIComponent
  | split ('&')
  | reduce ((acc, expr) => expr
    | split ('=') >> ifPredicate (prop (0) >> eq (''))
      (_ => acc)
      (([k, v]) => acc | assocM (k) (v))
   ) ({})

export const getQueryParam = key => getQueryParams () | prop (key)

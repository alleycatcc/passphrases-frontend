// ------
// trying to get a value which is undefined or null will result in an 'oops' bubble and a message in
// the console.
//
// the Or variant is for customising the message in the console.
//
// use bilby's Maybe for optional values.
//
// ------
defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  join, map, sprintfN,
  compact, match, guard, otherwise,
  ifTrue, ifFalse, whenEquals,
  ifPredicate, gt, noop,
  defaultTo, ok, eq, dot, dot1,
  list,
  die,
  divideBy,
  isArray,
  path,
  letS, lets,
  mapValues,
  invoke,
} from 'stick-js'

import {
  error,
  ierrorError,
} from './react-s-alert'

import {
  ifObject,
} from './predicate'

import {
  mergeAll,
} from './general'

const ifArray = isArray | ifPredicate

const configGetInOr = errMsg => config => thePath =>
  config | path (thePath) | defaultTo (
    _ => [thePath | join ('/'), errMsg]
      | sprintfN ("Couldn't get config key %s: %s")
      | ierrorError
  )

const configGetIn = config => thePath =>
  config | path (thePath) | defaultTo (
    _ => [thePath | join ('/')]
      | sprintfN ("Couldn't get config key %s")
      | ierrorError
  )

const getPath = get => ifArray (
  get,
  key => [key] | get,
)

// --- config -> errMsg -> key | [key] -> val
export const configGetOr = config => errMsg => config | letS ([
  configGetInOr (errMsg),
  (_, get) => get | getPath,
])

// --- config -> key | [key] -> val
export const configGet = letS ([
  configGetIn,
  (_, get) => get | getPath,
])

// --- config -> ( key | { alias: key, ... } ... ) -> {vals}

// --- examples:
//
//    const { bubble, bobble, } = configGets ('bobble', 'bubble')
//    const { trubble, trobble, } = configGets ({
//      trubble: 'bubble',
//      trobble: 'bobble',
//    })
//    const { trubble, } = configGets ({
//      trubble: ['path', 'to', 'var'],
//    })
//    const { trubble: someAlias, } = configGets ({
//      trubble: ['path', 'to', 'var'],
//    })

const configGetsMapper = letS ([
  configGet,
  (_, get) => ifObject (
    get | mapValues,
    key => ({ [key]: key | get }),
  )
])

export const configGets = letS ([
  configGetsMapper,
  (_, theMapper) => list >> map (theMapper) >> mergeAll,
])

export const init = config => ({
  configGet: config | configGet,
  configGetOr: config | configGetOr,
  configGets: config | configGets,
})

export default {
  configGet,
  configGetOr,
  configGets,
  init,
}

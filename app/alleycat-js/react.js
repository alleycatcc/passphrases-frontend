defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import curry from 'ramda/src/curry'

import {
  pipe, compose, composeRight,
  prop, tap, ok, whenOk, dot, dot1,
  eq, ifFalse, ifTrue, noop,
  whenPredicate, sprintf1,
  not,
} from 'stick-js'

const whenEquals = eq | whenPredicate

import React, { PureComponent, Component, } from 'react'

// --- curry is slow -- consider manually currying these.

export const Elem = elem => React.createElement (elem)
export const ElemP = curry ((elem, props) => React.createElement (elem, props))
export const ElemPC = curry ((elem, props, children) => React.createElement (elem, props, children))

// --- e.g.
// --- { results | mapX (wrapWithXP (ResultsRow, null, { width: '20%' })) }}
export const wrapWithXP = curry ((elemName, contents, propsInit, propsMerge, idx) => ElemPC (
  elemName,
  {
    ... propsInit,
    ... propsMerge,
    key: idx,
  },
  contents,
))

export const wrapWithX = curry ((elemName, contents, idx) => wrapWithXP (elemName, contents, {}, {}, idx))

export const keyPress = curry ((f, keyName) => prop ('key') >> whenEquals (keyName, f))

// --- usage:
// componentWillReceiveProps (nextProps) {
//   whyYouRerender (componentName, this.props, nextProps)
// }

// --- @deprecated
export const whyYouRerender = (componentName, curProps, nextProps) => {
  let allEqual = true
  for (const k in nextProps) {
    if (nextProps [k] !== curProps [k]) {
      allEqual = false
      console.log (componentName | sprintf1 ('rerender %s because:'), k)
    }
  }
  if (allEqual) console.log (componentName | sprintf1 (
    'rerender %s, although all props seem to ' +
    'be the same (check hidden ones like key)'
  ))
}

// --- usage:
//
// componentDidUpdate (prevProps) {
//   const { props, } = this
//   if (! propsChanged (prevProps, props, 'ComponentName')) return

export const propsChanged = (prevProps, curProps) => {
  for (const k in curProps)
    if (prevProps [k] !== curProps [k])
      return true
  return false
}

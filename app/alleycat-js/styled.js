defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  sprintfN, join,
} from 'stick-js'

const _mediaRule = (comp, val) => [comp, val] | sprintfN ('@media only screen and (%s: %spx)')
const mediaRule = (comp, val, css) => [_mediaRule (comp, val), css] | sprintfN ('%s {%s}')

export const mlt = x => ['max-width', x]
export const mgt = x => ['min-width', x]

export const mediaComp = (...css) => ([comp, val]) => mediaRule (comp, val, css | join ('\n'))
export const media = (...lines) => lines | join ('\n')

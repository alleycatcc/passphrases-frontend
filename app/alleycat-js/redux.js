defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  always, id,
} from 'stick-js'

export const reducer = reducerTable => action => action | (reducerTable [action.type] || always (id))

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, whenOk, dot, dot1, tap,
  map,
  ifPredicate, whenPredicate,
  eq, lt, gt,
  ifFalse, ifTrue, noop,
  condS, cond, otherwise, guard,
  lets, blush,
  dot2, exception, raise, invoke, defaultTo, gte,
  againstAll,
  always,
} from 'stick-js'

import axios from 'axios'
import { eventChannel, END, buffers, delay, } from 'redux-saga'

import {
  error as alertError,
} from './react-s-alert'

const error = (...args) => console.error (...args)
const both = (f, g) => againstAll ([f, g])

export const makeFileUploadChannel = invoke (() => {
  const onUploadProgress = (emitter, onProgress = noop) =>
    ({ loaded, total }) => lets (
      _ => Math.round ((loaded * 100) / total),
      (progress) => ({ progress, })
      | tap (onProgress)
      | emitter,
    )

  const upload = (emitter, file, uploadURL, onProgress) => {
    const data = new FormData ()
    data.append ('file', file)

    const options = {
      onUploadProgress: onUploadProgress (emitter, onProgress),
      validateStatus: both (200 | gte, 500 | lt),
    }

    const handleError = (err) => [({ err, }), END] | map (emitter)
    const handleSuccess = (results) => [({ success: { results, }}), END ] | map (emitter)

    return axios.post (uploadURL, data, options)
      .then ((res) => {
        const { data, status, } = res
        const { results, msg, } = data
        status | condS ([
          422 | eq  | guard (_ => ({ msg, userMsg: msg, }) | handleError),
          300 | lt  | guard (_ => results | handleSuccess),
          otherwise | guard (_ => ({ msg, }) | handleError),
        ])
      })
    .catch (
        tap ((err) => ({ msg: err, }) | handleError)
        >> error
      )
  }

  const unsubscriber = _ => noop

  const makeSubscriber = (file, uploadURL, onProgress) => emitter =>
    upload (emitter, file, uploadURL, onProgress) | always (unsubscriber)

  return ({ file, uploadURL, onProgress = noop, }) => eventChannel (
    makeSubscriber (file, uploadURL, onProgress),
    // --- only keep last 2 results.
    buffers.sliding (2),
  )
})


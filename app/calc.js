defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  lt, guardV, condS, otherwise,
  repeatV, prop,
  map, join,
} from 'stick-js'

const { log2, pow, random, floor, } = Math
const length = prop ('length')

export const getEntropy = (wordCount, numWords) => {
  const entropyPerWord = wordCount | log2
  const entropy = numWords * entropyPerWord
  return [ entropyPerWord, entropy, ]
}

export const calculate = (guessesPerSecond, entropyPerWord, wordCount, numWords) => {
  const keyspace = pow (wordCount, numWords)
  const seconds = (keyspace / 2) / guessesPerSecond

  return seconds | getTimeUnits
}

// --- excl upper bound
const randInt = (a, b) => (random () * (b - a) + a) | floor

export const generatePassphrase = (words, numWords) => {
  const wordCount = words | length
  return numWords | repeatV (0)
                  | map (_ => words | prop (randInt (0, wordCount)))
                  | join (' ')
}

const getTimeUnits = seconds => seconds | condS ([
  60                          | lt | guardV ([seconds, 'seconds', 0]),
  (60 * 60)                   | lt | guardV ([seconds / 60, 'minutes', 0]),
  (60 * 60 * 24)              | lt | guardV ([seconds / 60 / 60, 'hours', 0]),
  (60 * 60 * 24 * 30)         | lt | guardV ([seconds / 60 / 60 / 24, 'days', 0]),
  (60 * 60 * 24 * 365)        | lt | guardV ([seconds / 60 / 60 / 24 / 29.6, 'months', 0]),
  (60 * 60 * 24 * 365 * 1e3)  | lt | guardV ([seconds / 60 / 60 / 24 / 365, 'years', 1]),
  (60 * 60 * 24 * 365 * 1e6)  | lt | guardV ([seconds / 60 / 60 / 24 / 365 / 1e3, 'thousandYears', 2]),
  (60 * 60 * 24 * 365 * 1e9)  | lt | guardV ([seconds / 60 / 60 / 24 / 365 / 1e6, 'millionYears', 2]),
  (60 * 60 * 24 * 365 * 1e12) | lt | guardV ([seconds / 60 / 60 / 24 / 365 / 1e9, 'billionYears', 2]),
  (60 * 60 * 24 * 365 * 1e15) | lt | guardV ([seconds / 60 / 60 / 24 / 365 / 1e12, 'trillionYears', 2]),
  (60 * 60 * 24 * 365 * 1e18) | lt | guardV ([seconds / 60 / 60 / 24 / 365 / 1e15, 'quadrillionYears', 2]),
  otherwise                        | guardV ([seconds / 60 / 60 / 24 / 365 / 1e18, 'quintillionYears', 2]),
])
